This project contains Btrain-specific code to receive and transmit Btrain information 
(B, Bdot, I, etc) over White Rabbit (WR) using WR streamers and WR PTP Core. 
The WR-specific code is contained in another repository (wr-cores). 
