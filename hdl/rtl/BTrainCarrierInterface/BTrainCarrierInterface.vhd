-------------------------------------------------------------------------------
-- Title      : BTrainCarrierInterface
-- Project    : BTrain-over-WhiteRabbit
-- URL        : https://gitlab.cern.ch/BTrain-TEAM/Btrain-over-WhiteRabbit/wikis/home
-------------------------------------------------------------------------------
-- File       : BTrainCarrierInterface.vhd
-- Author     : Maciej Lipinski
-- Company    : CERN
-- Created    : 2019-10-19
-- Platform   : FPGA-generics
-- Standard   : VHDL
----------------------------------------------------------------------------------
-- Description:
-- Module to be instantiated on an FMC-carrier to interface WR-BTrain FMC.
-- It allows transmission and reception of BTran frames.
-------------------------------------------------------------------------------
--
-- Copyright (c) 2019 CERN BE/CO/HT
--
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.BTrainFrame_pkg.all;

entity BTrainCarrierInterface is
  port(
    clk_i               : in std_logic; -- system clock
    rst_n_i             : in std_logic;
    ----------------------------------------------------------------------------
    -- RX and TX WB Interfaces to be connected to FMC pins (LA)
    -- Clock domain of rx_wb_clk_o and tx_wb_clk_o for RX and TX, resepctively
    -- NOTE: rx/tx_wb_dat is bidirectional, depending on rx/tx_wb_we_o
    ----------------------------------------------------------------------------
    -- rx data interrupt
    rx_Irq_i                 : in   std_logic;                   -- la09_p

    --Rx WB Master
    rx_wb_clk_o              : out std_logic;                    -- la20_p
    rx_wb_adr_o              : out std_logic_vector(7 downto 0); -- 0:la10_p to 7:la13_n
    rx_wb_dat_o              : out std_logic_vector(7 downto 0); -- 0:la14_p to 7:la17_n
    rx_wb_dat_i              : in  std_logic_vector(7 downto 0); -- 0:la14_p to 7:la17_n
    rx_wb_cyc_o              : out std_logic;                    -- la19_p
    rx_wb_stb_o              : out std_logic;                    -- la19_n
    rx_wb_we_o               : out std_logic;                    -- la18_n
    rx_wb_ack_i              : in  std_logic;                    -- la18_p
    rx_wb_err_i              : in  std_logic;                    -- la30_p
    rx_wb_rty_i              : in  std_logic;                    -- la21_p
    rx_wb_stall_i            : in  std_logic;                    -- la21_n

    -- Tx WB Master
    tx_wb_clk_o              : out std_logic;                    -- la20_n
    tx_wb_adr_o              : out std_logic_vector(7 downto 0); -- 0:la22_p to 7:la25_n
    tx_wb_dat_o              : out std_logic_vector(7 downto 0); -- 0:la26_p to 7:la29_n
    tx_wb_dat_i              : in  std_logic_vector(7 downto 0); -- 0:la26_p to 7:la29_n
    tx_wb_cyc_o              : out std_logic;                    -- la31_p
    tx_wb_stb_o              : out std_logic;                    -- la31_n
    tx_wb_we_o               : out std_logic;                    -- la30_n
    tx_wb_ack_i              : in  std_logic;                    -- la32_p
    tx_wb_err_i              : in  std_logic;                    -- la32_n
    tx_wb_rty_i              : in  std_logic;                    -- la33_p
    tx_wb_stall_i            : in  std_logic;                    -- la33_n

    ----------------------------------------------------------------------------
    -- USER Interface (clk_i clock domain)
    ----------------------------------------------------------------------------

    --BTrain frame data received from WR network (records in BTrainFrame_pkg.vhd)
    rx_FrameHeader_o         : out t_FrameHeader;
    rx_BFramePayloads_o      : out t_BFramePayload;
    rx_IFramePayloads_o      : out t_IFramePayload;
    rx_CFramePayloads_o      : out t_CFramePayload;

    -- ID of the received frame (equal to the respective record in the header)
    rx_Frame_typeID_o        : out std_logic_vector(c_type_ID_size-1 downto 0);

    -- single-cycle strobe indicating that BTrain frame has been received
    -- and validating the data in the rx_Frame_typeID_o, rx_FrameHeader_o and
    -- respective payload record (depending on the typeID)
    rx_Frame_valid_p1_o      : out std_logic;

    -- Vector that defines which fields in the payload of the received BTrain
    -- frame are interesting for the user, thus should be read from FMC. It is
    -- a simple mechanism where each bit indicates respective field from
    -- BTrain payload, for example
    --   0: indicates B    or I for B frame or I and C frames, respectively
    --   1: indicates Bdot or V for B frame or C frame respectively
    -- Only consecutive '1' (ones) are allowed, e.g.
    -- * "00000001" is OK and will result in reading only B value if BFrame is
    --              received or only I value if CFrame or IFrame is received
    -- * "00000101" is not OK, anyhow it will not break things but the behavior
    --              will be equvalent to "00000001"
    -- * "11111111" is OK and will result in reading all fiels of the frame
    -- This method can be used to decrease the latency in reception of the
    -- BTrain frame. In particular, once the BTrain frame is received by 
    -- FMC, each field needs to be read from the FMC by the Carrier. This 
    -- takes time. If only B value is of interest, reading all the values can
    -- be skipped.
    rx_PFieldsReadVector_i   : in std_logic_vector(c_PayloadMaxFields-1 downto 0);

    -- Indicates which frames the user is interested in receiving, by default
    -- the "all frames" is set. By setting this value to the ID of a particular
    -- frame, all the other frames will be ignored and only the content of the
    -- frame of interest will be provided to the user (for all other frame types,
    -- nothing will happen, they will be ignored). For BTrain frame IDs see
    -- BTrainFrame_pkg.vhd)
    rx_Frame_typeID_i        : in std_logic_vector(c_type_ID_size-1 downto 0) := c_ID_ALL;

    -- BTrain frame data to be transmitted (records in BTrainFrame_pkg.vhd)
    tx_FrameHeader_i         : in t_FrameHeader   := c_FrameHeader_zero;
    tx_BFramePayloads_i      : in t_BFramePayload := c_BFramePayload_zero;
    tx_IFramePayloads_i      : in t_IFramePayload := c_IFramePayload_zero;
    tx_CFramePayloads_i      : in t_CFramePayload := c_CFramePayload_zero;

    -- single-cycle strobe will result in transmission of data in
    -- 1) tx_FrameHeader_i record
    -- 2) one of the payload records, depending on the frame_type field in the 
    --    tx_FrameHeader_i.
    tx_TransmitFrame_p1_i    : in std_logic := '0';

    -- '1' indicates that transmission can be requested
    -- (if ignored, any tx request is gracefully ignored)
    tx_ready_o               : out std_logic;

    -- Vector that defines which fields in the payload of the BTrain frame should
    -- be updated (written) in the FMC by the Carrier. For the fields that are
    -- not updated, the previously written value will be used. This allows to
    -- reduce the transmission latency. The rules are similar to the 
    -- rx_PFieldsReadVector_i vector, meaning that only consecutive '1' (ones)
    -- are allowed (e.g. "00000111", not "00000101") and all ones ("11111111")
    -- results in updating all the fields in the BTrain payload, regardles of 
    -- how many fields it has.
    tx_PFieldsUpdateVector_i : in  std_logic_vector(c_PayloadMaxFields-1  downto 0);

    -- some debugging is always good ;-)
    dbg_fms_rx_o             : out std_logic_vector(3 downto 0);
    dbg_fms_tx_o             : out std_logic_vector(3 downto 0)
  );
end BTrainCarrierInterface;

architecture Behavioral of BTrainCarrierInterface is
  -- tx data
  constant c_MASK_RDY    : std_logic_vector(7 downto 0) := x"01";
  constant c_MASK_TX     : std_logic_vector(7 downto 0) := x"02";

  type   t_tx_fms        is (S_WAIT_TX_READY, S_WAIT_TX_REQUES, S_WRITE_DATA, S_TRIGGER_TX, S_WAIT_AFTER_TRIGGER);
  signal s_tx_fsm        : t_tx_fms;
  signal tx_wb_adr       : std_logic_vector(7 downto 0);
  signal tx_wb_dat       : std_logic_vector(7 downto 0);
  signal tx_wb_cyc       : std_logic;
  signal tx_wb_stb       : std_logic;
  signal tx_wb_we        : std_logic;
  signal tx_addr_base    : std_logic_vector(7 downto 0);
  signal tx_ready        : std_logic;
  signal tx_header       : std_logic_vector(c_header_size -1 downto 0);
  signal tx_payload      : std_logic_vector(c_payload_size-1 downto 0);
  signal tx_frameType    : std_logic_vector(c_type_ID_size-1 downto 0);
  signal tx_b            : unsigned(7 downto 0); 
  signal tx_f            : unsigned(3 downto 0);
  signal tx_payload_size : unsigned(7 downto 0); 
  signal tx_wait_cnt     : unsigned(3 downto 0);
  

  -- rx data
  type   t_rx_fms        is (S_WAIT_RX_IRQ, S_READ_DATA, S_CLEAR_RX_IRQ);
  signal s_rx_fsm        : t_rx_fms;
  signal rx_wb_adr       : std_logic_vector(7 downto 0);
  signal rx_wb_dat       : std_logic_vector(7 downto 0);
  signal rx_wb_cyc       : std_logic;
  signal rx_wb_stb       : std_logic;
  signal rx_wb_we        : std_logic;
  signal rx_frameType    : std_logic_vector(c_type_ID_size-1 downto 0);
  signal rx_header       : std_logic_vector(c_header_size -1 downto 0);
  signal rx_payload      : std_logic_vector(c_payload_size-1 downto 0);
  signal rx_b            : unsigned(7 downto 0); 
  signal rx_f            : unsigned(3 downto 0);
  signal rx_payload_size : unsigned(7 downto 0); 
  signal rx_Frame_valid_p1: std_logic;

  constant c_MASK_IRQ    : std_logic_vector(7 downto 0) := x"01";
  constant c_MASK_CLR    : std_logic_vector(7 downto 0) := x"02";

  -- common for TX/RX
  constant c_ADDR_SCR    : std_logic_vector(7 downto 0) := x"01";
  constant c_ADDR_FTYPE  : std_logic_vector(7 downto 0) := x"02";
  constant c_ADDR_FLAGS  : std_logic_vector(7 downto 0) := x"03";
  constant c_ADDR_B_BASE : std_logic_vector(7 downto 0) := x"04";
  constant c_ADDR_I_BASE : std_logic_vector(7 downto 0) := x"1C";
  constant c_ADDR_C_BASE : std_logic_vector(7 downto 0) := x"20";
  
  signal dbg_fms_rx      : std_logic_vector(3 downto 0);
  signal not_clk         : std_logic;
  component clock_output is
  port (
    -- clocks (same as the desired output frequency) coming from the PLL
    -- primitive, phase-shifted by 180 degrees.
    clk_0_i   : in std_logic;
    clk_180_i : in std_logic;

    clk_out_o   : out std_logic;
    clk_out_p_o : out std_logic;
    clk_out_n_o : out std_logic
    );
  end component;
begin
  not_clk <= not clk_i;
  ------------------------------------------------------------------------------
  -- TX data
  ------------------------------------------------------------------------------
  p_tx_fsm: process(clk_i)
  begin
    if rising_edge(clk_i) then
      if (rst_n_i = '0') then
        tx_wb_adr           <= (others => '0');
        tx_wb_dat           <= (others => '0');
        tx_wb_cyc           <= '0';
        tx_wb_stb           <= '0';
        tx_wb_we            <= '0';
        tx_b                <= (others => '0');
        tx_f                <= (others => '0');
        tx_payload_size     <= (others => '0');
        tx_header           <= (others => '0');
        tx_payload          <= (others => '0');
        tx_ready            <= '0';
        tx_addr_base        <= (others => '0');
        s_tx_fsm            <= S_WAIT_TX_READY;
      else

        case s_tx_fsm is 
          ---------------------------------------------------------------------
          when S_WAIT_TX_READY =>  
          ---------------------------------------------------------------------
          if(tx_wb_cyc = '1' and tx_wb_ack_i = '1' and
             (tx_wb_dat_i    and c_MASK_RDY) = c_MASK_RDY)  then

            s_tx_fsm        <= S_WAIT_TX_REQUES;
            tx_wb_adr       <= (others => '0');
            tx_wb_cyc       <= '0';
            tx_wb_stb       <= '0';
            tx_wb_we        <= '0';
            tx_ready        <= '1';

          else

            tx_wb_adr       <= c_ADDR_SCR;
            tx_wb_cyc       <= '1';
            tx_wb_stb       <= '1';
            tx_wb_we        <= '0';

          end if;
          ---------------------------------------------------------------------
          when S_WAIT_TX_REQUES =>  
          ---------------------------------------------------------------------
            if ((tx_TransmitFrame_p1_i       = '1') and 
               ( tx_FrameHeader_i.frame_type = c_ID_BkFrame or 
                 tx_FrameHeader_i.frame_type = c_ID_ImFrame or
                 tx_FrameHeader_i.frame_type = c_ID_CdFrame))then

              case tx_FrameHeader_i.frame_type is
                when c_ID_BkFrame => 
                   tx_payload      <= f_serialize_Btype_payload(tx_BFramePayloads_i);
                   tx_addr_base    <= c_ADDR_B_BASE;
                   tx_payload_size <= to_unsigned(c_B_payload_size,8);
                when c_ID_ImFrame => 
                   tx_payload      <= f_serialize_Itype_payload(tx_IFramePayloads_i);
                   tx_addr_base    <= c_ADDR_I_BASE;
                   tx_payload_size <= to_unsigned(c_I_payload_size,8);
                when c_ID_CdFrame => 
                   tx_payload      <= f_serialize_Ctype_payload(tx_CFramePayloads_i);
                   tx_addr_base    <= c_ADDR_C_BASE;
                   tx_payload_size <= to_unsigned(c_C_payload_size,8);
                when others =>
                    -- not possible but has to be covered
              end case;

              s_tx_fsm         <= S_WRITE_DATA;
              tx_wb_cyc        <= '0';
              tx_wb_stb        <= '0';
              tx_wb_we         <= '1'; -- just in case, make we and addr available in advance
              tx_ready         <= '0';
              tx_wb_adr        <= c_ADDR_FTYPE;
              tx_wb_dat        <= tx_FrameHeader_i.frame_type;
              tx_header        <= f_serialize_header(tx_FrameHeader_i);
              tx_frameType     <= tx_FrameHeader_i.frame_type;

            end if;
          ---------------------------------------------------------------------
          when S_WRITE_DATA =>  
          ---------------------------------------------------------------------
          if(tx_wb_ack_i = '1') then
            case tx_wb_adr is

              ------------------------------------------------------------------
              when c_ADDR_FTYPE => 
              ------------------------------------------------------------------
                tx_wb_adr          <= c_ADDR_FLAGS;
                tx_wb_dat          <= tx_header(15 downto 8);
                tx_b               <= (others => '0');
                tx_f               <= (others => '0');

              ------------------------------------------------------------------
              when others       =>
              ------------------------------------------------------------------
                -- check whether the field (f) is flagged to be read in the
                -- input vector. Check also whether we are still in the allowed
                -- size of the payload. If yes, read this field in the frame
                if((tx_PFieldsUpdateVector_i(to_integer(tx_f)) = '1') and
                   (tx_b < tx_payload_size)) then

                  if(tx_wb_adr = c_ADDR_FLAGS) then
                    -- finished writing heaer
                    tx_wb_adr  <= tx_addr_base;
                  else
                    -- writing bytes of the payload
                    tx_wb_adr  <= std_logic_vector(unsigned(tx_wb_adr)+1);
                  end if;

                  tx_wb_dat    <= tx_payload(to_integer(tx_b)+7 downto to_integer(tx_b));
                  tx_b         <= tx_b + 8;
                  if(tx_b > 0 and tx_b(4 downto 3) = "11") then -- module(8)
                    tx_f       <= tx_f + 1;
                  end if;

                else -- finish writing

                  tx_wb_adr    <= c_ADDR_SCR;
                  tx_wb_dat    <= c_MASK_TX;
                  s_tx_fsm     <= S_TRIGGER_TX;

                end if;
            end case;
          else
            tx_wb_cyc        <= '1';
            tx_wb_stb        <= '1';
            tx_wb_we         <= '1';
          end if;
          ---------------------------------------------------------------------
          when S_TRIGGER_TX =>
          ---------------------------------------------------------------------

            if(tx_wb_ack_i = '1') then
              tx_wb_adr       <= c_ADDR_SCR;
              tx_wb_cyc       <= '0';
              tx_wb_stb       <= '0';
              tx_wb_we        <= '0';
              s_tx_fsm        <= S_WAIT_AFTER_TRIGGER;
              tx_wait_cnt     <= x"2";
            end if;

          ---------------------------------------------------------------------
          when S_WAIT_AFTER_TRIGGER =>
          ---------------------------------------------------------------------
            -- On the FMC, it takes some time for the trigger_tx request to 
            -- go from one clk domain to the other and then for the tx_ready
            -- signal to come back to the first clock domain (two synchronizers).
            -- Thus, before reading the state of tx_ready, we need to wait a bit.

            if(tx_wait_cnt = x"0") then
              s_tx_fsm        <= S_WAIT_TX_READY;
            else
              tx_wait_cnt     <= tx_wait_cnt - 1;
            end if;

          ---------------------------------------------------------------------
          when others => -- just in case
          ---------------------------------------------------------------------
            s_tx_fsm   <= S_WAIT_TX_READY;
            tx_wb_cyc  <= '0';
            tx_wb_stb  <= '0';
            tx_wb_we   <= '0';
            tx_wb_adr  <= (others => '0');
            tx_wb_dat  <= (others => '0');

        end case; 
      end if;
    end if;
  end process;

  tx_wb_adr_o <= tx_wb_adr;
  tx_wb_dat_o <= tx_wb_dat;
  tx_wb_cyc_o <= tx_wb_cyc;
  tx_wb_stb_o <= tx_wb_stb;
  tx_wb_we_o  <= tx_wb_we;
  tx_ready_o  <= tx_ready;

  cmp_tx_wb_clk_o: clock_output
    port map(
      clk_0_i   => clk_i,
      clk_180_i => not_clk,
      clk_out_o => tx_wb_clk_o);
  ------------------------------------------------------------------------------
  -- RX data
  ------------------------------------------------------------------------------
  -- read received data
  p_rx_fsm: process(clk_i)
  begin
    if rising_edge(clk_i) then
      if (rst_n_i = '0') then
        rx_wb_adr           <= (others => '0');
        rx_wb_dat           <= (others => '0');
        rx_wb_cyc           <= '0';
        rx_wb_stb           <= '0';
        rx_wb_we            <= '0';
        rx_frameType        <= (others => '0');
        rx_Frame_valid_p1   <='0';
        rx_b                <= (others => '0');
        rx_f                <= (others => '0');
        rx_payload_size     <= (others => '0');
        rx_header           <= (others => '0');
        rx_payload          <= (others => '0');
        s_rx_fsm            <= S_WAIT_RX_IRQ;
      else

        case s_rx_fsm is 
          ---------------------------------------------------------------------
          when S_WAIT_RX_IRQ =>  
          ---------------------------------------------------------------------
            if(rx_Irq_i = '1') then
              s_rx_fsm      <= S_READ_DATA;
              rx_wb_cyc     <= '1';
              rx_wb_stb     <= '1';
              rx_wb_we      <= '0';
              rx_wb_adr     <= c_ADDR_SCR;
              rx_frameType  <= (others => '0');
            end if;
          ---------------------------------------------------------------------
          when S_READ_DATA =>  
          ---------------------------------------------------------------------
          if(rx_wb_ack_i = '1') then
            case rx_wb_adr is

              ------------------------------------------------------------------
              when c_ADDR_SCR   => 
              ------------------------------------------------------------------
                if((rx_wb_dat_i and c_MASK_IRQ) = c_MASK_IRQ) then
                  rx_wb_adr           <= c_ADDR_FTYPE;
                  rx_header              <= (others => '0');
                  rx_payload             <= (others => '0');
                else
                  s_rx_fsm            <= S_WAIT_RX_IRQ;
                end if;

              ------------------------------------------------------------------
              when c_ADDR_FTYPE => 
              ------------------------------------------------------------------
                if(rx_Frame_typeID_i = c_ID_ALL or
                   rx_Frame_typeID_i = rx_wb_dat_i(c_type_ID_size-1 downto 0)) then
                  rx_wb_adr            <= c_ADDR_FLAGS;
                  rx_frameType         <= rx_wb_dat_i(c_type_ID_size-1 downto 0);
                  rx_header(7 downto 0)<= rx_wb_dat_i;
                else
                  s_rx_fsm             <= S_CLEAR_RX_IRQ;
                  rx_wb_cyc            <= '0';
                  rx_wb_stb            <= '0';
                  rx_wb_we             <= '1';
                  rx_wb_adr            <= c_ADDR_SCR;
                  rx_wb_dat            <= c_MASK_CLR;
                end if;

              ------------------------------------------------------------------
              when c_ADDR_FLAGS => 
              ------------------------------------------------------------------
                rx_header(15 downto 8) <= rx_wb_dat_i;
                rx_b                   <= (others => '0');
                rx_f                   <= (others => '0');
                case rx_frameType is
                  when c_ID_BkFrame =>
                    rx_wb_adr          <= c_ADDR_B_BASE;
                    rx_payload_size    <= to_unsigned(c_B_payload_size,8);
                  when c_ID_ImFrame =>
                    rx_wb_adr          <= c_ADDR_I_BASE;
                    rx_payload_size    <= to_unsigned(c_I_payload_size,8);
                  when c_ID_CdFrame =>
                    rx_wb_adr          <= c_ADDR_C_BASE;
                    rx_payload_size    <= to_unsigned(c_C_payload_size,8);
                  when others       =>
                    s_rx_fsm           <= S_CLEAR_RX_IRQ;
                  rx_wb_cyc            <= '0';
                  rx_wb_stb            <= '0';
                  rx_wb_we             <= '1';
                  rx_wb_adr            <= c_ADDR_SCR;
                  rx_wb_dat            <= c_MASK_CLR;
                    rx_payload_size    <= (others => '0');
                end case;

              ------------------------------------------------------------------
              when others       =>
              ------------------------------------------------------------------

                -- check whether the field (f) is flagged to be read in the
                -- input vector. Check also whether we are still in the allowed
                -- size of the payload. If yes, read this field in the frame
                if((rx_PFieldsReadVector_i(to_integer(rx_f)) = '1') and
                   (rx_b < rx_payload_size)) then

                  -- read bytes of the payload
                  rx_payload(to_integer(rx_b)+7 downto to_integer(rx_b)) <= rx_wb_dat_i;
                  rx_b                 <= rx_b + 8;
                  rx_wb_adr            <= std_logic_vector(unsigned(rx_wb_adr)+1);
                  if(rx_b > 0 and rx_b(4 downto 3) = "11") then -- module(8)
                    rx_f               <= rx_f + 1;
                  end if;

                else -- finish reading
                  rx_payload(c_payload_size-1 downto to_integer(rx_b)) <= (others =>'0');

                   -- This is a bit of a hack, we set the WE/ADDR/ADDR earlier
                   --  and only then in the next cycle STB/CYC goes up. This way,
                   -- we make sure that the data/add is valid before asking to
                   -- write
                  rx_wb_we             <= '1';
                  rx_wb_cyc            <= '0';
                  rx_wb_stb            <= '0'; 
                  rx_wb_adr            <= c_ADDR_SCR;
                  rx_wb_dat            <= c_MASK_CLR;
                  s_rx_fsm             <= S_CLEAR_RX_IRQ;
                  rx_Frame_valid_p1    <= '1';

                end if;
            end case;
          end if;
          ---------------------------------------------------------------------
          when S_CLEAR_RX_IRQ =>  
          ---------------------------------------------------------------------
            rx_Frame_valid_p1  <= '0';

            if(rx_wb_ack_i = '1') then
              rx_wb_cyc  <= '0';
              rx_wb_stb  <= '0';
              rx_wb_we   <= '0';
              rx_wb_adr  <= (others => '0');
              rx_wb_dat  <= (others => '0');
              s_rx_fsm   <= S_WAIT_RX_IRQ;
            else 
              rx_wb_cyc  <= '1';
              rx_wb_stb  <= '1';
              rx_wb_we   <= '1';
            end if;

          ---------------------------------------------------------------------
          when others => -- just in case
          ---------------------------------------------------------------------
            s_rx_fsm   <= S_WAIT_RX_IRQ;
            rx_wb_cyc  <= '0';
            rx_wb_stb  <= '0';
            rx_wb_we   <= '0';
            rx_wb_adr  <= (others => '0');
            rx_wb_dat  <= (others => '0');

        end case; 
      end if;
    end if;
  end process;

  rx_FrameHeader_o    <= f_deserialize_header(rx_header);
  rx_BFramePayloads_o <= f_deserialize_Btype_payload(rx_payload) when rx_frameType = c_ID_BkFrame else
                         c_BFramePayload_zero;
  rx_IFramePayloads_o <= f_deserialize_Itype_payload(rx_payload) when rx_frameType = c_ID_ImFrame else
                         c_IFramePayload_zero;
  rx_CFramePayloads_o <= f_deserialize_Ctype_payload(rx_payload) when rx_frameType = c_ID_CdFrame else
                         c_CFramePayload_zero;
  rx_Frame_typeID_o   <= rx_frameType;
  rx_Frame_valid_p1_o <= rx_Frame_valid_p1;

  rx_wb_adr_o         <= rx_wb_adr;
  rx_wb_dat_o         <= rx_wb_dat;
  rx_wb_cyc_o         <= rx_wb_cyc;
  rx_wb_stb_o         <= rx_wb_stb;
  rx_wb_we_o          <= rx_wb_we;

  ----------------------------------------------------------------------------
  -- clock genration
  ----------------------------------------------------------------------------
  cmp_rx_wb_clk_o: clock_output
    port map(
      clk_0_i   => clk_i,
      clk_180_i => not_clk,
      clk_out_o => rx_wb_clk_o);
	  
  ----------------------------------------------------------------------------
  -- dbg:
  dbg_fms_rx_o <= x"1" when s_rx_fsm = S_WAIT_RX_IRQ        else
                  x"2" when s_rx_fsm = S_READ_DATA          else
                  x"3" when s_rx_fsm = S_CLEAR_RX_IRQ       else
                  x"0";

  dbg_fms_tx_o <= x"1" when s_tx_fsm = S_WAIT_TX_READY      else
                  x"2" when s_tx_fsm = S_WAIT_TX_REQUES     else
                  x"3" when s_tx_fsm = S_WRITE_DATA         else
                  x"4" when s_tx_fsm = S_TRIGGER_TX         else
                  x"5" when s_tx_fsm = S_WAIT_AFTER_TRIGGER else
                  x"0";

end Behavioral;

