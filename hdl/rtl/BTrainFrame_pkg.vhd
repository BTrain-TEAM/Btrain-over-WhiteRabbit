-------------------------------------------------------------------------------
-- Title      : BTrainFrame
-- Project    : BTrain-over-WhiteRabbit
-- URL        : https://gitlab.cern.ch/BTrain-TEAM/Btrain-over-WhiteRabbit/wikis/home
-------------------------------------------------------------------------------
-- File       : BTrainFrame_pkg.vhd
-- Author     : Maciej Lipinski
-- Company    : CERN
-- Platform   : FPGA-generics
-- Standard   : VHDL
-- Created    : 2019-01-31
-------------------------------------------------------------------------------
-- Description:
--
-- This package defines record types and constants used in WR-BTrain and
-- representing the BTrain frame
-- 
-- Importantly, this package defines records that represent:
--   * BTrain B-Frame payload
--   * BTrain I-frame payload
--   * BTrain P-frame payload
--   * BTrain C-frame payload
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;

package BTrainFrame_pkg is

  -------------------------------------------------------------------------------------------
  --                           BTrain Frame definition
  -- Btran is defined in https://wikis.cern.ch/display/HT/WR+BTrain+New+Frame+Format
  -- All that is used to encode/decode both types of BTrain frames is defined below
  -------------------------------------------------------------------------------------------
  -- frame types:
  constant c_type_ID_size   : integer                      := 8;
  constant c_ID_BkFrame     : std_logic_vector(7 downto 0) := x"42"; -- B magnetic field     (0x42="B" in ASCII)
  constant c_ID_ImFrame     : std_logic_vector(7 downto 0) := x"49"; -- I main current       (0x49="I" in ASCII)
  constant c_ID_CdFrame     : std_logic_vector(7 downto 0) := x"43"; -- Converter's current  (0x43="C" in ASCII)
  constant c_ID_ALL         : std_logic_vector(7 downto 0) := x"00"; -- wide card

  -- header of BTrain frame (common for all types):
  constant c_header_size    : integer := 16; -- [bits]

  -- payload of the B-type of BTrain frame consists of six 32-bit words, its size is:
  constant c_B_payload_size : integer := 6*32; -- 192 [bits]

  -- payload of the I-type of BTrain frame consists of one 32-bit word, its size is:
  constant c_I_payload_size : integer := 1*32; --  32 [bits]

  -- payload of the CI-type of BTrain frame consists of 4 x 32-bit word and reserved 64 bits, its size is:
  constant c_C_payload_size : integer := 6*32; --  192 [bits]

  -- maximum number of fields in the BTrain payload
  constant c_PayloadMaxFields: integer := 8;

  -- common size of payload in a single "WR Streamer word"
  constant c_payload_size   : integer := 6*32; --  192 [bits]

  -- final frame sizes of different types of BTrain frame
  constant c_B_frame_size   : integer := c_header_size + c_B_payload_size; -- 208 [bits]
  constant c_I_frame_size   : integer := c_header_size + c_I_payload_size; --  48 [bits]
  constant c_C_frame_size   : integer := c_header_size + c_C_payload_size; -- 208 [bits]
  -- for simplicity, the data-word send using streamers uses the size of the bigger BTrain
  -- frame type, thus
  constant c_BTrain_streamer_data_width : integer :=c_B_frame_size;

  -- record to store the header of  the BTrain frame
  type t_FrameHeader is record
    version_id       : std_logic_vector(1 downto 0); --version id of the frame type (only for backward compatible) updates
    d_low_marker     : std_logic;                    --low marker Defocus associated to the sent value 
    f_low_marker     : std_logic;                    --low marker Focus associated to the sent value 
    zero_cycle       : std_logic;                    --zero signal associated to the sent value 
    C0               : std_logic;                    --C0 signal associated to the sent value 
    error            : std_logic;                    --Error detected before sending bit
    sim_eff          : std_logic;                    --Simulation or Effectiv bit
    frame_type       : std_logic_vector(c_type_ID_size-1 downto 0); --type: 0x42 for B field frame | 0x49 for Imain frame
  end record;

  constant c_FrameHeader_zero : t_FrameHeader := (
    version_id       => (others =>'0'),
    d_low_marker     => '0',
    f_low_marker     => '0',
    zero_cycle       => '0',
    C0               => '0',
    error            => '0',
    sim_eff          => '0',
    frame_type       => (others =>'0')
  );

  constant c_FrameHeader_dummy : t_FrameHeader := (
    version_id       => (others =>'0'),
    d_low_marker     => '0',
    f_low_marker     => '0',
    zero_cycle       => '0',
    C0               => '0',
    error            => '0',
    sim_eff          => '0',
    frame_type       => c_ID_BkFrame
  );

  -- record to store the payload of the B-type BTrain frame
  type t_BFramePayload is record
    B     : std_logic_vector(31 downto 0);
    Bdot  : std_logic_vector(31 downto 0);
    oldB  : std_logic_vector(31 downto 0);
    measB : std_logic_vector(31 downto 0);
    simB  : std_logic_vector(31 downto 0);
    synB  : std_logic_vector(31 downto 0);
  end record;

  constant c_BFramePayload_zero : t_BFramePayload := (
    B     => (others=>'0'),
    Bdot  => (others=>'0'),
    oldB  => (others=>'0'),
    measB => (others=>'0'),
    simB  => (others=>'0'),
    synB  => (others=>'0')
  );

  constant c_BFramePayload_dummy : t_BFramePayload := (
    B     => x"deafbeef",
    Bdot  => x"cafecafe",
    oldB  => x"87654321",
    measB => x"deedbeaf",
    simB  => x"bad0babe",
    synB  => x"00000000"
  );

  -- record to store the payload of the I-type BTrain frame
  type t_IFramePayload is record
    I   : std_logic_vector(31 downto 0);
  end record;

  constant c_IFramePayload_zero : t_IFramePayload := (
    I     => (others=>'0')
  );

  constant c_IFramePayload_dummy : t_IFramePayload := (
    I     => x"deedbeaf"
  );

  ------------------------------------------------------------------------------
  -- PFW is depricated and not implemetned any more.
  -- NOTE: The definition of the PFW record and interfaces in
  -- BTrainFrameTransceiver top module and pkg are left only to provide
  -- backward compatibility and flawless upgrade of BTrain-over-WhiteRabbit.
  -- The functionality of sending/receiving PFW frames is deleted.
  ------------------------------------------------------------------------------
  -- record to store the payload of the PFW-type BTrain frame
  type t_PFramePayload is record
    Istatus: std_logic_vector(31 downto 0);
    IWLB8L : std_logic_vector(31 downto 0);
    IWFNI  : std_logic_vector(31 downto 0);
    IWFNP  : std_logic_vector(31 downto 0);
    IWDNI  : std_logic_vector(31 downto 0);
    IWDNP  : std_logic_vector(31 downto 0);
    IWFW   : std_logic_vector(31 downto 0);
    IWDW   : std_logic_vector(31 downto 0);
  end record;

  constant c_PFramePayload_zero : t_PFramePayload := (
    Istatus => (others=>'0'),
    IWLB8L  => (others=>'0'),
    IWFNI   => (others=>'0'),
    IWFNP   => (others=>'0'),
    IWDNI   => (others=>'0'),
    IWDNP   => (others=>'0'),
    IWFW    => (others=>'0'),
    IWDW    => (others=>'0')
  );
  ------------------------------------------------------------------------------
  -- PFW is depricated and not implemetned any more. 
  ------------------------------------------------------------------------------

  -- record to store the payload of the C-type BTrain frame
  type t_CFramePayload is record
    I          : std_logic_vector(31 downto 0);
    V          : std_logic_vector(31 downto 0);
    MSrcId     : std_logic_vector(15 downto 0);
    CSrcId     : std_logic_vector(15 downto 0);
    UserData   : std_logic_vector(31 downto 0);
    Reserved_1 : std_logic_vector(31 downto 0);
    Reserved_2 : std_logic_vector(31 downto 0);
  end record;

  constant c_CFramePayload_zero : t_CFramePayload := (
    I          => (others=>'0'),
    V          => (others=>'0'),
    MSrcId     => (others=>'0'),
    CSrcId     => (others=>'0'),
    UserData   => (others=>'0'),
    Reserved_1 => (others=>'0'),
    Reserved_2 => (others=>'0')
  );

  constant c_CFramePayload_dummy : t_CFramePayload := (
    I          => x"deafbeef",
    V          => x"cafecafe",
    MSrcId     => x"8765",
    CSrcId     => x"4321",
    UserData   => x"deedbeaf",
    Reserved_1 => x"87654321",
    Reserved_2 => x"deedbeaf"
  );


  type t_BFramePayload_array is array (natural range <>) of t_BFramePayload;
  type t_IFramePayload_array is array (natural range <>) of t_IFramePayload;
  type t_CFramePayload_array is array (natural range <>) of t_CFramePayload;

  -- functions
  function f_serialize_header         (h : t_FrameHeader)        return std_logic_vector;
  function f_serialize_Btype_payload  (p : t_BFramePayload)      return std_logic_vector;
  function f_serialize_Itype_payload  (p : t_IFramePayload)      return std_logic_vector;
  function f_serialize_Ctype_payload  (p : t_CFramePayload)      return std_logic_vector;
  function f_deserialize_header       (h : std_logic_vector)     return t_FrameHeader;
  function f_deserialize_Btype_payload(p : std_logic_vector)     return t_BFramePayload;
  function f_deserialize_Itype_payload(p : std_logic_vector)     return t_IFramePayload;
  function f_deserialize_Ctype_payload(p : std_logic_vector)     return t_CFramePayload;
  function f_bigEndianess             (x : std_logic_vector)     return std_logic_vector;

  function f_header_dbg     (frameType   : std_logic_vector) return t_FrameHeader;
  function f_B_frame_dbg_cnt(dbg_counter : std_logic_vector) return t_BFramePayload;
  function f_B_frame_dbg_sim(simValue    : std_logic_vector) return t_BFramePayload;
  function f_I_frame_dbg_cnt(dbg_counter : std_logic_vector) return t_IFramePayload;
  function f_I_frame_dbg_sim(simValue    : std_logic_vector) return t_IFramePayload;
  function f_C_frame_dbg_cnt(dbg_counter : std_logic_vector) return t_CFramePayload;
  function f_C_frame_dbg_sim(simValue    : std_logic_vector) return t_CFramePayload;

end BTrainFrame_pkg;

package body BTrainFrame_pkg is

  -------------------------------------------------------------------------------------------
  -- function to handle bframes
  -------------------------------------------------------------------------------------------
  function f_serialize_header(h: t_FrameHeader) return std_logic_vector is
    variable ret   : std_logic_vector(c_header_size-1 downto 0);
    begin
      ret(15 downto 14) := h.version_id;
      ret(13)           := h.d_low_marker;
      ret(12)           := h.f_low_marker;
      ret(11)           := h.zero_cycle;
      ret(10)           := h.C0;
      ret(9)            := h.error;
      ret(8)            := h.sim_eff;
      ret(7 downto 0)   := h.frame_type;
    return ret;
  end f_serialize_header;

  function f_serialize_Btype_payload(p: t_BFramePayload) return std_logic_vector is
    variable ret : std_logic_vector(c_payload_size-1 downto 0);
    begin
      ret:= p.synB & p.simB & p.measB & p.oldB & p.Bdot & p.B;
    return ret;
  end f_serialize_Btype_payload;

  function f_serialize_Itype_payload(p: t_IFramePayload) return std_logic_vector is
    variable ret   : std_logic_vector(c_payload_size-1 downto 0);
    variable zeros : std_logic_vector(c_payload_size-c_I_payload_size-1 downto 0):=(others=>'0');
    begin
      ret := zeros & p.I;
    return ret;
  end f_serialize_Itype_payload;

  function f_serialize_Ctype_payload(p: t_CFramePayload) return std_logic_vector is
    variable ret   : std_logic_vector(c_payload_size-1 downto 0);
    variable SrcId : std_logic_vector(31 downto 0);
    begin
      -- Just to make things clear, first create Source ID,
      -- second, use the Soure ID in creation of the frame.
      SrcId := p.MSrcId & p.CSrcId;
      -- Create C Frame with Sourc ID
      ret   := p.Reserved_2 & p.Reserved_1 & p.UserData & SrcId & p.V & p.I ;
    return ret;
  end f_serialize_Ctype_payload;

  function f_deserialize_header(h : std_logic_vector) return t_FrameHeader is
    variable ret : t_FrameHeader;
    begin
      ret.version_id      := h(15 downto 14);
      ret.d_low_marker    := h(13);
      ret.f_low_marker    := h(12);
      ret.zero_cycle      := h(11);
      ret.C0              := h(10);
      ret.error           := h(9);
      ret.sim_eff         := h(8);
      ret.frame_type      := h(7 downto 0);
    return ret;
  end f_deserialize_header;

  function f_deserialize_Btype_payload(p : std_logic_vector) return t_BFramePayload is
    variable ret: t_BFramePayload;
    begin
      ret.synB  := p(6*32-1 downto 5*32);
      ret.simB  := p(5*32-1 downto 4*32);
      ret.measB := p(4*32-1 downto 3*32);
      ret.oldB  := p(3*32-1 downto 2*32);
      ret.Bdot  := p(2*32-1 downto 1*32);
      ret.B     := p(1*32-1 downto 0*32);
    return ret;
  end f_deserialize_Btype_payload;

  function f_deserialize_Itype_payload(p : std_logic_vector) return t_IFramePayload is
    variable ret : t_IFramePayload;
    begin
      ret.I     := p(1*32-1 downto 0*32);
    return ret;
  end f_deserialize_Itype_payload;

  function f_deserialize_Ctype_payload(p : std_logic_vector) return t_CFramePayload is
    variable ret  : t_CFramePayload;
    variable SrcId: std_logic_vector(31 downto 0);
    begin
      -- disassemble major fields
      ret.Reserved_2 := p(6*32-1    downto 5*32);
      ret.Reserved_1 := p(5*32-1    downto 4*32);
      ret.UserData   := p(4*32-1    downto 3*32);
      SrcId          := p(3*32-1    downto 2*32);
      ret.V          := p(2*32-1    downto 1*32);
      ret.I          := p(1*32-1    downto 0*32);

      -- disassemble sub-fields
      ret.MSrcId     := SrcId(31 downto 16);
      ret.CSrcId     := SrcId(15 downto  0);

      return ret;
  end f_deserialize_Ctype_payload;

  -- apply big endianness order of  16-bit words in a concatenated vector of 32-bit words
  -- (we concatenate 32-values in a vector, this vector is sent over network, big-endianess
  -- is the entwork order, so we change orer of 16-bit words so that the most significant
  -- 16-bit word of the 32-bit value is transferred first, the big-endianess of bytes is
  -- applied in the Endpoint which accepts 16-bit words)
  function f_bigEndianess(x : std_logic_vector) return std_logic_vector is
    variable y : std_logic_vector(x'length -1 downto 0);
    begin
      for i in 0 to (((x'length )/32)-1) loop
        y((2*i+1)*16-1 downto (2*i+0)*16) := x((2*i+2)*16-1  downto (2*i+1)*16);
        y((2*i+2)*16-1 downto (2*i+1)*16) := x((2*i+1)*16-1  downto (2*i+0)*16);
      end loop;
    return y;
  end f_bigEndianess;

 --------------------------------------------------------------------------------------------
  -- functions used for debugging
  --------------------------------------------------------------------------------------------
  -- functions that create BTrain frames (B and I) for debugging. They are nod comlex and  are
  -- made as functions only to make the txCtrlBTrain a claer code.
  function f_header_dbg(frameType : std_logic_vector) return t_FrameHeader is
    variable ret : t_FrameHeader;
    begin
      ret.version_id      := (others =>'0');
      ret.d_low_marker    := '0';
      ret.f_low_marker    := '0';
      ret.zero_cycle      := '0';
      ret.C0              := '0';
      ret.error           := '0';
      ret.sim_eff         := '0';
      ret.frame_type      := frameType;
    return ret;
  end f_header_dbg;

  function f_B_frame_dbg_cnt(dbg_counter : std_logic_vector) return t_BFramePayload is
    variable ret : t_BFramePayload;
    begin
      ret.synB  := x"01234567"; 
      ret.simB  := x"DEADBEEF";
      ret.measB := x"00000003";
      ret.oldB  := x"00000002";
      ret.Bdot  := x"00000001";
      ret.B     := x"00" & dbg_counter;
    return ret;
  end f_B_frame_dbg_cnt;

  function f_B_frame_dbg_sim(simValue : std_logic_vector) return t_BFramePayload is
    variable ret : t_BFramePayload;
    begin
      ret.synB  := x"01234567"; 
      ret.simB  := x"DEADBEEF";
      ret.measB := x"00000003";
      ret.oldB  := x"00000002";
      ret.Bdot  := x"00000001";
      ret.B     := simValue; 
    return ret;
  end f_B_frame_dbg_sim;

  function f_I_frame_dbg_cnt(dbg_counter : std_logic_vector) return t_IFramePayload is
    variable ret : t_IFramePayload;
    begin
      ret.I     := x"00" & dbg_counter;
    return ret;
  end f_I_frame_dbg_cnt;

  function f_I_frame_dbg_sim(simValue : std_logic_vector) return t_IFramePayload is
    variable ret : t_IFramePayload;
    begin
      ret.I    := simValue;
    return ret;
  end f_I_frame_dbg_sim;

  function f_C_frame_dbg_cnt(dbg_counter : std_logic_vector) return t_CFramePayload is
    variable ret : t_CFramePayload;
    begin
      ret.Reserved_2 := x"87654321";
      ret.Reserved_1 := x"deedbeaf";
      ret.UserData   := x"00000003";
      ret.CSrcId     := x"5678";
      ret.MSrcId     := x"1234";
      ret.V          := x"CAFEBABE";
      ret.I          := x"00" & dbg_counter;
    return ret;
  end f_C_frame_dbg_cnt;

  function f_C_frame_dbg_sim(simValue : std_logic_vector) return t_CFramePayload is
    variable ret : t_CFramePayload;
    begin
      ret.Reserved_2 := x"87654321";
      ret.Reserved_1 := x"deedbeaf";
      ret.UserData   := x"00000003";
      ret.CSrcId     := x"5678";
      ret.MSrcId     := x"1234";
      ret.V          := x"CAFEBABE";
      ret.I          := simValue;
    return ret;
  end f_C_frame_dbg_sim;

end BTrainFrame_pkg;
