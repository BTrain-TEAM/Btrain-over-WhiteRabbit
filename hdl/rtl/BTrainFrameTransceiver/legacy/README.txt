The files in this legacy folder contain legacy modules. They are in the repo
just because there might be some applications that use these. If these
applications wanted to "smoothly" upgrade to new (debugged) code, it should
be possible using this legacy files

NOTE and BEWARE: anything that is in the legacy folder shall not be used in
new implementations. They are deprecated and their support will cease
as soon as reasonable.