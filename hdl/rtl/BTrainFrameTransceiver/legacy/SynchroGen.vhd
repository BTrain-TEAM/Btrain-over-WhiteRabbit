-------------------------------------------------------------------------------
-- Title      : Generator of sent tick
-- Project    : BTrain-over-WhiteRabbit
-- URL        : https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
-------------------------------------------------------------------------------
-- File       : SynchroGen.vhd
-- Author     : Daniel Oberson
-- Company    : CERN
-- Platform   : FPGA-generics
-- Standard   : VHDL
-- Created    : 2013-09-30
-------------------------------------------------------------------------------
-- Description:
--
-- A simple counter that provides a single-clock-cycle tick every
-- reg_sample_period_i number of clock cycles. It is used to trigger periodically
-- the transmission of BTrain frames using WR streamers
--
-- Resolution 16ns, 32 bit unsigned. @ 62.5 MHz
-------------------------------------------------------------------------------
--
-- Copyright (c) 2016 CERN TE/MSC/MM+BE/CO/HT
--
-- This source file is free software; you can redistribute it   
-- and/or modify it under the terms of the GNU Lesser General   
-- Public License as published by the Free Software Foundation; 
-- either version 2.1 of the License, or (at your option) any   
-- later version.                                               
--
-- This source is distributed in the hope that it will be       
-- useful, but WITHOUT ANY WARRANTY; without even the implied   
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
-- PURPOSE.  See the GNU Lesser General Public License for more 
-- details.                                                     
--
-- You should have received a copy of the GNU Lesser General    
-- Public License along with this source; if not, download it   
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;


entity SynchroGen is
  generic (
    g_counter_width      : integer := 26
  );
  port(
    clk_i                : in std_logic; 
    rst_n_i              : in std_logic;
    reg_sample_period_i  : in std_logic_vector(g_counter_width-1 downto 0);
    send_tick_p_o        : out std_logic
  );
end SynchroGen;

architecture Behavioral of SynchroGen is

constant c_reset_value      : unsigned(g_counter_width-1 downto 0) := to_unsigned(1,g_counter_width);
--Signal
signal sample_rate_counter  : unsigned(g_counter_width-1 downto 0);
signal zero_cnt             : std_logic;

begin

  --Sample rate control
  process (clk_i)
  begin
    if rising_edge(clk_i) then
      if (rst_n_i='0' or unsigned(reg_sample_period_i) = 0) then
        sample_rate_counter     <= c_reset_value;
        zero_cnt                <= '0';
      else
        if(sample_rate_counter < unsigned(reg_sample_period_i)) then
          zero_cnt              <= '0';
          sample_rate_counter   <= unsigned(sample_rate_counter) + 1;
        else
          zero_cnt              <= '1';
          sample_rate_counter   <= c_reset_value;
        end if;
      end if;
    end if;
  end process;

  send_tick_p_o <= zero_cnt; 

end Behavioral;

