  -- -*- Mode: LUA; tab-width: 2 -*-

peripheral {
  name = "WR BTrain transmission control";
  description = "-----------------------------------------------------------------\
  Wishbone registers to manage/debug transmission and reception of \
  BTrain frames using WR Streamers                                 \
  -----------------------------------------------------------------\
  Copyright (c) 2016 CERN/BE-CO-HT and CERN/TE-MS-MM               \
                                                                   \
  This source file is free software; you can redistribute it       \
  and/or modify it under the terms of the GNU Lesser General       \
  Public License as published by the Free Software Foundation;     \
  either version 2.1 of the License, or (at your option) any       \
  later version.                                                   \
                                                                   \
  This source is distributed in the hope that it will be           \
  useful, but WITHOUT ANY WARRANTY; without even the implied       \
  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR          \
  PURPOSE.  See the GNU Lesser General Public License for more     \
  details                                                          \
                                                                   \
  You should have received a copy of the GNU Lesser General        \
  Public License along with this source; if not, download it       \
  from http://www.gnu.org/licenses/lgpl-2.1.html                   \
  -----------------------------------------------------------------";
  prefix = "BTrain";
  hdl_entity = "BTrain_wb";
  version = 1;

  reg {
    name = "Status and ctontrol register";
    prefix = "SCR";

    field {
      name = "Transmit single frame";
      prefix = "TX_SINGLE";
      description = "Transmit single WR BTrain frame";
      type = MONOSTABLE;
    };

    field {
      name = "Debug mode";
      prefix = "TX_DBG";
      description = "Debugging mode:\
                    0: debugging disabled \
                    1: send values of incremented counters in the frames\
                    2: use for B value provided by the BvalueSimGen, programmed separetaly\
                    3: send frames with dummy content (see BTrani FrameTransceiver_pkg)";
      type = SLV;
      size = 2;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
    field {
      name = "Debug Frame type";
      prefix = "TX_DBG_FTYPE";
      description = "Debugging of forced frame type:\
                    0: do not enforce (send the type that is input to module) \
                    1: force B frames\
                    2: force I frames\
                    3: force C frames";
      type = SLV;
      size = 4;
      align = 4;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
    field {
      name = "Invert RX valid polarity";
      prefix = "RX_VALID_POL_INV";
      description = "0: normal polarity (active high)\n1: inverted polarity (active low)";
      type = BIT;
      align = 8;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
    field {
      name = "TX override default config";
      prefix = "TX_OR_CONFIG";
      description = "0: default configuration of the Tx period (taken from generics)\
                     1: values of the Tx period from WB registers";
      type = BIT;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
    field {
      name = "RX override default config";
      prefix = "RX_OR_CONFIG";
      description = "0: default configuration of the Rx valid period/delay and polarity (taken from generics)\
                     1: values of the Rx valid period/delay and polarity from WB registers";
      type = BIT;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
    field {
      name = "Dummy";
      prefix = "DUMMY";
      description = "dumy";
      type = SLV;
      size = 16;
      align =16;
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };
  };
  reg {
    name = "Transmission period";
    prefix = "TX_PERIOD";
    field {
      name = "Period in clock cylces (16ns). When the value is not zero, frames are sent with that period. ";
      prefix = "value";
      reset_value = 0x0;
      type = SLV;
      size = 32;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
   };
  reg {
    name = "Rx output data valid delay and period";
    description = "Defines the time for which the received data is valid (in clk cycles).";
    prefix = "RX_OUT_DATA_TIME";
    field {
      name = "Output data valid period";
      description = "Period in clock cylces (16ns) during which data is valid. By default it is 1 cycle strobe. Two special values\
        0x0000: output data disabled\
        0xFFFF: output data continuously valid until next update (might be invalid for the time defined by delay).";
      prefix = "valid";
      reset_value = 0x1;
      type = SLV;
      size = 16;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
    field {
      name = "Output data delay";
      description = "Delay in clock cylces (16ns) between receiving data and making it valid. By default it is 0 cycle";
      prefix = "delay";
      reset_value = 0x0;
      type = SLV;
      size = 16;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };

   };

  reg {
    name = "SimBval: control subcycles";
    prefix = "SIMB_CTRL";
    field {
      name = "Cycle Length";
      prefix = "CLEN";
      description = "Cycle lenght counted in the number of sub-cycles that compose a full cycle. The full cycle is repeated indefinitely, special values:\
                     0x0: generation disabled\
                     0xF: generation continous (uses increment from sub-cycle 0 indefinitely)";
      type = SLV;
      size = 4;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };

    field {
      name = "Sub-cycle ID";
      prefix = "SCID";
      description = "The ID (number) of the sub-cycle for which parameters can be set through registers BSIM_CYC_LEN and BSIM_CYC_INC";
      type = SLV;
      size = 4;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };

    field {
      name = "Cycle maximum length";
      prefix = "CMAXLEN";
      description = "The maximum number of sub-cycles that can be configured.";
      type = SLV;
      size = 4;
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };
  };

  reg {
    name = "SimBval: Sub-cycle lenght";
    description = "Lenght of the sub-cycle with SCID";
    prefix = "BSIM_SCYC_LEN";
    field {
      name = "Lenght in sent frames, i.e. the ";
      prefix = "val";
      type = SLV;
      size = 32;
      load = LOAD_EXT;
      access_bus = READ_WRITE;
      access_dev = READ_WRITE;
    };
  };
  reg {
    name = "SimBval: Sub-cycle increment";
    description = "Increment of Bvalue for the sub-cycle with SCID";
    prefix = "BSIM_SCYC_INC";
    field {
      name = "Signed value that is added to the Bvalue each time BTrain frame is transmitted.";
      prefix = "val";
      type = SLV;
      size = 16;
      load = LOAD_EXT;
      access_bus = READ_WRITE;
      access_dev = READ_WRITE;
    };
   };
  reg {
    name = "DBG RX B or I value";
    description = "It provides access to the most recently received value of B or I, depending on the BTrain frame type";
    prefix = "DBG_RX_B_or_I";
    field {
      name = "Debug content";
      size = 32;
      type =SLV;
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };
   };
  reg {
    name = "DBG TX B or I value, depending which frame";
    description = "It provides access to the most recently transmitted value of B or I, depending on the BTrain frame type";
    prefix = "DBG_TX_B_or_I";
    field {
      name = "Debug content";
      size = 32;
      type =SLV;
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };
   };
};
