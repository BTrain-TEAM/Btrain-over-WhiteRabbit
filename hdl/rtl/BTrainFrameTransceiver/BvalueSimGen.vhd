-------------------------------------------------------------------------------
-- Title      : B value simulation generator
-- Project    : BTrain-over-WhiteRabbit
-- URL        : https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
-------------------------------------------------------------------------------
-- File       : BvalueSimGen.vhd
-- Author     : Maciej Lipinski
-- Company    : CERN
-- Platform   : FPGA-generics
-- Standard   : VHDL
-- Created    : 2016-05-31
-------------------------------------------------------------------------------
-- Description: 
-- This module allows to generate a waveform that is intended to simulate a 
-- btrain cycle (e.g. pre-ramp -> flat -> ramp -> flat top -> downt o zero).
-- Using wishbone registers it is possible to form the shape of a waveform
-- of a single cycle, this cycle is then repeated indefinitely. 
-- 
-- Each cycle is composed from N sub-cycles. The number of sub-cycles is configured.
-- Each subcycle has two parameters: 
-- * length    - it is counted in the number of sent frames, for example. if it is 
--               set to 10, the subcycle takes 10 frames to be sent
-- * increment - the value by which the Bvalue is incremeted each time the frame
--               is sent.
--
-- For each sub-cycle the above parameters are set. Then the number of syb-cycles
-- is set, this is called cycle length (CLEN). Setting the CLEN to 0xFF enables
-- continuous mode. In this mode, the only value that matters is the increment value
-- for sub-cycle 0 (CNUM=0).
--
-- The generator us programmed using WB described in: doc/wb-regs/BTrain_wb.html
-- with the following registers:
-- 0xC0: SIMB_CTRL->CNUM   - sets the sub-cycle for which configuration is written to
--                           BSIM_CYC_LEN and BSIM_CYC_INC
-- 0xC0: SIMB_CTRL->CLEN   - sets the number of sub-cycles in the cycle, 0xFF makes
--                           the 0th configuration of sub-cycle used continously
-- 0xC0: SIMB_CTRL->CMAX   - provides the maximum number of sub-cycle that is supported
--                           by the gateware (it is a generic)
-- 0x10: BSIM_CYC_LEN->VAL - sets the lenght of the CNUM-th sub-cycle 
-- 0x14: BSIM_CYC_INC->VAL - sets the increment in the CNUM-th sub-cycle 

-------------------------------------------------------------------------------
--
-- Copyright (c) 2016 CERN/BE-CO-HT
--
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.BTrain_wbgen2_pkg.all;
use work.BTrainFrameTransceiver_pkg.all;

entity BvalueSimGen is
  generic(
    g_subcyc_num    : integer :=8
  );
  port(
    clk_i               : in std_logic;
    rst_n_i             : in std_logic;

    frame_sent_i        : in std_logic; -- increment counter
    simGen_ena_i        : in std_logic;

    wbregs_i            : in t_BTrain_out_registers;
    wbregs_o            : out t_BTrain_in_registers;
    simValue_o          : out  std_logic_vector(31 downto 0)
    );
end BvalueSimGen;

architecture Behavioral of BvalueSimGen is

  type t_sybCycLen_array is array (natural range <>) of unsigned(31 downto 0);
  type t_sybCycInc_array is array (natural range <>) of signed(15 downto 0);
  
  signal subCycCnt      : unsigned(3 downto 0);
  signal sentFrameCnt   : unsigned(31 downto 0);
  signal simValue         : signed(31 downto 0);
  signal cur_simValueInc  : signed(31 downto 0);

  signal sybCycLen      : t_sybCycLen_array(g_subcyc_num-1 downto 0);
  signal sybCycInc      : t_sybCycInc_array(g_subcyc_num-1 downto 0);
  signal cfg_subcyc_num : integer range 0 to g_subcyc_num-1;
  signal cur_subcyc_num : integer range 0 to g_subcyc_num-1;
  signal cfg_cyc_len    : unsigned(3 downto 0);
  signal cur_sybCycLen  : unsigned(31 downto 0);

begin

  cfg_subcyc_num <= to_integer(unsigned(wbregs_i.btrain_simb_ctrl_scid_o));
  cur_subcyc_num <= to_integer(unsigned(subCycCnt));
  cfg_cyc_len    <= unsigned(wbregs_i.btrain_simb_ctrl_clen_o(3 downto 0));
  cur_simValueInc<= resize(sybCycInc(cur_subcyc_num), 32);
  cur_sybCycLen  <= sybCycLen(cur_subcyc_num);
  p_config: process (clk_i)  
  begin
    if rising_edge(clk_i) then
      if (rst_n_i='0') then
        sybCycLen <= (others =>(others=>'0'));
        sybCycInc <= (others =>(others=>'0'));
      else
        if(wbregs_i.btrain_bsim_scyc_len_val_load_o = '1') then
          sybCycLen(cfg_subcyc_num) <= unsigned(wbregs_i.btrain_bsim_scyc_len_val_o);
        end if;
        if(wbregs_i.btrain_bsim_scyc_inc_val_load_o = '1') then
          sybCycInc(cfg_subcyc_num) <= signed(wbregs_i.btrain_bsim_scyc_inc_val_o);
        end if;
      end if;
    end if;
  end process p_config;

  wbregs_o.btrain_bsim_scyc_len_val_i <= std_logic_vector(sybCycLen(cfg_subcyc_num));
  wbregs_o.btrain_bsim_scyc_inc_val_i <= std_logic_vector(sybCycInc(cfg_subcyc_num));
  wbregs_o.btrain_simb_ctrl_cmaxlen_i <= std_logic_vector(to_unsigned(g_subcyc_num, wbregs_o.btrain_simb_ctrl_cmaxlen_i'length));

  wbregs_o.btrain_scr_dummy_i     <= (others => '0');
  wbregs_o.btrain_dbg_rx_b_or_i_i <= (others => '0');
  wbregs_o.btrain_dbg_tx_b_or_i_i <= (others => '0');

  p_genCycles: process (clk_i)
  begin
    if rising_edge(clk_i) then
      if (rst_n_i='0' or simGen_ena_i = '0' or cfg_cyc_len = x"0") then
        subCycCnt          <= (others => '0');
        sentFrameCnt       <= (others => '0');
        simValue           <= (others => '0');
      else

        if(frame_sent_i = '1') then 
          if(cfg_cyc_len = x"F") then
              simValue     <= simValue + cur_simValueInc;
          elsif(sentFrameCnt < cur_sybCycLen-1) then
            sentFrameCnt <= sentFrameCnt +1;
            simValue     <= simValue + cur_simValueInc;
          else
            sentFrameCnt <= (others => '0');
            if(subCycCnt < cfg_cyc_len-1) then
              subCycCnt  <= subCycCnt + 1;
              simValue   <= simValue + cur_simValueInc;
            else
              subCycCnt  <= (others=>'0');
              simValue   <= (others=>'0');
            end if;
          end if;
        end if;
      end if;
    end if;
  end process p_genCycles;

  simValue_o <= std_logic_vector(simValue);

end Behavioral;

