#!/bin/bash

echo "To generate WB interface of FMC interface, a special version of wbgen2 is needed, contact Maciej.Lipinski@cern.ch"

wbgen2_8b -p RxBTFmcIF_wbgen2_pkg.vhd -H record -V  RxBTFmcIF_wb.vhd -D ../../../doc/wb-regs/RxBTFmcIF_wb.html --cstyle struct --lang vhdl RxBTFmcIF_wb.wb 

wbgen2_8b -p TxBTFmcIF_wbgen2_pkg.vhd -H record -V  TxBTFmcIF_wb.vhd -D ../../../doc/wb-regs/TxBTFmcIF_wb.html --cstyle struct --lang vhdl TxBTFmcIF_wb.wb 
