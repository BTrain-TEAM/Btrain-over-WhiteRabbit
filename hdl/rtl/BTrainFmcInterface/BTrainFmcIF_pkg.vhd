-------------------------------------------------------------------------------
-- Title      : BTrainFmcIF
-- Project    : BTrain-over-WhiteRabbit
-- URL        : https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
-------------------------------------------------------------------------------
-- File       : BTrainFmcIF.vhd
-- Author     : 
-- Company    : CERN
-- Platform   : FPGA-generics
-- Standard   : VHDL
-- Created    : 
-------------------------------------------------------------------------------
-- Description:
--
-------------------------------------------------------------------------------
--
-- Copyright (c) 2018 CERN
--
-- This source file is free software; you can redistribute it   
-- and/or modify it under the terms of the GNU Lesser General   
-- Public License as published by the Free Software Foundation; 
-- either version 2.1 of the License, or (at your option) any   
-- later version.                                               
--
-- This source is distributed in the hope that it will be       
-- useful, but WITHOUT ANY WARRANTY; without even the implied   
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
-- PURPOSE.  See the GNU Lesser General Public License for more 
-- details.                                                     
--
-- You should have received a copy of the GNU Lesser General    
-- Public License along with this source; if not, download it   
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
---------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;
use work.wr_fabric_pkg.all;
use work.BTrainFrameTransceiver_pkg.all;
use work.wishbone_pkg.all;
use work.TxBTFmcIF_wbgen2_pkg.all;
use work.RxBTFmcIF_wbgen2_pkg.all;
use work.BTrainFrame_pkg.all;

package BTrainFmcIF_pkg is

  component BTrainFmcInterface is
    generic (
      g_with_chipscope        : boolean :=FALSE
    );
    port(
      clk_sys_i               : in std_logic;
      rst_sys_n_i             : in std_logic;

      ----------------------------------------------------------------
      -- Interface with BTrain FMC
      ----------------------------------------------------------------
      --received from WR network:
      rx_FrameHeader_i       : in t_FrameHeader   := c_FrameHeader_zero;
      rx_BFramePayloads_i    : in t_BFramePayload := c_BFramePayload_zero;
      rx_IFramePayloads_i    : in t_IFramePayload := c_IFramePayload_zero;
      rx_CFramePayloads_i    : in t_CFramePayload := c_CFramePayload_zero;
      rx_Frame_valid_pX_i    : in std_logic;
      rx_Frame_typeID_i      : in std_logic_vector(c_type_ID_size-1 downto 0);
      -- transmit to wr network:
      tx_ready_i             : in  std_logic; -- if ignored, any tx request is gracefully ignored
      tx_TransmitFrame_p1_o  : out std_logic;
      tx_FrameHeader_o       : out t_FrameHeader   := c_FrameHeader_zero;
      tx_BFramePayloads_o    : out t_BFramePayload := c_BFramePayload_zero;
      tx_IFramePayloads_o    : out t_IFramePayload := c_IFramePayload_zero;
      tx_CFramePayloads_o    : out t_CFramePayload := c_CFramePayload_zero;

      rx_FrameIrq_o          : out std_logic;
      rst_carrier_n_a_i      : in  std_logic;
      ----------------------------------------------------------------
      -- Wishbone interface (connect only if g_use_wb_config=true,
      -- otherwise you will run into problems
      ----------------------------------------------------------------
      rx_wb_clk_i            : in std_logic;
      rx_wb_slave_i          : in  t_wishbone_slave_in := cc_dummy_slave_in;
      rx_wb_slave_o          : out t_wishbone_slave_out;

      tx_wb_clk_i            : in std_logic;
      tx_wb_slave_i          : in  t_wishbone_slave_in := cc_dummy_slave_in;
      tx_wb_slave_o          : out t_wishbone_slave_out

    );
  end component;

  component RxBTFmcIF_wb is
  port (
    rst_n_i                                  : in     std_logic;
    clk_sys_i                                : in     std_logic;
    wb_adr_i                                 : in     std_logic_vector(5 downto 0);
    wb_dat_i                                 : in     std_logic_vector(7 downto 0);
    wb_dat_o                                 : out    std_logic_vector(7 downto 0);
    wb_cyc_i                                 : in     std_logic;
    wb_sel_i                                 : in     std_logic_vector(0 downto 0);
    wb_stb_i                                 : in     std_logic;
    wb_we_i                                  : in     std_logic;
    wb_ack_o                                 : out    std_logic;
    wb_err_o                                 : out    std_logic;
    wb_rty_o                                 : out    std_logic;
    wb_stall_o                               : out    std_logic;
    regs_i                                   : in     t_RxBTFmcIF_in_registers;
    regs_o                                   : out    t_RxBTFmcIF_out_registers
  );
  end component;

component TxBTFmcIF_wb is
  port (
    rst_n_i                                  : in     std_logic;
    clk_sys_i                                : in     std_logic;
    wb_adr_i                                 : in     std_logic_vector(5 downto 0);
    wb_dat_i                                 : in     std_logic_vector(7 downto 0);
    wb_dat_o                                 : out    std_logic_vector(7 downto 0);
    wb_cyc_i                                 : in     std_logic;
    wb_sel_i                                 : in     std_logic_vector(0 downto 0);
    wb_stb_i                                 : in     std_logic;
    wb_we_i                                  : in     std_logic;
    wb_ack_o                                 : out    std_logic;
    wb_err_o                                 : out    std_logic;
    wb_rty_o                                 : out    std_logic;
    wb_stall_o                               : out    std_logic;
    regs_i                                   : in     t_TxBTFmcIF_in_registers;
    regs_o                                   : out    t_TxBTFmcIF_out_registers
  );
end component;


end BTrainFmcIF_pkg;

package body BTrainFmcIF_pkg is


end BTrainFmcIF_pkg;
