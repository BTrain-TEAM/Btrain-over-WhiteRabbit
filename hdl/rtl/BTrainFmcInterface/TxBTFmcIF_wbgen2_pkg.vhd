---------------------------------------------------------------------------------------
-- Title          : Wishbone slave core for WR BTrain Tx FMC Interface
---------------------------------------------------------------------------------------
-- File           : TxBTFmcIF_wbgen2_pkg.vhd
-- Author         : auto-generated by wbgen2 from TxBTFmcIF_wb.wb
-- Created        : Wed Nov  6 16:49:42 2019
-- Version        : 0x00000001
-- Standard       : VHDL'87
---------------------------------------------------------------------------------------
-- THIS FILE WAS GENERATED BY wbgen2 FROM SOURCE FILE TxBTFmcIF_wb.wb
-- DO NOT HAND-EDIT UNLESS IT'S ABSOLUTELY NECESSARY!
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package TxBTFmcIF_wbgen2_pkg is
  
  
  -- Input registers (user design -> WB slave)
  
  type t_TxBTFmcIF_in_registers is record
    txbtfmcif_scr_ready_i                    : std_logic;
    end record;
  
  constant c_TxBTFmcIF_in_registers_init_value: t_TxBTFmcIF_in_registers := (
    txbtfmcif_scr_ready_i => '0'
    );
    
    -- Output registers (WB slave -> user design)
    
    type t_TxBTFmcIF_out_registers is record
      txbtfmcif_ver_id_o                       : std_logic_vector(7 downto 0);
      txbtfmcif_scr_tx_o                       : std_logic;
      txbtfmcif_bheader_1_frame_type_o         : std_logic_vector(7 downto 0);
      txbtfmcif_bheader_2_sim_eff_o            : std_logic;
      txbtfmcif_bheader_2_error_o              : std_logic;
      txbtfmcif_bheader_2_c0_o                 : std_logic;
      txbtfmcif_bheader_2_zero_cycle_o         : std_logic;
      txbtfmcif_bheader_2_f_low_marker_o       : std_logic;
      txbtfmcif_bheader_2_d_low_marker_o       : std_logic;
      txbtfmcif_bheader_2_version_id_o         : std_logic_vector(1 downto 0);
      txbtfmcif_bframepayload_0_b_0_o          : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_1_b_1_o          : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_2_b_2_o          : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_3_b_3_o          : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_4_bdot_0_o       : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_5_bdot_1_o       : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_6_bdot_2_o       : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_7_bdot_3_o       : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_8_oldb_0_o       : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_9_oldb_1_o       : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_10_oldb_2_o      : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_11_oldb_3_o      : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_12_measb_0_o     : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_13_measb_1_o     : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_14_measb_2_o     : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_15_measb_3_o     : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_16_simb_0_o      : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_17_simb_1_o      : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_18_simb_2_o      : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_19_simb_3_o      : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_20_synb_0_o      : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_21_synb_1_o      : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_22_synb_2_o      : std_logic_vector(7 downto 0);
      txbtfmcif_bframepayload_23_synb_3_o      : std_logic_vector(7 downto 0);
      txbtfmcif_iframepayload_0_i_0_o          : std_logic_vector(7 downto 0);
      txbtfmcif_iframepayload_1_i_1_o          : std_logic_vector(7 downto 0);
      txbtfmcif_iframepayload_2_i_2_o          : std_logic_vector(7 downto 0);
      txbtfmcif_iframepayload_3_i_3_o          : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_0_i_0_o          : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_1_i_1_o          : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_2_i_2_o          : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_3_i_3_o          : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_4_v_0_o          : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_5_v_1_o          : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_6_v_2_o          : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_7_v_3_o          : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_8_srcid_0_o      : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_9_srcid_1_o      : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_10_srcid_2_o     : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_11_srcid_3_o     : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_12_userdata_0_o  : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_13_userdata_1_o  : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_14_userdata_2_o  : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_15_userdata_3_o  : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_16_reserved_1_0_o : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_17_reserved_1_1_o : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_18_reserved_1_2_o : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_19_reserved_1_3_o : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_20_reserved_2_0_o : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_21_reserved_2_1_o : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_22_reserved_2_2_o : std_logic_vector(7 downto 0);
      txbtfmcif_cframepayload_23_reserved_2_3_o : std_logic_vector(7 downto 0);
      end record;
    
    constant c_TxBTFmcIF_out_registers_init_value: t_TxBTFmcIF_out_registers := (
      txbtfmcif_ver_id_o => (others => '0'),
      txbtfmcif_scr_tx_o => '0',
      txbtfmcif_bheader_1_frame_type_o => (others => '0'),
      txbtfmcif_bheader_2_sim_eff_o => '0',
      txbtfmcif_bheader_2_error_o => '0',
      txbtfmcif_bheader_2_c0_o => '0',
      txbtfmcif_bheader_2_zero_cycle_o => '0',
      txbtfmcif_bheader_2_f_low_marker_o => '0',
      txbtfmcif_bheader_2_d_low_marker_o => '0',
      txbtfmcif_bheader_2_version_id_o => (others => '0'),
      txbtfmcif_bframepayload_0_b_0_o => (others => '0'),
      txbtfmcif_bframepayload_1_b_1_o => (others => '0'),
      txbtfmcif_bframepayload_2_b_2_o => (others => '0'),
      txbtfmcif_bframepayload_3_b_3_o => (others => '0'),
      txbtfmcif_bframepayload_4_bdot_0_o => (others => '0'),
      txbtfmcif_bframepayload_5_bdot_1_o => (others => '0'),
      txbtfmcif_bframepayload_6_bdot_2_o => (others => '0'),
      txbtfmcif_bframepayload_7_bdot_3_o => (others => '0'),
      txbtfmcif_bframepayload_8_oldb_0_o => (others => '0'),
      txbtfmcif_bframepayload_9_oldb_1_o => (others => '0'),
      txbtfmcif_bframepayload_10_oldb_2_o => (others => '0'),
      txbtfmcif_bframepayload_11_oldb_3_o => (others => '0'),
      txbtfmcif_bframepayload_12_measb_0_o => (others => '0'),
      txbtfmcif_bframepayload_13_measb_1_o => (others => '0'),
      txbtfmcif_bframepayload_14_measb_2_o => (others => '0'),
      txbtfmcif_bframepayload_15_measb_3_o => (others => '0'),
      txbtfmcif_bframepayload_16_simb_0_o => (others => '0'),
      txbtfmcif_bframepayload_17_simb_1_o => (others => '0'),
      txbtfmcif_bframepayload_18_simb_2_o => (others => '0'),
      txbtfmcif_bframepayload_19_simb_3_o => (others => '0'),
      txbtfmcif_bframepayload_20_synb_0_o => (others => '0'),
      txbtfmcif_bframepayload_21_synb_1_o => (others => '0'),
      txbtfmcif_bframepayload_22_synb_2_o => (others => '0'),
      txbtfmcif_bframepayload_23_synb_3_o => (others => '0'),
      txbtfmcif_iframepayload_0_i_0_o => (others => '0'),
      txbtfmcif_iframepayload_1_i_1_o => (others => '0'),
      txbtfmcif_iframepayload_2_i_2_o => (others => '0'),
      txbtfmcif_iframepayload_3_i_3_o => (others => '0'),
      txbtfmcif_cframepayload_0_i_0_o => (others => '0'),
      txbtfmcif_cframepayload_1_i_1_o => (others => '0'),
      txbtfmcif_cframepayload_2_i_2_o => (others => '0'),
      txbtfmcif_cframepayload_3_i_3_o => (others => '0'),
      txbtfmcif_cframepayload_4_v_0_o => (others => '0'),
      txbtfmcif_cframepayload_5_v_1_o => (others => '0'),
      txbtfmcif_cframepayload_6_v_2_o => (others => '0'),
      txbtfmcif_cframepayload_7_v_3_o => (others => '0'),
      txbtfmcif_cframepayload_8_srcid_0_o => (others => '0'),
      txbtfmcif_cframepayload_9_srcid_1_o => (others => '0'),
      txbtfmcif_cframepayload_10_srcid_2_o => (others => '0'),
      txbtfmcif_cframepayload_11_srcid_3_o => (others => '0'),
      txbtfmcif_cframepayload_12_userdata_0_o => (others => '0'),
      txbtfmcif_cframepayload_13_userdata_1_o => (others => '0'),
      txbtfmcif_cframepayload_14_userdata_2_o => (others => '0'),
      txbtfmcif_cframepayload_15_userdata_3_o => (others => '0'),
      txbtfmcif_cframepayload_16_reserved_1_0_o => (others => '0'),
      txbtfmcif_cframepayload_17_reserved_1_1_o => (others => '0'),
      txbtfmcif_cframepayload_18_reserved_1_2_o => (others => '0'),
      txbtfmcif_cframepayload_19_reserved_1_3_o => (others => '0'),
      txbtfmcif_cframepayload_20_reserved_2_0_o => (others => '0'),
      txbtfmcif_cframepayload_21_reserved_2_1_o => (others => '0'),
      txbtfmcif_cframepayload_22_reserved_2_2_o => (others => '0'),
      txbtfmcif_cframepayload_23_reserved_2_3_o => (others => '0')
      );
    function "or" (left, right: t_TxBTFmcIF_in_registers) return t_TxBTFmcIF_in_registers;
    function f_x_to_zero (x:std_logic) return std_logic;
    function f_x_to_zero (x:std_logic_vector) return std_logic_vector;
end package;

package body TxBTFmcIF_wbgen2_pkg is
function f_x_to_zero (x:std_logic) return std_logic is
begin
if x = '1' then
return '1';
else
return '0';
end if;
end function;
function f_x_to_zero (x:std_logic_vector) return std_logic_vector is
variable tmp: std_logic_vector(x'length-1 downto 0);
begin
for i in 0 to x'length-1 loop
if(x(i) = '1') then
tmp(i):= '1';
else
tmp(i):= '0';
end if; 
end loop; 
return tmp;
end function;
function "or" (left, right: t_TxBTFmcIF_in_registers) return t_TxBTFmcIF_in_registers is
variable tmp: t_TxBTFmcIF_in_registers;
begin
tmp.txbtfmcif_scr_ready_i := f_x_to_zero(left.txbtfmcif_scr_ready_i) or f_x_to_zero(right.txbtfmcif_scr_ready_i);
return tmp;
end function;
end package body;
