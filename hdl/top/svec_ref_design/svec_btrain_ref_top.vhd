-------------------------------------------------------------------------------
-- Title      : BTrain reference design for SVEC
-- Project    : BTrain over White Rabbit
-- URL        : https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
-------------------------------------------------------------------------------
-- File       : svec_btrain_ref_top.vhd
-- Author(s)  : Dimitrios Lampridis  <dimitrios.lampridis@cern.ch>
-- Company    : CERN (BE-CO-HT)
-- Created    : 2017-04-10
-- Last update: 2017-05-10
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: Top-level file for the BTrain reference design on the SVEC.
--
-- This is the BTrain reference top HDL that instantiates the BTrain
-- transceiver, the WR streamers and the WR PTP Core together with its
-- peripherals, to be run on a SVEC card.
--
-- SVEC:  http://www.ohwr.org/projects/svec/
--
-------------------------------------------------------------------------------
-- Copyright (c) 2017 CERN
-------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.vme64x_pkg.all;
use work.wr_board_pkg.all;
use work.wr_svec_pkg.all;
use work.streamers_pkg.all;

use work.BTrainFrameTransceiver_pkg.all;
use work.BTrainFrame_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity svec_btrain_ref_top is
  generic (
    -- setting g_simulation to TRUE will speed up some initialization processes
    g_simulation  : integer := 0;
    -- setting g_dpram_initf to file path will result in syntesis/simulation using the
    -- content of this file to run LM32 microprocessor
    -- setting g_dpram_init to empty string (i.e."") will result in synthesis/simulation
    -- with empty RAM for the LM32 (it will not work until code is loaded)
    -- NOTE: the path is correct when used from the synthesis folder (this is where
    --       ISE calls the function to find the file, the path is not correct for where
    --       this file is stored, i.e. in the top/ folder)
    g_dpram_initf : string  := "../../ip_cores/wr-cores/bin/wrpc/wrc_phy8.bram"
    );
  port (
    ---------------------------------------------------------------------------
    -- Clocks/resets
    ---------------------------------------------------------------------------

    -- Reset from system fpga
    rst_n_i : in std_logic;

    -- Local oscillators
    clk_20m_vcxo_i : in std_logic;                -- 20MHz VCXO clock

    clk_125m_pllref_p_i : in std_logic;           -- 125 MHz PLL reference
    clk_125m_pllref_n_i : in std_logic;

    clk_125m_gtp_n_i : in std_logic;              -- 125 MHz GTP reference
    clk_125m_gtp_p_i : in std_logic;

    ---------------------------------------------------------------------------
    -- VME interface
    ---------------------------------------------------------------------------

    vme_write_n_i    : in    std_logic;
    vme_sysreset_n_i : in    std_logic;
    vme_retry_oe_o   : out   std_logic;
    vme_retry_n_o    : out   std_logic;
    vme_lword_n_b    : inout std_logic;
    vme_iackout_n_o  : out   std_logic;
    vme_iackin_n_i   : in    std_logic;
    vme_iack_n_i     : in    std_logic;
    vme_gap_i        : in    std_logic;
    vme_dtack_oe_o   : out   std_logic;
    vme_dtack_n_o    : out   std_logic;
    vme_ds_n_i       : in    std_logic_vector(1 downto 0);
    vme_data_oe_n_o  : out   std_logic;
    vme_data_dir_o   : out   std_logic;
    vme_berr_o       : out   std_logic;
    vme_as_n_i       : in    std_logic;
    vme_addr_oe_n_o  : out   std_logic;
    vme_addr_dir_o   : out   std_logic;
    vme_irq_o        : out   std_logic_vector(7 downto 1);
    vme_ga_i         : in    std_logic_vector(4 downto 0);
    vme_data_b       : inout std_logic_vector(31 downto 0);
    vme_am_i         : in    std_logic_vector(5 downto 0);
    vme_addr_b       : inout std_logic_vector(31 downto 1);

    ---------------------------------------------------------------------------
    -- SPI interfaces to DACs
    ---------------------------------------------------------------------------

    pll20dac_din_o    : out std_logic;
    pll20dac_sclk_o   : out std_logic;
    pll20dac_sync_n_o : out std_logic;
    pll25dac_din_o    : out std_logic;
    pll25dac_sclk_o   : out std_logic;
    pll25dac_sync_n_o : out std_logic;

    ---------------------------------------------------------------------------
    -- SFP I/O for transceiver
    ---------------------------------------------------------------------------

    sfp_txp_o         : out   std_logic;
    sfp_txn_o         : out   std_logic;
    sfp_rxp_i         : in    std_logic;
    sfp_rxn_i         : in    std_logic;
    sfp_mod_def0_i    : in    std_logic;          -- sfp detect
    sfp_mod_def1_b    : inout std_logic;          -- scl
    sfp_mod_def2_b    : inout std_logic;          -- sda
    sfp_rate_select_o : out   std_logic;
    sfp_tx_fault_i    : in    std_logic;
    sfp_tx_disable_o  : out   std_logic;
    sfp_los_i         : in    std_logic;

    ---------------------------------------------------------------------------
    -- Carrier I2C EEPROM
    ---------------------------------------------------------------------------

    carrier_scl_b : inout std_logic;
    carrier_sda_b : inout std_logic;

    ---------------------------------------------------------------------------
    -- Onewire interface
    ---------------------------------------------------------------------------

    onewire_b : inout std_logic;

    ---------------------------------------------------------------------------
    -- UART
    ---------------------------------------------------------------------------

    uart_rxd_i : in  std_logic;
    uart_txd_o : out std_logic;

    ---------------------------------------------------------------------------
    -- SPI (flash is connected to SFPGA and routed to AFPGA
    -- once the boot process is complete)
    ---------------------------------------------------------------------------

    spi_sclk_o : out std_logic;
    spi_ncs_o  : out std_logic;
    spi_mosi_o : out std_logic;
    spi_miso_i : in  std_logic;

    ---------------------------------------------------------------------------
    -- Carrier front panel LEDs and IOs
    ---------------------------------------------------------------------------

    fp_led_line_oen_o : out std_logic_vector(1 downto 0);
    fp_led_line_o     : out std_logic_vector(1 downto 0);
    fp_led_column_o   : out std_logic_vector(3 downto 0);

    fp_gpio1_o      : out std_logic;              -- tx_valid signal
    fp_gpio2_o      : out std_logic;              -- rx_valid signal
    fp_gpio3_o      : out std_logic;              -- WR PPS output
    fp_gpio4_o      : out std_logic;              -- WR TM valid
    fp_term_en_o    : out std_logic_vector(4 downto 1);
    fp_gpio1_a2b_o  : out std_logic;
    fp_gpio2_a2b_o  : out std_logic;
    fp_gpio34_a2b_o : out std_logic);

end entity svec_btrain_ref_top;

architecture top of svec_btrain_ref_top is

  -----------------------------------------------------------------------------
  -- Constants
  -----------------------------------------------------------------------------

  -- Number of masters on the wishbone crossbar
  constant c_NUM_WB_MASTERS : integer := 1;

  -- Number of slaves on the primary wishbone crossbar
  constant c_NUM_WB_SLAVES : integer := 2;

  -- Primary Wishbone master(s) offsets
  constant c_WB_MASTER_VME : integer := 0;

  -- Primary Wishbone slave(s) offsets
  constant c_WB_SLAVE_WRC    : integer := 0;
  constant c_WB_SLAVE_BTRAIN : integer := 1;

  -- sdb header address on primary crossbar
  constant c_SDB_ADDRESS : t_wishbone_address := x"00000000";

  constant c_xwb_btrain_sdb : t_sdb_device := (
    abi_class     => x"0000",
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"4",                        -- 32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"00000000000000FF",
      product     => (
        vendor_id => x"000000000000CE42",         -- CERN
        device_id => x"00000604",
        version   => x"00000002",
        date      => x"20170418",
        name      => "BTrainFrameTxRx    ")));

  -- f_xwb_bridge_manual_sdb(size, sdb_addr)
  -- Note: sdb_addr is the sdb records address relative to the bridge base address
  constant c_wrc_bridge_sdb : t_sdb_bridge :=
    f_xwb_bridge_manual_sdb(x"0003ffff", x"00030000");

  -- Primary wishbone crossbar layout
  constant c_WB_LAYOUT : t_sdb_record_array(c_NUM_WB_SLAVES - 1 downto 0) := (
    c_WB_SLAVE_BTRAIN => f_sdb_embed_device(c_xwb_btrain_sdb, x"00001200"),
    c_WB_SLAVE_WRC    => f_sdb_embed_bridge(c_wrc_bridge_sdb, x"00040000"));

  -----------------------------------------------------------------------------
  -- Signals
  -----------------------------------------------------------------------------

  -- Wishbone buse(s) from masters attached to crossbar
  signal cnx_master_out : t_wishbone_master_out_array(c_NUM_WB_MASTERS-1 downto 0);
  signal cnx_master_in  : t_wishbone_master_in_array(c_NUM_WB_MASTERS-1 downto 0);

  -- Wishbone buse(s) to slaves attached to crossbar
  signal cnx_slave_out : t_wishbone_slave_out_array(c_NUM_WB_SLAVES-1 downto 0);
  signal cnx_slave_in  : t_wishbone_slave_in_array(c_NUM_WB_SLAVES-1 downto 0);

  -- clock and reset
  signal areset_n       : std_logic;
  signal clk_sys_62m5   : std_logic;
  signal rst_sys_62m5_n : std_logic;
  signal clk_ref_125m   : std_logic;
  signal rst_ref_125m_n : std_logic;

  -- I2C EEPROM
  signal eeprom_sda_in  : std_logic;
  signal eeprom_sda_out : std_logic;
  signal eeprom_scl_in  : std_logic;
  signal eeprom_scl_out : std_logic;

  -- VME
  signal vme_data_b_out    : std_logic_vector(31 downto 0);
  signal vme_addr_b_out    : std_logic_vector(31 downto 1);
  signal vme_lword_n_b_out : std_logic;
  signal Vme_data_dir_int  : std_logic;
  signal vme_addr_dir_int  : std_logic;
  signal vme_ga            : std_logic_vector(5 downto 0);
  signal vme_berr_n        : std_logic;
  signal vme_irq_n         : std_logic_vector(7 downto 1);

  -- SFP
  signal sfp_sda_in  : std_logic;
  signal sfp_sda_out : std_logic;
  signal sfp_scl_in  : std_logic;
  signal sfp_scl_out : std_logic;

  -- OneWire
  signal onewire_data : std_logic;
  signal onewire_oe   : std_logic;

  -- LEDs and GPIO
  signal pps_led     : std_logic;
  signal pps_led_d   : std_logic;
  signal rxv_led_d   : std_logic;
  signal svec_led    : std_logic_vector(15 downto 0);
  signal wr_led_link : std_logic;
  signal wr_led_act  : std_logic;
  signal tx_valid_ext: std_logic;
  signal rx_valid_ext: std_logic;

  -- WR Streamers <---> BTrain
  signal tx_data     : std_logic_vector(c_BTrain_streamer_data_width-1 downto 0);
  signal tx_valid    : std_logic;
  signal tx_dreq     : std_logic;
  signal tx_last_p1  : std_logic;
  signal tx_flush_p1 : std_logic;
  -- rx
  signal rx_data     : std_logic_vector(c_BTrain_streamer_data_width-1 downto 0);
  signal rx_valid    : std_logic;
  signal rx_first_p1 : std_logic;
  signal rx_dreq     : std_logic;
  signal rx_last_p1  : std_logic;

  -- BTrain <---> BTrain application (e.g. on BTrainFMC)
  signal tx_ready            : std_logic;
  signal tx_TransmitFrame_p1 : std_logic;
  signal tx_FrameHeader    : t_FrameHeader;
  signal tx_BFramePayloads : t_BFramePayload;
  signal tx_IFramePayloads : t_IFramePayload;
  signal tx_CFramePayloads : t_CFramePayload;
  signal rx_FrameHeader      : t_FrameHeader;
  signal rx_BFramePayloads   : t_BFramePayload;
  signal rx_IFramePayloads   : t_IFramePayload;
  signal rx_CFramePayloads   : t_CFramePayload;
  signal rx_Frame_valid_pX   : std_logic;
  signal rx_Frame_typeID     : std_logic_vector(c_type_ID_size-1 downto 0);

  -- SIM <---> BTrain application
  signal sim_tx_FrameHeader_version_id   : std_logic_vector(1 downto 0);
  signal sim_tx_FrameHeader_d_low_marker : std_logic;
  signal sim_tx_FrameHeader_f_low_marker : std_logic;
  signal sim_tx_FrameHeader_zero_cycle   : std_logic;
  signal sim_tx_FrameHeader_C0           : std_logic;
  signal sim_tx_FrameHeader_error        : std_logic;
  signal sim_tx_FrameHeader_sim_eff      : std_logic;
  signal sim_tx_FrameHeader_frame_type   : std_logic_vector(c_type_ID_size-1 downto 0);
  signal sim_tx_BFramePayload_B          : std_logic_vector(31 downto 0);
  signal sim_tx_BFramePayload_Bdot       : std_logic_vector(31 downto 0);
  signal sim_tx_BFramePayload_oldB       : std_logic_vector(31 downto 0);
  signal sim_tx_BFramePayload_measB      : std_logic_vector(31 downto 0);
  signal sim_tx_BFramePayload_simB       : std_logic_vector(31 downto 0);
  signal sim_tx_BFramePayload_synB       : std_logic_vector(31 downto 0);
  signal sim_tx_IFramePayload_I          : std_logic_vector(31 downto 0);
  signal sim_tx_TransmitFrame_p1         : std_logic;
  signal sim_tx_CFramePayload_I          : std_logic_vector(31 downto 0);
  signal sim_tx_CFramePayload_V          : std_logic_vector(31 downto 0);
  signal sim_tx_CFramePayload_MSrcId     : std_logic_vector(15 downto 0);
  signal sim_tx_CFramePayload_CSrcId     : std_logic_vector(15 downto 0);
  signal sim_tx_CFramePayload_UserData   : std_logic_vector(31 downto 0);
  signal sim_tx_CFramePayload_Reserved_1 : std_logic_vector(31 downto 0);
  signal sim_tx_CFramePayload_Reserved_2 : std_logic_vector(31 downto 0);
  signal sim_tx_ready                    : std_logic;
  signal sim_rx_FrameHeader_version_id   : std_logic_vector(1 downto 0);
  signal sim_rx_FrameHeader_d_low_marker : std_logic;
  signal sim_rx_FrameHeader_f_low_marker : std_logic;
  signal sim_rx_FrameHeader_zero_cycle   : std_logic;
  signal sim_rx_FrameHeader_C0           : std_logic;
  signal sim_rx_FrameHeader_error        : std_logic;
  signal sim_rx_FrameHeader_sim_eff      : std_logic;
  signal sim_rx_FrameHeader_frame_type   : std_logic_vector(c_type_ID_size-1 downto 0);
  signal sim_rx_BFramePayload_B          : std_logic_vector(31 downto 0);
  signal sim_rx_BFramePayload_Bdot       : std_logic_vector(31 downto 0);
  signal sim_rx_BFramePayload_oldB       : std_logic_vector(31 downto 0);
  signal sim_rx_BFramePayload_measB      : std_logic_vector(31 downto 0);
  signal sim_rx_BFramePayload_simB       : std_logic_vector(31 downto 0);
  signal sim_rx_BFramePayload_synB       : std_logic_vector(31 downto 0);
  signal sim_rx_IFramePayload_I          : std_logic_vector(31 downto 0);
  signal sim_rx_CFramePayload_I          : std_logic_vector(31 downto 0);
  signal sim_rx_CFramePayload_V          : std_logic_vector(31 downto 0);
  signal sim_rx_CFramePayload_MSrcId     : std_logic_vector(15 downto 0);
  signal sim_rx_CFramePayload_CSrcId     : std_logic_vector(15 downto 0);
  signal sim_rx_CFramePayload_UserData   : std_logic_vector(31 downto 0);
  signal sim_rx_CFramePayload_Reserved_1 : std_logic_vector(31 downto 0);
  signal sim_rx_CFramePayload_Reserved_2 : std_logic_vector(31 downto 0);
  signal sim_rx_Frame_valid_pX           : std_logic;
  signal sim_rx_Frame_typeID             : std_logic_vector(c_type_ID_size-1 downto 0);

begin  -- architecture top

  ------------------------------------------------------------------------------
  -- System reset
  ------------------------------------------------------------------------------

  -- logic AND of all async reset sources (active low)
  areset_n <= vme_sysreset_n_i and rst_n_i;

  -----------------------------------------------------------------------------
  -- Primary wishbone Crossbar
  -----------------------------------------------------------------------------

  cmp_sdb_crossbar : xwb_sdb_crossbar
    generic map (
      g_num_masters => c_NUM_WB_MASTERS,
      g_num_slaves  => c_NUM_WB_SLAVES,
      g_registered  => TRUE,
      g_wraparound  => TRUE,
      g_layout      => c_WB_LAYOUT,
      g_sdb_addr    => c_SDB_ADDRESS)
    port map (
      clk_sys_i => clk_sys_62m5,
      rst_n_i   => rst_sys_62m5_n,
      slave_i   => cnx_master_out,
      slave_o   => cnx_master_in,
      master_i  => cnx_slave_out,
      master_o  => cnx_slave_in);

  -----------------------------------------------------------------------------
  -- VME64x Core (WB Master #1)
  -----------------------------------------------------------------------------

  cmp_vme_core : xvme64x_core
    generic map (
      g_CLOCK_PERIOD    => 16,
      g_DECODE_AM       => True,
      g_USER_CSR_EXT    => False,
      g_WB_GRANULARITY  => BYTE,
      g_MANUFACTURER_ID => c_CERN_ID,
      g_BOARD_ID        => c_SVEC_ID,
      g_REVISION_ID     => c_SVEC_REVISION_ID,
      g_PROGRAM_ID      => c_SVEC_PROGRAM_ID)
    port map (
      clk_i           => clk_sys_62m5,
      rst_n_i         => rst_sys_62m5_n,
      vme_i.as_n      => vme_as_n_i,
      vme_i.rst_n     => vme_sysreset_n_i,
      vme_i.write_n   => vme_write_n_i,
      vme_i.am        => vme_am_i,
      vme_i.ds_n      => vme_ds_n_i,
      vme_i.ga        => vme_ga,
      vme_i.lword_n   => vme_lword_n_b,
      vme_i.addr      => vme_addr_b,
      vme_i.data      => vme_data_b,
      vme_i.iack_n    => vme_iack_n_i,
      vme_i.iackin_n  => vme_iackin_n_i,
      vme_o.berr_n    => vme_berr_n,
      vme_o.dtack_n   => vme_dtack_n_o,
      vme_o.retry_n   => vme_retry_n_o,
      vme_o.retry_oe  => vme_retry_oe_o,
      vme_o.lword_n   => vme_lword_n_b_out,
      vme_o.data      => vme_data_b_out,
      vme_o.addr      => vme_addr_b_out,
      vme_o.irq_n     => vme_irq_n,
      vme_o.iackout_n => vme_iackout_n_o,
      vme_o.dtack_oe  => vme_dtack_oe_o,
      vme_o.data_dir  => vme_data_dir_int,
      vme_o.data_oe_n => vme_data_oe_n_o,
      vme_o.addr_dir  => vme_addr_dir_int,
      vme_o.addr_oe_n => vme_addr_oe_n_o,
      wb_o            => cnx_master_out(c_WB_MASTER_VME),
      wb_i            => cnx_master_in(c_WB_MASTER_VME));

  vme_ga <= vme_gap_i & vme_ga_i;
  vme_berr_o <= not vme_berr_n;
  vme_irq_o  <= not vme_irq_n;

  -- VME tri-state buffers
  vme_data_b    <= vme_data_b_out    when vme_data_dir_int = '1' else (others => 'Z');
  vme_addr_b    <= vme_addr_b_out    when vme_addr_dir_int = '1' else (others => 'Z');
  vme_lword_n_b <= vme_lword_n_b_out when vme_addr_dir_int = '1' else 'Z';

  vme_addr_dir_o <= vme_addr_dir_int;
  vme_data_dir_o <= vme_data_dir_int;

  -----------------------------------------------------------------------------
  -- The WR PTP core board package (WB Slave + WB Master #2 (Etherbone))
  -----------------------------------------------------------------------------

  cmp_xwrc_board_svec : xwrc_board_svec
    generic map (
      g_simulation                => g_simulation,
      g_with_external_clock_input => FALSE,
      g_dpram_initf               => g_dpram_initf,
      g_streamers_op_mode         => TX_AND_RX,
      g_tx_streamer_params        => c_tx_streamer_params_btrain,
      g_rx_streamer_params        => c_rx_streamer_params_btrain,
      g_fabric_iface              => STREAMERS)
    port map (
      clk_20m_vcxo_i      => clk_20m_vcxo_i,
      clk_125m_pllref_p_i => clk_125m_pllref_p_i,
      clk_125m_pllref_n_i => clk_125m_pllref_n_i,
      clk_125m_gtp_n_i    => clk_125m_gtp_n_i,
      clk_125m_gtp_p_i    => clk_125m_gtp_p_i,
      areset_n_i          => areset_n,
      clk_sys_62m5_o      => clk_sys_62m5,
      clk_ref_125m_o      => clk_ref_125m,
      rst_sys_62m5_n_o    => rst_sys_62m5_n,
      rst_ref_125m_n_o    => rst_ref_125m_n,
      pll20dac_din_o      => pll20dac_din_o,
      pll20dac_sclk_o     => pll20dac_sclk_o,
      pll20dac_sync_n_o   => pll20dac_sync_n_o,
      pll25dac_din_o      => pll25dac_din_o,
      pll25dac_sclk_o     => pll25dac_sclk_o,
      pll25dac_sync_n_o   => pll25dac_sync_n_o,
      sfp_txp_o           => sfp_txp_o,
      sfp_txn_o           => sfp_txn_o,
      sfp_rxp_i           => sfp_rxp_i,
      sfp_rxn_i           => sfp_rxn_i,
      sfp_det_i           => sfp_mod_def0_i,
      sfp_sda_i           => sfp_sda_in,
      sfp_sda_o           => sfp_sda_out,
      sfp_scl_i           => sfp_scl_in,
      sfp_scl_o           => sfp_scl_out,
      sfp_rate_select_o   => sfp_rate_select_o,
      sfp_tx_fault_i      => sfp_tx_fault_i,
      sfp_tx_disable_o    => sfp_tx_disable_o,
      sfp_los_i           => sfp_los_i,
      eeprom_sda_i        => eeprom_sda_in,
      eeprom_sda_o        => eeprom_sda_out,
      eeprom_scl_i        => eeprom_scl_in,
      eeprom_scl_o        => eeprom_scl_out,
      onewire_i           => onewire_data,
      onewire_oen_o       => onewire_oe,
      uart_rxd_i          => uart_rxd_i,
      uart_txd_o          => uart_txd_o,
      spi_sclk_o          => spi_sclk_o,
      spi_ncs_o           => spi_ncs_o,
      spi_mosi_o          => spi_mosi_o,
      spi_miso_i          => spi_miso_i,
      wb_slave_o          => cnx_slave_out(c_WB_SLAVE_WRC),
      wb_slave_i          => cnx_slave_in(c_WB_SLAVE_WRC),
      pps_p_o             => fp_gpio3_o,
      tm_time_valid_o     => fp_gpio4_o,
      pps_led_o           => pps_led,
      led_link_o          => wr_led_link,
      led_act_o           => wr_led_act,
      wrs_tx_data_i       => tx_data,
      wrs_tx_valid_i      => tx_valid,
      wrs_tx_dreq_o       => tx_dreq,
      wrs_tx_last_i       => tx_last_p1,
      wrs_tx_flush_i      => tx_flush_p1,
      wrs_tx_cfg_i        => c_tx_streamer_cfg_btrain,
      wrs_rx_first_o      => rx_first_p1,
      wrs_rx_last_o       => rx_last_p1,
      wrs_rx_data_o       => rx_data,
      wrs_rx_valid_o      => rx_valid,
      wrs_rx_dreq_i       => rx_dreq,
      wrs_rx_cfg_i        => c_rx_streamer_cfg_btrain
      );

  -- tri-state Carrier EEPROM
  carrier_sda_b <= '0' when (eeprom_sda_out = '0') else 'Z';
  eeprom_sda_in <= carrier_sda_b;
  carrier_scl_b <= '0' when (eeprom_scl_out = '0') else 'Z';
  eeprom_scl_in <= carrier_scl_b;

  -- Tristates for SFP EEPROM
  sfp_mod_def1_b <= '0' when sfp_scl_out = '0' else 'Z';
  sfp_mod_def2_b <= '0' when sfp_sda_out = '0' else 'Z';
  sfp_scl_in     <= sfp_mod_def1_b;
  sfp_sda_in     <= sfp_mod_def2_b;

  -- tri-state onewire access
  onewire_b    <= '0' when (onewire_oe = '1') else 'Z';
  onewire_data <= onewire_b;

  -------------------------------------------------------------------------------------------
  -- BTrain frames transceiver
  -------------------------------------------------------------------------------------------
  cmp_btrain : BTrainFrameTransceiver
    generic map(
      g_rx_BframeType     => c_ID_ALL,  -- accept all types of BTrain frame - only debugging
      g_use_wb_config     => TRUE,
      g_slave_mode        => CLASSIC,
      g_slave_granularity => BYTE)
    port map(
      clk_i   => clk_sys_62m5,
      rst_n_i => rst_sys_62m5_n,

      tx_data_o     => tx_data,
      tx_valid_o    => tx_valid,
      tx_dreq_i     => tx_dreq,
      tx_last_p1_o  => tx_last_p1,
      tx_flush_p1_o => tx_flush_p1,
      -- rx
      rx_data_i     => rx_data,
      rx_valid_i    => rx_valid,
      rx_first_p1_i => rx_first_p1,
      rx_dreq_o     => rx_dreq,
      rx_last_p1_i  => rx_last_p1,

      rx_FrameHeader_o    => rx_FrameHeader,
      rx_BFramePayloads_o => rx_BFramePayloads,
      rx_IFramePayloads_o => rx_IFramePayloads,
      rx_CframePayloads_o => rx_CFramePayloads,
      rx_Frame_valid_pX_o => rx_Frame_valid_pX,
      rx_Frame_typeID_o   => rx_Frame_typeID,

      ready_o               => tx_ready,
      tx_TransmitFrame_p1_i => tx_TransmitFrame_p1,
      tx_FrameHeader_i      => tx_FrameHeader,
      tx_BFramePayloads_i   => tx_BFramePayloads,
      tx_IFramePayloads_i   => tx_IFramePayloads,
      tx_CFramePayloads_i   => tx_CFramePayloads,

      wb_slave_i => cnx_slave_in(c_WB_SLAVE_BTRAIN),
      wb_slave_o => cnx_slave_out(c_WB_SLAVE_BTRAIN)
      );

  --fake data
  gen_fake_data: if g_simulation = 0 generate
    tx_TransmitFrame_p1 <= '0';
    tx_FrameHeader      <= c_FrameHeader_dummy;
    tx_BFramePayloads   <= c_BFramePayload_dummy;
    tx_IFramePayloads   <= c_IFramePayload_zero;
    tx_CFramePayloads   <= c_CFramePayload_zero;
  end generate gen_fake_data;

  -- Simulation connection points. Since binding to VHDL records in SV is not well-defined,
  -- we break the records into std_logic signals.
  gen_sim_bind : if g_simulation = 1 generate
    tx_FrameHeader.version_id   <= sim_tx_FrameHeader_version_id;
    tx_FrameHeader.d_low_marker <= sim_tx_FrameHeader_d_low_marker;
    tx_FrameHeader.f_low_marker <= sim_tx_FrameHeader_f_low_marker;
    tx_FrameHeader.zero_cycle   <= sim_tx_FrameHeader_zero_cycle;
    tx_FrameHeader.C0           <= sim_tx_FrameHeader_C0;
    tx_FrameHeader.error        <= sim_tx_FrameHeader_error;
    tx_FrameHeader.sim_eff      <= sim_tx_FrameHeader_sim_eff;
    tx_FrameHeader.frame_type   <= sim_tx_FrameHeader_frame_type;
    tx_BFramePayloads.B         <= sim_tx_BFramePayload_B;
    tx_BFramePayloads.Bdot      <= sim_tx_BFramePayload_Bdot;
    tx_BFramePayloads.oldB      <= sim_tx_BFramePayload_oldB;
    tx_BFramePayloads.measB     <= sim_tx_BFramePayload_measB;
    tx_BFramePayloads.simB      <= sim_tx_BFramePayload_simB;
    tx_BFramePayloads.synB      <= sim_tx_BFramePayload_synB;
    tx_IFramePayloads.I         <= sim_tx_IFramePayload_I;
    tx_TransmitFrame_p1         <= sim_tx_TransmitFrame_p1;
    tx_CFramePayloads.I         <= sim_tx_CFramePayload_I;
    tx_CFramePayloads.V         <= sim_tx_CFramePayload_V;
    tx_CFramePayloads.MSrcId    <= sim_tx_CFramePayload_MSrcId;
    tx_CFramePayloads.CSrcId    <= sim_tx_CFramePayload_CSrcId;
    tx_CFramePayloads.UserData  <= sim_tx_CFramePayload_UserData;
    tx_CFramePayloads.Reserved_1<= sim_tx_CFramePayload_Reserved_1;
    tx_CFramePayloads.Reserved_2<= sim_tx_CFramePayload_Reserved_2;
    sim_tx_ready                <= tx_ready;

    sim_rx_FrameHeader_version_id   <= rx_FrameHeader.version_id;
    sim_rx_FrameHeader_d_low_marker <= rx_FrameHeader.d_low_marker;
    sim_rx_FrameHeader_f_low_marker <= rx_FrameHeader.f_low_marker;
    sim_rx_FrameHeader_zero_cycle   <= rx_FrameHeader.zero_cycle;
    sim_rx_FrameHeader_C0           <= rx_FrameHeader.C0;
    sim_rx_FrameHeader_error        <= rx_FrameHeader.error;
    sim_rx_FrameHeader_sim_eff      <= rx_FrameHeader.sim_eff;
    sim_rx_FrameHeader_frame_type   <= rx_FrameHeader.frame_type;
    sim_rx_BFramePayload_B          <= rx_BFramePayloads.B;
    sim_rx_BFramePayload_Bdot       <= rx_BFramePayloads.Bdot;
    sim_rx_BFramePayload_oldB       <= rx_BFramePayloads.oldB;
    sim_rx_BFramePayload_measB      <= rx_BFramePayloads.measB;
    sim_rx_BFramePayload_simB       <= rx_BFramePayloads.simB;
    sim_rx_BFramePayload_synB       <= rx_BFramePayloads.synB;
    sim_rx_IFramePayload_I          <= rx_IFramePayloads.I;
    sim_rx_CFramePayload_I          <= rx_CFramePayloads.I;
    sim_rx_CFramePayload_V          <= rx_CFramePayloads.V;
    sim_rx_CFramePayload_MSrcId     <= rx_CFramePayloads.MSrcId;
    sim_rx_CFramePayload_CSrcId     <= rx_CFramePayloads.CSrcId;
    sim_rx_CFramePayload_UserData   <= rx_CFramePayloads.UserData;
    sim_rx_CFramePayload_Reserved_1 <= rx_CFramePayloads.Reserved_1;
    sim_rx_CFramePayload_Reserved_2 <= rx_CFramePayloads.Reserved_2;
    sim_rx_Frame_valid_pX           <= rx_Frame_valid_pX;
    sim_rx_Frame_typeID             <= rx_Frame_typeID;
  end generate gen_sim_bind;

  -------------------------------- Register TX/RX pulses for DIO -----------------------------
  -- Register the tx/rx valid signal at the output
  -- 1) rx_valid is registered to avoid gliches as it is combinatorial
  -- 2) tx_valid is registered to match the additional latency of a flip-flop
  p_register_txrx_valid: process (clk_sys_62m5)
  begin
    if rising_edge (clk_sys_62m5) then
      if rst_sys_62m5_n = '0' then
        tx_valid_ext <= '0';
        rx_valid_ext <= '0';
      else
        tx_valid_ext <= tx_valid;
        rx_valid_ext <= rx_valid;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Carrier front panel LEDs and LEMOs
  ------------------------------------------------------------------------------

  fp_gpio1_o <= tx_valid_ext;
  fp_gpio2_o <= rx_valid_ext;

  -- pps_p signal from the WR core is 8ns- (single clk_ref cycle) wide. This is
  -- too short to drive outputs such as LEDs. Let's extend its length to some
  -- human-noticeable value
  U_Extend_PPS : gc_extend_pulse
    generic map (
      g_width => 10000000)  -- output length: 10000000x8ns = 80 ms.

    port map (
      clk_i      => clk_ref_125m,
      rst_n_i    => rst_ref_125m_n,
      pulse_i    => pps_led,
      extended_o => pps_led_d);

  U_Extend_RX_VALID : gc_extend_pulse
    generic map (
      g_width => 5000000)  -- output length: 5000000x16ns = 80 ms.

    port map (
      clk_i      => clk_sys_62m5,
      rst_n_i    => rst_sys_62m5_n,
      pulse_i    => rx_valid,
      extended_o => rxv_led_d);

  cmp_led_controller : gc_bicolor_led_ctrl
    generic map(
      g_nb_column    => 4,
      g_nb_line      => 2,
      g_clk_freq     => 62500000,                 -- in Hz
      g_refresh_rate => 250                       -- in Hz
      )
    port map(
      rst_n_i => rst_sys_62m5_n,
      clk_i   => clk_sys_62m5,

      led_intensity_i => "1100100",               -- in %

      led_state_i => svec_led,

      column_o   => fp_led_column_o,
      line_o     => fp_led_line_o,
      line_oen_o => fp_led_line_oen_o);

  -- LED 1
  svec_led(1 downto 0) <= c_led_green when wr_led_link = '1' else c_led_off;

  -- LED 4
  svec_led(7 downto 6) <= c_led_green when pps_led_d = '1' else c_led_off;

  -- LED 5
  svec_led(9 downto 8) <= c_led_red_green when wr_led_act = '1' else c_led_off;

  -- LED 8
  svec_led(15 downto 14) <= c_led_green when rxv_led_d = '1' else c_led_off;

  -- unused LEDs
  svec_led(5 downto 2)   <= (others => '0');
  svec_led(13 downto 10) <= (others => '0');

  -- Front panel IO configuration
  fp_term_en_o    <= (others => '0');
  fp_gpio1_a2b_o  <= '1';
  fp_gpio2_a2b_o  <= '1';
  fp_gpio34_a2b_o <= '1';

end architecture top;
