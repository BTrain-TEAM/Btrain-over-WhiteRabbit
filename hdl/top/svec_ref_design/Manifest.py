fetchto = "../../ip_cores"

modules = {
    "local" : [
        "../../rtl/",
        "../../ip_cores/wr-cores", 
        "../../ip_cores/wr-cores/board/svec",
        "../../ip_cores/general-cores",
        "../../ip_cores/gn4124-core",
        "../../ip_cores/vme64x-core",  
        "../../ip_cores/etherbone-core",
    ],
}

files = [
    "svec_btrain_ref_top.vhd",
]
