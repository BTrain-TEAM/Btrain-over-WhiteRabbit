fetchto = "../../ip_cores"

modules = {
    "local" : [
        "../../rtl/",
        "../../ip_cores/wr-cores",
        "../../ip_cores/wr-cores/board/vxs", 
        "../../ip_cores/general-cores",
        "../../ip_cores/gn4124-core",
        "../../ip_cores/etherbone-core",

    ],
}

files = [
    "vxs_btrain_ref_top.vhd"
]
