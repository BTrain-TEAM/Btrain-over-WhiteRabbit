-------------------------------------------------------------------------------
-- Title      : WRPC reference design for BTrain on CUTE
-- Project    : WR PTP Core
-- URL        : https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
-------------------------------------------------------------------------------
-- File       : cute_btrain_ref_top.vhd
-- Author(s)  : Maciej Lipinski <maciej.lipinski@cern.ch.
--              Hongming Li <lihm.thu@foxmail.com>
--              Grzegorz Daniluk <grzegorz.daniluk@cern.ch>
-- Company    : CERN
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: Top-level file for the WRPC BTrain reference design on the CUTE.
--
-- This is a reference top HDL that instanciates 
-- * The WR PTP Core and streamers interface
-- * WR Btrain tranceiver
-- * Interface over FMC to access Btrain tx/rx fuction
--
-- This is a single-port version (even if HW supports two ports).
--
-- CUTE:  https://www.ohwr.org/projects/cute-wr-dp
--
-------------------------------------------------------------------------------
-- Copyright (c) 2018 CERN
-------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
--
-- This source file is free software; you can redistribute it   
-- and/or modify it under the terms of the GNU Lesser General   
-- Public License as published by the Free Software Foundation; 
-- either version 2.1 of the License, or (at your option) any   
-- later version.                                               
--
-- This source is distributed in the hope that it will be       
-- useful, but WITHOUT ANY WARRANTY; without even the implied   
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
-- PURPOSE.  See the GNU Lesser General Public License for more 
-- details.                                                     
--
-- You should have received a copy of the GNU Lesser General    
-- Public License along with this source; if not, download it   
-- from http://www.gnu.org/licenses/lgpl-2.1.html
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.wr_board_pkg.all;
use work.wr_cute_pkg.all;
use work.BTrainFrameTransceiver_pkg.all;
use work.BTrainFmcIF_pkg.all;
use work.streamers_pkg.all;
use work.BTrainFrame_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity cute_btrain_ref_top is
  generic (
    g_dpram_initf : string := "../../ip_cores/wr-cores/bin/wrpc/wrc_phy8.bram";
    g_sfp0_enable : integer:= 0;
    g_sfp1_enable : integer:= 1;
    g_cute_version       : string:= "2.2";
    g_aux_sdb            : t_sdb_device  := c_xwb_xil_multiboot_sdb;
    g_multiboot_enable   : boolean:= false;
    -- Simulation-mode enable parameter. Set by default (synthesis) to 0, and
    -- changed to non-zero in the instantiation of the top level DUT in the testbench.
    -- Its purpose is to reduce some internal counters/timeouts to speed up simulations.
    g_simulation : integer := 0
  );
  port (
    ---------------------------------------------------------------------------
    -- Clocks/resets
    ---------------------------------------------------------------------------

    -- Local oscillators
    clk20m_vcxo_i       : in std_logic;           -- 20mhz vcxo clock

    clk_125m_pllref_p_i : in std_logic;           -- 125 MHz PLL reference
    clk_125m_pllref_n_i : in std_logic;

    sfp0_ref_clk_p     : in std_logic;  -- dedicated clock for xilinx gtp transceiver
    sfp0_ref_clk_n     : in std_logic;
    sfp1_ref_clk_p     : in std_logic;  -- dedicated clock for xilinx gtp transceiver
    sfp1_ref_clk_n     : in std_logic;

    ---------------------------------------------------------------------------
    -- SPI interface to DACs
    ---------------------------------------------------------------------------

    plldac_sclk        : out std_logic;
    plldac_din         : out std_logic;
    plldac_clr_n       : out std_logic;
    plldac_load_n      : out std_logic;
    plldac_sync_n      : out std_logic;

    ---------------------------------------------------------------------------
    -- SFP I/O for transceiver
    ---------------------------------------------------------------------------

--     sfp0_tx_p          : out   std_logic;
--     sfp0_tx_n          : out   std_logic;
--     sfp0_rx_p          : in    std_logic;
--     sfp0_rx_n          : in    std_logic;
--     sfp0_det           : in    std_logic;  -- sfp detect
--     sfp0_scl           : inout std_logic;  -- scl
--     sfp0_sda           : inout std_logic;  -- sda
--     sfp0_tx_fault      : in    std_logic;
--     sfp0_tx_disable    : out   std_logic;
--     sfp0_los           : in    std_logic;  
    sfp1_tx_p          : out   std_logic;
    sfp1_tx_n          : out   std_logic;
    sfp1_rx_p          : in    std_logic;
    sfp1_rx_n          : in    std_logic;
    sfp1_det           : in    std_logic;  -- sfp detect
    sfp1_scl           : inout std_logic;  -- scl
    sfp1_sda           : inout std_logic;  -- sda
    sfp1_tx_fault      : in    std_logic;
    sfp1_tx_disable    : out   std_logic;
    sfp1_tx_los        : in    std_logic;
  
    ---------------------------------------------------------------------------
    -- Onewire interface
    ---------------------------------------------------------------------------

    one_wire           : inout std_logic;      -- 1-wire interface to ds18b20
    ---------------------------------------------------------------------------
    -- UART
    ---------------------------------------------------------------------------
    uart_rx            : in  std_logic;
    uart_tx            : out std_logic;

    ---------------------------------------------------------------------------
    -- I2C configuration EEPROM interface
    ---------------------------------------------------------------------------
    eeprom_scl         : inout std_logic;
    eeprom_sda         : inout std_logic;

    ---------------------------------------------------------------------------
    -- Flash memory SPI interface
    ---------------------------------------------------------------------------

    flash_sclk_o       : out std_logic;
    flash_ncs_o        : out std_logic;
    flash_mosi_o       : out std_logic;
    flash_miso_i       : in  std_logic:='1';

    ---------------------------------------------------------------------------
    -- FMC bus
    ---------------------------------------------------------------------------
    -- system clock
--     clk_sys_62m5_p_o   : out   std_logic;                    -- clk0_m2c_p
--     clk_sys_62m5_n_o   : out   std_logic;                    -- clk0_m2c_n
    -- WR clock
--     clk_ref_125m_o     : out   std_logic;                    -- clk1_m2c_p
--     clk_ref_125m_o     : out   std_logic;                    -- clk1_m2c_n
    -- AUX clocks
--     aux_clk0_p_i       : in    std_logic;                    -- la00_cc_p
--     aux_clk0_n_i       : in    std_logic;                    -- la00_cc_n
--     aux_clk1_p_i       : in    std_logic;                    -- la01_cc_p
--     aux_clk1_n_i       : in    std_logic;                    -- la01_cc_n
    -- resets
    rst_from_carrier_n_i : in    std_logic;                    -- la02_p
    rst_to_carrier_n_o   : out   std_logic;                    -- la02_n
    -- control of AUX clocks
--     aux_sclk_o         : out   std_logic;                    -- la03_p
--     aux_mosi_o         : out   std_logic;                    -- la03_n
--     aux_miso_i         : in    std_logic;                    -- la04_p
--     aux_ss_o           : out   std_logic;                    -- la04_n
    -- config/diags of WRPC
--     wrpc_scl_i         : in    std_logic;                    -- la05_p
--     wrpc_sda_b         : inout std_logic;                    -- la05_n
    -- Virtual UART
--     uart_i             : in    std_logic;                    -- la06_p
--     uart_o             : out   std_logic;                    -- la06_n
    -- timing interface
    link_up_o          : out   std_logic;                    -- la07_p
    time_valid_o       : out   std_logic;                    -- la07_n
--     pps_o              : out   std_logic;                    -- la08_p
--     tai_o              : out   std_logic;                    -- la08_n
    -- interrupts
    irq_rx_data_o      : out   std_logic;                    -- la09_p
    irq_aux_o          : out   std_logic;                    -- la09_n
    -- reception data interface

    rx_wb_adr_i        : in    std_logic_vector(7 downto 0); -- la10 to la13
    rx_wb_dat_b        : inout std_logic_vector(7 downto 0); -- la14 to la17
    rx_wb_clk_i        : in    std_logic;                    -- la20_p 
    rx_wb_we_i         : in    std_logic;                    -- la18_n
    rx_wb_cyc_i        : in    std_logic;                    -- la19_p
    rx_wb_stb_i        : in    std_logic;                    -- la19_n
    rx_wb_ack_o        : out   std_logic;                    -- la18_p
    rx_wb_err_o        : out   std_logic;                    -- la30_p
    rx_wb_rty_o        : out   std_logic;                    -- la21_p
    rx_wb_stall_o      : out   std_logic;                    -- la21_n
    -- transmission data interface
    tx_wb_adr_i        : in    std_logic_vector(7 downto 0); -- la22 to la25
    tx_wb_dat_b        : inout std_logic_vector(7 downto 0); -- la26 to la29
    tx_wb_clk_i        : in    std_logic;                    -- la20_n
    tx_wb_we_i         : in    std_logic;                    -- la30_n
    tx_wb_cyc_i        : in    std_logic;                    -- la31_p
    tx_wb_stb_i        : in    std_logic;                    -- la31_n
    tx_wb_ack_o        : out   std_logic;                    -- la32_p
    tx_wb_err_o        : out   std_logic;                    -- la32_n
    tx_wb_rty_o        : out   std_logic;                    -- la33_p
    tx_wb_stall_o      : out   std_logic;                    -- la33_n
    ---------------------------------------------------------------------------
    -- Miscellanous I/O pins
    ---------------------------------------------------------------------------
    -- user interface
    sfp0_led           : out std_logic;
    sfp1_led           : out std_logic;
    ext_clk            : out std_logic;
    usr_button         : in  std_logic;
    usr_led1           : out std_logic;
    usr_led2           : out std_logic;
    pps_out            : out std_logic
  );
end cute_btrain_ref_top;

architecture rtl of cute_btrain_ref_top is
  

  -----------------------------------------------------------------------------
  -- Signals
  -----------------------------------------------------------------------------
  -- I2C EEPROM
  signal eeprom_scl_o      : std_logic;
  signal eeprom_scl_i      : std_logic;
  signal eeprom_sda_o      : std_logic;
  signal eeprom_sda_i      : std_logic;

  -- OneWire
  signal onewire_i         : std_logic;
  signal onewire_oen_o     : std_logic;

  -- SFP
  signal sfp0_scl_i        : std_logic;
  signal sfp0_scl_o        : std_logic;
  signal sfp0_sda_i        : std_logic;
  signal sfp0_sda_o        : std_logic;
  signal sfp1_scl_i        : std_logic;
  signal sfp1_scl_o        : std_logic;
  signal sfp1_sda_i        : std_logic;
  signal sfp1_sda_o        : std_logic;
  
  signal pps               : std_logic;
  signal pps_csync         : std_logic;
  attribute maxdelay       : string;
  attribute maxdelay of pps_csync : signal is "500 ps";
  signal tm_tai            : std_logic_vector(39 downto 0);
  signal tm_time_valid     : std_logic;
  signal tm_link_up        : std_logic;
  signal pps_led           : std_logic;
  signal led_act           : std_logic;
  -- Wishbone buse(s) from masters attached to crossbar
  signal cnx_master_out : t_wishbone_master_out_array(0 downto 0);
  signal cnx_master_in  : t_wishbone_master_in_array(0 downto 0);
  -- Wishbone buse(s) to slaves attached to crossbar
  signal cnx_slave_out : t_wishbone_slave_out_array(0 downto 0);
  signal cnx_slave_in  : t_wishbone_slave_in_array(0 downto 0);

  -- Not needed now, but useful if application cores are added
  signal clk_sys_62m5   : std_logic;
  signal clk_ref_125m   : std_logic;
  signal rst_sys_62m5_n : std_logic;
  signal rst_ref_125m_n : std_logic;

 -- WR Streamers <---> BTrain
  signal tx_data     : std_logic_vector(c_rx_streamer_params_btrain.data_width-1 downto 0);
  signal tx_valid    : std_logic;
  signal tx_dreq     : std_logic;
  signal tx_last_p1  : std_logic;
  signal tx_flush_p1 : std_logic;
  signal tx_cfg      : t_tx_streamer_cfg;
  -- rx
  signal rx_data     : std_logic_vector(c_tx_streamer_params_btrain.data_width-1 downto 0);
  signal rx_valid    : std_logic;
  signal rx_first_p1 : std_logic;
  signal rx_dreq     : std_logic;
  signal rx_last_p1  : std_logic;
  signal rx_cfg      : t_rx_streamer_cfg;

  -- BTrain <---> BTrain application (e.g. on BTrainFMC)
  signal tx_ready            : std_logic;
  signal tx_TransmitFrame_p1 : std_logic;
  signal tx_FrameHeader    : t_FrameHeader;
  signal tx_BFramePayloads : t_BFramePayload;
  signal tx_IFramePayloads : t_IFramePayload;
  signal tx_CFramePayloads : t_CFramePayload;

  signal tx_wb_dat_in        : std_logic_vector(7 downto 0);
  signal tx_wb_dat_out       : std_logic_vector(7 downto 0);
  signal tx_wb_ack_out       : std_logic;
  signal tx_wb_err_out       : std_logic;
  signal tx_wb_rty_out       : std_logic;
  signal tx_wb_stall_out     : std_logic;
  signal tx_wb_slave_in      : t_wishbone_slave_in;
  signal tx_wb_slave_out     : t_wishbone_slave_out;

  signal rx_FrameHeader      : t_FrameHeader;
  signal rx_BFramePayloads   : t_BFramePayload;
  signal rx_IFramePayloads   : t_IFramePayload;
  signal rx_CFramePayloads   : t_CFramePayload;
  signal rx_Frame_valid_pX   : std_logic;
  signal rx_Frame_typeID     : std_logic_vector(c_type_ID_size-1 downto 0);
  signal rx_cfg_pol_inv      : std_logic;
  signal rx_wb_dat_in        : std_logic_vector(7 downto 0);
  signal rx_wb_dat_out       : std_logic_vector(7 downto 0);
  signal rx_wb_ack_out       : std_logic;
  signal rx_wb_err_out       : std_logic;
  signal rx_wb_rty_out       : std_logic;
  signal rx_wb_stall_out     : std_logic;
  signal rx_FrameIrq         : std_logic;
  signal rx_wb_slave_in      : t_wishbone_slave_in;
  signal rx_wb_slave_out     : t_wishbone_slave_out;
  ------------------------------------------------------------------------------
  -- ChipScope for histogram readout/debugging
  ------------------------------------------------------------------------------
--   component chipscope_spartan6_icon
--     port (
--       CONTROL0 : inout std_logic_vector(35 downto 0));
--   end component;
-- 
--   component chipscope_spartan6_ila
--     port (
--       CONTROL : inout std_logic_vector(35 downto 0);
--       CLK     : in    std_logic;
--       TRIG0   : in    std_logic_vector(31 downto 0);
--       TRIG1   : in    std_logic_vector(31 downto 0);
--       TRIG2   : in    std_logic_vector(31 downto 0);
--       TRIG3   : in    std_logic_vector(31 downto 0));
--   end component;

  signal control0                   : std_logic_vector(35 downto 0);
  signal trig0, trig1, trig2, trig3 : std_logic_vector(31 downto 0);
  ------------------------------------------------------------------------------

begin

  u_wr_core : xwrc_board_cute
    generic map(
      g_dpram_initf      => g_dpram_initf,
      g_sfp0_enable      => g_sfp0_enable,
      g_sfp1_enable      => g_sfp1_enable,
      g_aux_sdb          => g_aux_sdb,
      g_cute_version     => g_cute_version,
      g_phy_refclk_sel   => 4,
      g_multiboot_enable => g_multiboot_enable,
      g_streamers_op_mode => TX_AND_RX,
      g_tx_streamer_params=> c_tx_streamer_params_btrain,
      g_rx_streamer_params=> c_rx_streamer_params_btrain,
      g_fabric_iface      => STREAMERS
      )
    port map (
      areset_n_i          => usr_button,
      clk_20m_vcxo_i      => clk20m_vcxo_i,
      clk_125m_pllref_p_i => clk_125m_pllref_p_i,
      clk_125m_pllref_n_i => clk_125m_pllref_n_i,
      clk_125m_gtp0_p_i   => sfp0_ref_clk_p,
      clk_125m_gtp0_n_i   => sfp0_ref_clk_n,
      clk_125m_gtp1_p_i   => sfp1_ref_clk_p,
      clk_125m_gtp1_n_i   => sfp1_ref_clk_n,
      clk_sys_62m5_o      => clk_sys_62m5,
      clk_ref_125m_o      => clk_ref_125m,
      clk_10m_ext_o       => ext_clk,
      rst_sys_62m5_n_o    => rst_sys_62m5_n,
      rst_ref_125m_n_o    => rst_ref_125m_n,
  
      plldac_sclk_o       => plldac_sclk,
      plldac_din_o        => plldac_din,
      plldac_clr_n_o      => plldac_clr_n,
      plldac_load_n_o     => plldac_load_n,
      plldac_sync_n_o     => plldac_sync_n,
  
--       sfp0_txp_o          => sfp0_tx_p,
--       sfp0_txn_o          => sfp0_tx_n,
--       sfp0_rxp_i          => sfp0_rx_p,
--       sfp0_rxn_i          => sfp0_rx_n,
--       sfp0_det_i          => sfp0_det,
--       sfp0_scl_i          => sfp0_scl_i,
--       sfp0_scl_o          => sfp0_scl_o,
--       sfp0_sda_i          => sfp0_sda_i,
--       sfp0_sda_o          => sfp0_sda_o,
--       sfp0_rate_select_o  => open,
--       sfp0_tx_fault_i     => sfp0_tx_fault,
--       sfp0_tx_disable_o   => sfp0_tx_disable,
--       sfp0_los_i          => sfp0_los,
      sfp1_txp_o          => sfp1_tx_p,
      sfp1_txn_o          => sfp1_tx_n,
      sfp1_rxp_i          => sfp1_rx_p,
      sfp1_rxn_i          => sfp1_rx_n,
      sfp1_det_i          => sfp1_det,
      sfp1_scl_i          => sfp1_scl_i,
      sfp1_scl_o          => sfp1_scl_o,
      sfp1_sda_i          => sfp1_sda_i,
      sfp1_sda_o          => sfp1_sda_o,
      sfp1_rate_select_o  => open,
      sfp1_tx_fault_i     => sfp1_tx_fault,
      sfp1_tx_disable_o   => sfp1_tx_disable,
      sfp1_los_i          => sfp1_tx_los,
  
      eeprom_scl_i        => eeprom_scl_i,
      eeprom_scl_o        => eeprom_scl_o,
      eeprom_sda_i        => eeprom_sda_i,
      eeprom_sda_o        => eeprom_sda_o,
  
      onewire_i           => onewire_i,
      onewire_oen_o       => onewire_oen_o,
  
      uart_rxd_i          => uart_rx,
      uart_txd_o          => uart_tx,
  
      flash_sclk_o        => flash_sclk_o,
      flash_ncs_o         => flash_ncs_o,
      flash_mosi_o        => flash_mosi_o,
      flash_miso_i        => flash_miso_i,

      wb_slave_o          => cnx_slave_out(0),
      wb_slave_i          => cnx_slave_in(0),

      wb_eth_master_o     => cnx_master_out(0),
      wb_eth_master_i     => cnx_master_in(0),
  
      tm_link_up_o        => tm_link_up,
      tm_time_valid_o     => tm_time_valid,
      tm_tai_o            => tm_tai,
      tm_cycles_o         => open,
  
      led_act_o           => led_act,
      led_link_o          => open,
      pps_p_o             => pps_out,
      pps_led_o           => pps_led,
      pps_csync_o         => pps_csync,
      link_ok_o           => open,

      -- BTRAIN
      wrs_tx_data_i       => tx_data,
      wrs_tx_valid_i      => tx_valid,
      wrs_tx_dreq_o       => tx_dreq,
      wrs_tx_last_i       => tx_last_p1,
      wrs_tx_flush_i      => tx_flush_p1,
      wrs_tx_cfg_i        => tx_cfg,
      wrs_rx_first_o      => rx_first_p1,
      wrs_rx_last_o       => rx_last_p1,
      wrs_rx_data_o       => rx_data,
      wrs_rx_valid_o      => rx_valid,
      wrs_rx_dreq_i       => rx_dreq,
      wrs_rx_cfg_i        => rx_cfg

      );
  
  cnx_slave_in <= cnx_master_out;
  cnx_master_in <= cnx_slave_out;

  -- Configuration of streamers:
  -- 1) For synthesis : In Btrain deployments, by default, streamers use VID=0
  --    (i.e. priority tagging). 
  -- 2) For simulation: Using VLANs (VID=0) is problematic (requires special
  --    configuration). It is easier to disable VLANs for simulation.
  gen_streamers_cfg_sim: if (g_simulation>0) generate -- no VLAN
    rx_cfg <= c_rx_streamer_cfg_default;
    tx_cfg <= c_tx_streamer_cfg_default;
  end generate gen_streamers_cfg_sim;
  gen_streamers_cfg_syn: if (g_simulation=0) generate -- with VLAN(VID=0)
    rx_cfg <= c_rx_streamer_cfg_btrain;
    tx_cfg <= c_tx_streamer_cfg_btrain;
  end generate gen_streamers_cfg_syn;

  -- Tristates for configuration EEPROM
  eeprom_scl  <= '0' when eeprom_scl_o = '0' else 'Z';
  eeprom_sda  <= '0' when eeprom_sda_o = '0' else 'Z';
  eeprom_scl_i  <= eeprom_scl;
  eeprom_sda_i  <= eeprom_sda;

  -- Tristates for SFP EEPROM
--   sfp0_scl <= '0' when sfp0_scl_o = '0' else 'Z';
--   sfp0_sda <= '0' when sfp0_sda_o = '0' else 'Z';
--   sfp0_scl_i <= sfp0_scl;
--   sfp0_sda_i <= sfp0_sda;
  sfp1_scl <= '0' when sfp1_scl_o = '0' else 'Z';
  sfp1_sda <= '0' when sfp1_sda_o = '0' else 'Z';
  sfp1_scl_i <= sfp1_scl;
  sfp1_sda_i <= sfp1_sda;
  
  -- Tristates for Onewire
  one_wire <= '0' when onewire_oen_o = '1' else 'Z';
  onewire_i  <= one_wire;
  
  sfp0_led <= not led_act;
  sfp1_led <= not pps_led;
  
  usr_led1 <= not tm_time_valid;
  usr_led2 <= not tm_link_up;
  
  link_up_o    <= tm_link_up;
  time_valid_o <= tm_time_valid;
  rst_to_carrier_n_o <= rst_sys_62m5_n;

 ------------------------------------------------------------------------------
  -- WR BTrain transceiver
  ------------------------------------------------------------------------------

  cmp_btrain : BTrainFrameTransceiver
    generic map(
      g_rx_BframeType       => c_ID_ALL, -- accept all types of BTrain frames
      g_use_wb_config       => TRUE,
      g_slave_mode          => CLASSIC,
      g_slave_granularity   => BYTE,
      g_rx_out_data_time_valid => x"000A")
    port map(
      clk_i                 => clk_sys_62m5,
      rst_n_i               => rst_sys_62m5_n,

      tx_data_o             => tx_data,
      tx_valid_o            => tx_valid,
      tx_dreq_i             => tx_dreq,
      tx_last_p1_o          => tx_last_p1,
      tx_flush_p1_o         => tx_flush_p1,
      -- rx
      rx_data_i             => rx_data,
      rx_valid_i            => rx_valid,
      rx_first_p1_i         => rx_first_p1,
      rx_dreq_o             => rx_dreq,
      rx_last_p1_i          => rx_last_p1,

      rx_FrameHeader_o      => rx_FrameHeader,
      rx_BFramePayloads_o   => rx_BFramePayloads,
      rx_IFramePayloads_o   => rx_IFramePayloads,
      rx_CframePayloads_o   => rx_CFramePayloads,
      rx_Frame_valid_pX_o   => rx_Frame_valid_pX,
      rx_Frame_typeID_o     => rx_Frame_typeID,

      ready_o               => tx_ready,
      tx_TransmitFrame_p1_i => tx_TransmitFrame_p1,
      tx_FrameHeader_i      => tx_FrameHeader,
      tx_BFramePayloads_i   => tx_BFramePayloads,
      tx_IFramePayloads_i   => tx_IFramePayloads,
      tx_CFramePayloads_i   => tx_CFramePayloads

--       wb_slave_i => cnx_slave_in(c_WB_SLAVE_BTRAIN),
--       wb_slave_o => cnx_slave_out(c_WB_SLAVE_BTRAIN)
      );


  U_BT_FMC_IF: BTrainFmcInterface 
  generic map(
    g_with_chipscope       => TRUE)
  port map (
    clk_sys_i              => clk_sys_62m5,
    rst_sys_n_i            => rst_sys_62m5_n,

    rx_FrameHeader_i       => rx_FrameHeader,
    rx_BFramePayloads_i    => rx_BFramePayloads,
    rx_IFramePayloads_i    => rx_IFramePayloads,
    rx_CFramePayloads_i    => rx_CFramePayloads,
    rx_Frame_valid_pX_i    => rx_Frame_valid_pX,
    rx_Frame_typeID_i      => rx_Frame_typeID,
    rx_FrameIrq_o          => rx_FrameIrq,

    tx_ready_i             => tx_ready,
    tx_TransmitFrame_p1_o  => tx_TransmitFrame_p1,
    tx_FrameHeader_o       => tx_FrameHeader,
    tx_BFramePayloads_o    => tx_BFramePayloads,
    tx_IFramePayloads_o    => tx_IFramePayloads,
    tx_CFramePayloads_o    => tx_CFramePayloads,

    rst_carrier_n_a_i      => rst_from_carrier_n_i,
    ----------------------------------------------------------------
    -- Wishbone interface (connect only if g_use_wb_config=true,
    -- otherwise you will run into problems
    ----------------------------------------------------------------
    rx_wb_clk_i            => rx_wb_clk_i,
    rx_wb_slave_i          => rx_wb_slave_in,
    rx_wb_slave_o          => rx_wb_slave_out,

    tx_wb_clk_i            => tx_wb_clk_i,
    tx_wb_slave_i          => tx_wb_slave_in,
    tx_wb_slave_o          => tx_wb_slave_out
  );
  ------------------------------------------------------------------------------
  --  WB rx
  ------------------------------------------------------------------------------
  rx_wb_slave_in.adr(7 downto 0) <= rx_wb_adr_i;
  rx_wb_slave_in.dat(7 downto 0) <= rx_wb_dat_in;
  rx_wb_slave_in.cyc             <= rx_wb_cyc_i;
  rx_wb_slave_in.stb             <= rx_wb_stb_i;
  rx_wb_slave_in.we              <= rx_wb_we_i;

  rx_wb_dat_out                  <= rx_wb_slave_out.dat(7 downto 0);
  rx_wb_ack_out                  <= rx_wb_slave_out.ack;
  rx_wb_stall_out                <= rx_wb_slave_out.stall;
  rx_wb_err_out                  <= '0';
  rx_wb_rty_out                  <= '0';
  -- associate inputs/outputs - need this to use chipscope
  rx_wb_dat_in  <= rx_wb_dat_b;
  rx_wb_dat_b   <= (others => 'Z') when rx_wb_we_i = '1' else rx_wb_dat_out;
  rx_wb_ack_o   <= rx_wb_ack_out;
  rx_wb_err_o   <= rx_wb_err_out;
  rx_wb_rty_o   <= rx_wb_rty_out;
  rx_wb_stall_o <= rx_wb_stall_out;

  irq_rx_data_o <= rx_FrameIrq;
  irq_aux_o     <= '0';
  ------------------------------------------------------------------------------
  --  WB tx
  ------------------------------------------------------------------------------
  tx_wb_slave_in.adr(7 downto 0) <= tx_wb_adr_i;
  tx_wb_slave_in.dat(7 downto 0) <= tx_wb_dat_in;
  tx_wb_slave_in.cyc             <= tx_wb_cyc_i;
  tx_wb_slave_in.stb             <= tx_wb_stb_i;
  tx_wb_slave_in.we              <= tx_wb_we_i;

  tx_wb_dat_out                  <= tx_wb_slave_out.dat(7 downto 0);
  tx_wb_ack_out                  <= tx_wb_slave_out.ack;
  tx_wb_stall_out                <= tx_wb_slave_out.stall;
  tx_wb_err_out                  <= '0';
  tx_wb_rty_out                  <= '0';
  -- associate inputs/outputs - need this to use chipscope
  tx_wb_dat_in  <= tx_wb_dat_b;
  tx_wb_dat_b   <= (others => 'Z') when tx_wb_we_i = '1' else tx_wb_dat_out;
  tx_wb_ack_o   <= tx_wb_ack_out;
  tx_wb_err_o   <= tx_wb_err_out;
  tx_wb_rty_o   <= tx_wb_rty_out;
  tx_wb_stall_o <= tx_wb_stall_out;


  ------------------------------------------------------------------------------
  --  CLKs
  ------------------------------------------------------------------------------
--   U_output_clk: clock_output
--   port map (
--     clk_0_i     => clk_sys_62m5,
--     clk_180_i   => std_logic(not clk_sys_62m5),
-- 
--     clk_out_p_o => clk_sys_62m5_p_o,
--     clk_out_n_o => clk_sys_62m5_n_o
--     );

  ------------------------------------------------------------------------------
  -- chipscipe
  ------------------------------------------------------------------------------
  -- snoop values in/out:
--   TRIG0 ( 7 downto  0) <= rx_wb_adr_i;
--   TRIG0 (15 downto  8) <= rx_wb_dat_out;
--   TRIG0 (23 downto 16) <= rx_wb_dat_in;
--   TRIG0 (          24) <= rx_wb_we_i;
--   TRIG0 (          25) <= rx_wb_cyc_i;
--   TRIG0 (          26) <= rx_wb_stb_i;
--   TRIG0 (          27) <= rx_wb_ack_out;
--   TRIG0 (          28) <= rx_wb_err_out;
--   TRIG0 (          29) <= rx_wb_rty_out;
--   TRIG0 (          30) <= rx_wb_stall_out;
--   TRIG0 (          31) <= rx_FrameIrq;
-- 
--   TRIG1 ( 7 downto  0) <= rx_Frame_typeID;
--   TRIG1 (23 downto  8) <= rx_BFramePayloads.B(15 downto 0);
--   TRIG1 (          24) <= rx_Frame_valid_pX;
--   TRIG1 (          25) <= rx_valid;
--   TRIG1 (          26) <= rx_first_p1;
--   TRIG1 (          27) <= rx_dreq;
--   TRIG1 (          28) <= rx_last_p1;
--   TRIG1 (          29) <= '0';
--   TRIG1 (          30) <= '0';
--   TRIG1 (          31) <= '0';
-- 
--   TRIG2 (31 downto  0) <= rx_data(31 downto  0);
--   TRIG3 (31 downto  0) <= (others =>'0');
--   
--   CS_ICON : chipscope_spartan6_icon
--     port map (
--       CONTROL0 => CONTROL0);
--   CS_ILA : chipscope_spartan6_ila
--     port map (
--       CONTROL => CONTROL0,
--       CLK     => clk_sys_62m5,
--       TRIG0   => TRIG0,
--       TRIG1   => TRIG1,
--       TRIG2   => TRIG2,
--       TRIG3   => TRIG3);

end rtl;
