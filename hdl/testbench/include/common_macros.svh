//-----------------------------------------------------------------------------
// Title      : Definitions for BTrain reference design testbench
// Project    : BTrain over White Rabbit
// URL        : https://wikis.cern.ch/display/HT/Btrain+over+White+Rabbit
//-----------------------------------------------------------------------------
// File       : common_macros.svh
// Author(s)  : Dimitrios Lampridis <dimitrios.lampridis@cern.ch>
// Company    : CERN (BE-CO-HT)
// Created    : 2017-05-05
//-----------------------------------------------------------------------------
// Description:
//
// Macros and definitions common to all BTrain reference design testbenches.
//
//-----------------------------------------------------------------------------
//
// Copyright (c) 2017 CERN
//
// This source file is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation;
// either version 2.1 of the License, or (at your option) any
// later version.
//
// This source is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General
// Public License along with this source; if not, download it
// from http://www.gnu.org/licenses/lgpl-2.1.html
//
//-----------------------------------------------------------------------------

`ifndef BTRAIN_COMMON_MACROS_SV
 `define BTRAIN_COMMON_MACROS_SV 1

/// ///////////////////////////////////////////////////////////////////////////
/// Marco to verify successfull randomization of an SV Class
/// ///////////////////////////////////////////////////////////////////////////
`define SV_RAND_CHECK(r) \
   do begin \
      assert (r) else begin \
	 $fatal(1, "%s:%0d: Randomization failed \"%s\"", \
		`__FILE__,`__LINE__, `"r`"); \
      end \
   end while (0)

/// ///////////////////////////////////////////////////////////////////////////
/// Connections between a BTrain TX interface and the DUT
/// ///////////////////////////////////////////////////////////////////////////
`define BTRAIN_REF_DUT_ATTACH_TX_IF\
.rst_n                    (rst_sys_62m5_n),\
.clk                      (clk_sys_62m5),\
.FrameHeader_version_id   (sim_tx_FrameHeader_version_id),\
.FrameHeader_d_low_marker (sim_tx_FrameHeader_d_low_marker),\
.FrameHeader_f_low_marker (sim_tx_FrameHeader_f_low_marker),\
.FrameHeader_zero_cycle   (sim_tx_FrameHeader_zero_cycle),\
.FrameHeader_C0           (sim_tx_FrameHeader_C0),\
.FrameHeader_error        (sim_tx_FrameHeader_error),\
.FrameHeader_sim_eff      (sim_tx_FrameHeader_sim_eff),\
.FrameHeader_frame_type   (sim_tx_FrameHeader_frame_type),\
.BFramePayload_B          (sim_tx_BFramePayload_B),\
.BFramePayload_Bdot       (sim_tx_BFramePayload_Bdot),\
.BFramePayload_oldB       (sim_tx_BFramePayload_oldB),\
.BFramePayload_measB      (sim_tx_BFramePayload_measB),\
.BFramePayload_simB       (sim_tx_BFramePayload_simB),\
.BFramePayload_synB       (sim_tx_BFramePayload_synB),\
.IFramePayload_I          (sim_tx_IFramePayload_I),\
.CFramePayload_I          (sim_tx_CFramePayload_I),\
.CFramePayload_V          (sim_tx_CFramePayload_V),\
.CFramePayload_MSrcId     (sim_tx_CFramePayload_MSrcId),\
.CFramePayload_CSrcId     (sim_tx_CFramePayload_CSrcId),\
.CFramePayload_UserData   (sim_tx_CFramePayload_UserData),\
.CFramePayload_Reserved_1 (sim_tx_CFramePayload_Reserved_1),\
.CFramePayload_Reserved_2 (sim_tx_CFramePayload_Reserved_2),\
.rx_Frame_valid_pX        (),\
.rx_Frame_typeID          (),\
.tx_ready                 (sim_tx_ready),\
.tx_TransmitFrame_p1      (sim_tx_TransmitFrame_p1)

/// ////////////////////////////////////////////////////////////////////////
/// Connections between a BTrain RX interface and the DUT
/// ////////////////////////////////////////////////////////////////////////
`define BTRAIN_REF_DUT_ATTACH_RX_IF\
.rst_n                    (rst_sys_62m5_n),\
.clk                      (clk_sys_62m5),\
.FrameHeader_version_id   (sim_rx_FrameHeader_version_id),\
.FrameHeader_d_low_marker (sim_rx_FrameHeader_d_low_marker),\
.FrameHeader_f_low_marker (sim_rx_FrameHeader_f_low_marker),\
.FrameHeader_zero_cycle   (sim_rx_FrameHeader_zero_cycle),\
.FrameHeader_C0           (sim_rx_FrameHeader_C0),\
.FrameHeader_error        (sim_rx_FrameHeader_error),\
.FrameHeader_sim_eff      (sim_rx_FrameHeader_sim_eff),\
.FrameHeader_frame_type   (sim_rx_FrameHeader_frame_type),\
.BFramePayload_B          (sim_rx_BFramePayload_B),\
.BFramePayload_Bdot       (sim_rx_BFramePayload_Bdot),\
.BFramePayload_oldB       (sim_rx_BFramePayload_oldB),\
.BFramePayload_measB      (sim_rx_BFramePayload_measB),\
.BFramePayload_simB       (sim_rx_BFramePayload_simB),\
.BFramePayload_synB       (sim_rx_BFramePayload_synB),\
.IFramePayload_I          (sim_rx_IFramePayload_I),\
.CFramePayload_I          (sim_rx_CFramePayload_I),\
.CFramePayload_V          (sim_rx_CFramePayload_V),\
.CFramePayload_MSrcId     (sim_rx_CFramePayload_MSrcId),\
.CFramePayload_CSrcId     (sim_rx_CFramePayload_CSrcId),\
.CFramePayload_UserData   (sim_rx_CFramePayload_UserData),\
.CFramePayload_Reserved_1 (sim_rx_CFramePayload_Reserved_1),\
.CFramePayload_Reserved_2 (sim_rx_CFramePayload_Reserved_2),\
.rx_Frame_valid_pX        (sim_rx_Frame_valid_pX),\
.rx_Frame_typeID          (sim_rx_Frame_typeID),\
.tx_ready                 (),\
.tx_TransmitFrame_p1      (),\
.bup                      (sim_rx_bup),\
.bdown                    (sim_rx_bdown)

`endif // BTRAIN_COMMON_MACROS_SV
