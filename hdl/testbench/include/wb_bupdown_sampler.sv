//-----------------------------------------------------------------------------
// CERN
// BTrain-over-WhiteRabbit
// https://wikis.cern.ch/display/HT/BTrain+over+White+Rabbit
//-----------------------------------------------------------------------------
//
// unit name:     tb_environment_and_test_program.sv
//
// description:
//
// SystemVerilog file with environment and test program.
//
//-----------------------------------------------------------------------------
// Copyright (c) 2018 CERN BE/CO/HT
//-----------------------------------------------------------------------------
// GNU LESSER GENERAL PUBLIC LICENSE
//-----------------------------------------------------------------------------
// This source file is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation;
// either version 2.1 of the License, or (at your option) any
// later version.
//
// This source is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General
// Public License along with this source; if not, download it
// from http://www.gnu.org/licenses/lgpl-2.1.html
//
//-----------------------------------------------------------------------------

`include "simdrv_defs.svh"         // CBusAccessor class and uint types
`include "bupdown_sampler_wb.svh"  // bupdown_sampler registers

/*
 * BTrain WB slave class
 */
class CWBBUpDownSampler extends CWBPeriph;

   const uint32_t c_DUMMY_VAL = 'h0000CAFE;

   function new(const ref string name,
		const ref CBusAccessor bus,
		const ref uint32_t base);
      // call parent contructor
      super.new(name, bus, base);
   endfunction // new

   task refresh_status ();
      uint32_t rval;
      read32(`ADDR_BUPDOWN_SAMPLER_SCR, rval);
      rval = (`BUPDOWN_SAMPLER_SCR_DUMMY & rval) >> `BUPDOWN_SAMPLER_SCR_DUMMY_OFFSET;
      if (c_DUMMY_VAL == rval) begin
	present = 1;
	$display("BUpDownSampler present");
      end else begin
	present = 0;
	$display("BUpDownSampler absent");
      end
   endtask // refresh_status


   task acq_start();
      $display("[%t] %s: start acquisition", $time, name);
      modify32(`ADDR_BUPDOWN_SAMPLER_SCR,
               `BUPDOWN_SAMPLER_SCR_ACQ_START,
               `BUPDOWN_SAMPLER_SCR_ACQ_START);
   endtask; // acq_start

   task acq_stop();
      $display("[%t] %s: start acquisition", $time, name);
      modify32(`ADDR_BUPDOWN_SAMPLER_SCR,
               `BUPDOWN_SAMPLER_SCR_ACQ_STOP,
               `BUPDOWN_SAMPLER_SCR_ACQ_STOP);
   endtask; // acq_start

   task acq_mode(int mode);
      uint32_t val;
      $display("[%t] %s: set acquisition mode: %d", $time, name, mode);

      val = mode << `BUPDOWN_SAMPLER_SCR_ACQ_MODE_OFFSET;
      modify32(`ADDR_BUPDOWN_SAMPLER_SCR, `BUPDOWN_SAMPLER_SCR_ACQ_MODE, val);
   endtask; // acq_mode

   task cmode(int mode);
      uint32_t val;
      $display("[%t] %s: set C0 reset mode: %d", $time, name, mode);

      val = mode << `BUPDOWN_SAMPLER_SCR_C0_MODE_OFFSET;
      modify32(`ADDR_BUPDOWN_SAMPLER_SCR, `BUPDOWN_SAMPLER_SCR_C0_MODE, val);
   endtask; // cmode

   task acq_stat_read();
      uint32_t rval;
      read32(`ADDR_BUPDOWN_SAMPLER_SCR, rval);
      rval =(`BUPDOWN_SAMPLER_SCR_ACQ_STATUS & rval) >> `BUPDOWN_SAMPLER_SCR_ACQ_STATUS_OFFSET;
      if (rval == 0)
        $display("[%t] %s: acq_stat: none", $time, name);
      else
        $display("[%t] %s: acq_stat: ongoing", $time, name);
   endtask; // acq_stat_read

   task speriod(int period);
      uint32_t val;
      $display("[%t] %s: set sampling period : %d", $time, name, period);

      val = period << `BUPDOWN_SAMPLER_MCR_S_PERIOD_OFFSET;
      modify32(`ADDR_BUPDOWN_SAMPLER_MCR, `BUPDOWN_SAMPLER_MCR_S_PERIOD, val);
   endtask; // cmode

endclass // CWBBTrain