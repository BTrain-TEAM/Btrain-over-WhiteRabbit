//-----------------------------------------------------------------------------
// Title      : Definitions for SPEC BTrain reference design testbench
// Project    : BTrain over White Rabbit
// URL        : https://wikis.cern.ch/display/HT/Btrain+over+White+Rabbit
//-----------------------------------------------------------------------------
// File       : local_macros.svh
// Author(s)  : Dimitrios Lampridis <dimitrios.lampridis@cern.ch>
// Company    : CERN (BE-CO-HT)
// Created    : 2017-05-05
//-----------------------------------------------------------------------------
// Description:
//
// Macros and definitions relevant to the SPEC BTrain reference
// design testbench.
//
//-----------------------------------------------------------------------------
//
// Copyright (c) 2017 CERN
//
// This source file is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation;
// either version 2.1 of the License, or (at your option) any
// later version.
//
// This source is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General
// Public License along with this source; if not, download it
// from http://www.gnu.org/licenses/lgpl-2.1.html
//
//-----------------------------------------------------------------------------

`ifndef BTRAIN_SPEC_MACROS_SV
 `define BTRAIN_SPEC_MACROS_SV 1

/// ///////////////////////////////////////////////////////////////////////////
/// Clock connections
/// ///////////////////////////////////////////////////////////////////////////

`define SPEC_BTRAIN_REF_CONNECT_CLOCKS \
.clk_20m_vcxo_i(clk_20m_vcxo),\
.clk_125m_pllref_p_i(clk_125m_pllref),\
.clk_125m_pllref_n_i(~clk_125m_pllref),\
.clk_125m_gtp_p_i(clk_125m_pllref),\
.clk_125m_gtp_n_i(~clk_125m_pllref)

/// ///////////////////////////////////////////////////////////////////////////
/// Unused ports
/// ///////////////////////////////////////////////////////////////////////////

`define SPEC_BTRAIN_REF_CONNECT_UNUSED \
.plldac_sclk_o(),\
.plldac_din_o(),\
.pll25dac_cs_n_o(),\
.pll20dac_cs_n_o(),\
.sfp_mod_def0_i(),\
.sfp_mod_def1_b(),\
.sfp_mod_def2_b(),\
.sfp_rate_select_o(),\
.sfp_tx_fault_i(),\
.sfp_tx_disable_o(),\
.onewire_b(),\
.uart_rxd_i(),\
.uart_txd_o(),\
.flash_sclk_o(),\
.flash_ncs_o(),\
.flash_mosi_o(),\
.flash_miso_i(),\
.led_link_o(),\
.led_act_o(),\
.button2_n_i(),\
.dio_n_i(),\
.dio_p_i(),\
.dio_n_o(),\
.dio_p_o(),\
.dio_oe_n_o(),\
.dio_term_en_o(),\
.dio_led_top_o(),\
.dio_led_bot_o(),\
.dio_scl_b(),\
.dio_sda_b()

`endif // BTRAIN_SPEC_MACROS_SV
