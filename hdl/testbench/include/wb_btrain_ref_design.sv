//-----------------------------------------------------------------------------
// Title      : Definitions for BTrain reference design WB peripherals
// Project    : BTrain over White Rabbit
// URL        : https://wikis.cern.ch/display/HT/Btrain+over+White+Rabbit
//-----------------------------------------------------------------------------
// File       : wb_btrain_ref_design.sv
// Author(s)  : Dimitrios Lampridis <dimitrios.lampridis@cern.ch>
// Company    : CERN (BE-CO-HT)
// Created    : 2017-04-13
//-----------------------------------------------------------------------------
// Description:
//
// SystemVerilog file with all definitions, classes, etc. necessary
// for accessing the Wishbone peripherals in the the BTrain reference design.
//
// This includes the WR Streamer and BTrain itself
//
//-----------------------------------------------------------------------------
//
// Copyright (c) 2017 CERN
//
// This source file is free software; you can redistribute it
// and/or modify it under the terms of the GNU Lesser General
// Public License as published by the Free Software Foundation;
// either version 2.1 of the License, or (at your option) any
// later version.
//
// This source is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General
// Public License along with this source; if not, download it
// from http://www.gnu.org/licenses/lgpl-2.1.html
//

//-----------------------------------------------------------------------------


`include "simdrv_defs.svh"     // CBusAccessor class and uint types
`include "wr_streamers_wb.svh" // Streamer WB register definitions
`include "BTrain_wb.svh"       // BTrain WB register definitions

/*
 * A simple virtual class to represent any WB slave peripheral.
 * To be extended by actual WB peripherals.
 */
virtual class CWBPeriph;

   // Handle for all bus accesses
   protected CBusAccessor bus;

   // Offset in address map
   protected uint32_t base;

   protected string name;

   // 1 if the peripheral is present, set once by the constructor,
   // use refresh_status() to refresh it
   bit		    present;

   /*
    * Class constructor, initialises all class variables
    */
   function new(const ref string name,
		const ref CBusAccessor bus,
		const ref uint32_t base);
      // string to be used in various messages etc.
      this.name = name;

      this.bus  = bus;
      this.base = base;

      this.present = 0;

   endfunction // new

   task read32 (uint32_t offset, ref uint32_t rval);
      uint64_t rval64;
      bus.read(base + offset, rval64, 4);
      // bus.read works with uint64_t, so we cast it down to uint32_t.
      // Anyway, we asked for 4 bytes to be read.
      rval = uint32_t'(rval64);
   endtask // read32

   /*
    * Note that bus.write expects uint64_t, but no casting is really
    * necessary to go from uint32_t up to uint64_t (at least not in Modelsim)
    */
   task write32 (uint32_t offset, wval);
      bus.write(base + offset, wval, 4);
   endtask // write32

   /*
    * read/modify/write cycle
    */
   task modify32(uint32_t offset, mask, wval);
      uint32_t val;
      read32(offset, val);
      val &= ~mask;
      val |= (wval & mask);
      write32(offset, val);
   endtask // modify32

endclass // CWBPeriph

/*
 * White Rabbit Streamer WB slave class
 */
class CWBStreamer extends CWBPeriph;

   const uint32_t c_DUMMY_VAL = 'hDEADBEEF;

   // Struct to represent a timestamp
   typedef struct {
      uint32_t cyc;
      uint32_t tai;
   } tstamp_t;

   // Struct holding all the Streamer stats
   struct	  {
      tstamp_t acq_start, acq_end;
      uint64_t sent_frames;
      uint64_t rcvd_frames;
      uint64_t lost_frames;
      uint64_t lost_blocks;
      struct	  {
	 uint32_t maximum;
	 uint32_t minimum;
	 uint64_t acc;
	 bit	  acc_invalid;
	 uint64_t acc_cnt;
      } rx_latency;
   } stats;

   function new(const ref string name,
		const ref CBusAccessor bus,
		const ref uint32_t base);
      // call parent contructor
      super.new(name, bus, base);
      stats = '{'{0,0},'{0,0},0,0,0,0,'{0,0,0,0,0}};
   endfunction // new

   /*
    * Update "present" flag and all streamer stats
    */
   task refresh_status ();
      uint32_t rval;

      // update present flag
      read32(`ADDR_WR_STREAMERS_DUMMY, rval);
      rval = (`WR_STREAMERS_DUMMY_DUMMY & rval) >> `WR_STREAMERS_DUMMY_DUMMY_OFFSET;
      if (c_DUMMY_VAL == rval)
	present = 1;
      else
	present = 0;

      // update streamer stats
      read32(`ADDR_WR_STREAMERS_SSCR1, rval);
      stats.rx_latency.acc_invalid = ((rval & `WR_STREAMERS_SSCR1_RX_LATENCY_ACC_OVERFLOW) != 0);
      read32(`ADDR_WR_STREAMERS_TX_STAT2, rval);
      stats.sent_frames = rval;
      read32(`ADDR_WR_STREAMERS_TX_STAT3, rval);
      stats.sent_frames |= (rval << 32);
      read32(`ADDR_WR_STREAMERS_RX_STAT4, rval);
      stats.rcvd_frames = rval;
      read32(`ADDR_WR_STREAMERS_RX_STAT5, rval);
      stats.rcvd_frames |= (rval << 32);
      read32(`ADDR_WR_STREAMERS_RX_STAT6, rval);
      stats.lost_frames = rval;
      read32(`ADDR_WR_STREAMERS_RX_STAT7, rval);
      stats.lost_frames |= (rval << 32);
      read32(`ADDR_WR_STREAMERS_RX_STAT0, rval);
      stats.rx_latency.maximum = rval & `WR_STREAMERS_RX_STAT0_RX_LATENCY_MAX;
      read32(`ADDR_WR_STREAMERS_RX_STAT1, rval);
      stats.rx_latency.minimum = rval & `WR_STREAMERS_RX_STAT1_RX_LATENCY_MIN;
      read32(`ADDR_WR_STREAMERS_RX_STAT10, rval);
      stats.rx_latency.acc = rval;
      read32(`ADDR_WR_STREAMERS_RX_STAT11, rval);
      stats.rx_latency.acc |= (rval << 32);
      read32(`ADDR_WR_STREAMERS_RX_STAT12, rval);
      stats.rx_latency.acc_cnt = rval;
      read32(`ADDR_WR_STREAMERS_RX_STAT13, rval);
      stats.rx_latency.acc_cnt |= (rval << 32);
      read32(`ADDR_WR_STREAMERS_RX_STAT8, rval);
      stats.lost_blocks = rval;
      read32(`ADDR_WR_STREAMERS_RX_STAT9, rval);
      stats.lost_blocks |= (rval << 32);

   endtask // refresh_status

   /*
    * pretty-print" current streamer stats
    */
   task stats_display();
      $display("[%t] %s: ", $time, name,
	       "start: %0d:%0d, ", stats.acq_start.tai, stats.acq_start.cyc,
	       "end: %0d:%0d, ",   stats.acq_end.tai, stats.acq_end.cyc,
	       "sent frames: %0d, rcvd frames: %0d, ", stats.sent_frames, stats.rcvd_frames,
	       "lost frames: %0d, lost blocks: %0d, ", stats.lost_frames, stats.lost_blocks,
	       "min latency: %0d, max latency: %0d, ", stats.rx_latency.minimum, stats.rx_latency.maximum,
	       "acc latency: %0d, acc latency cnt: %0d, acc latency overflow: %b",
	       stats.rx_latency.acc, stats.rx_latency.acc_cnt, stats.rx_latency.acc_invalid);
   endtask // stats_display

   /*
    * Assert the RST_STATS bit to reset streamer stats
    */
   task stats_reset();
      uint32_t val;
      // Pulse RST_STATS
      modify32(`ADDR_WR_STREAMERS_SSCR1, `WR_STREAMERS_SSCR1_RST_STATS, `WR_STREAMERS_SSCR1_RST_STATS);
      modify32(`ADDR_WR_STREAMERS_SSCR1, `WR_STREAMERS_SSCR1_RST_STATS, ~`WR_STREAMERS_SSCR1_RST_STATS);
   endtask // stats_reset

   /*
    * Retrieve the timestamp from the latest reset
    */
   task stats_get_rst_tstamp(ref tstamp_t ts);
      uint32_t rval;
      read32(`ADDR_WR_STREAMERS_SSCR1, rval);
      ts.cyc = (rval & `WR_STREAMERS_SSCR1_RST_TS_CYC) >> `WR_STREAMERS_SSCR1_RST_TS_CYC_OFFSET;
      read32(`ADDR_WR_STREAMERS_SSCR2, rval);
      ts.tai = rval;
   endtask

   /*
    * Reset stats and record timestamp
    */
   task stats_acq_start();
      // Pulse RST_STATS
      stats_reset();
      // Read RST falling edge timestamp and update acq_start
      stats_get_rst_tstamp(stats.acq_start);
      // Reset acq_end stat values
      stats.acq_end = '{0,0};
   endtask // stats_acq_start

   /*
    * Take snapshot of running stats
    */
   task stats_acq_update();
      uint32_t mask, val;
      // enable snapshot bit
      mask = `WR_STREAMERS_SSCR1_SNAPSHOT_STATS;
      val  = `WR_STREAMERS_SSCR1_SNAPSHOT_STATS;
      modify32(`ADDR_WR_STREAMERS_SSCR1, mask, val);
      // refresh all stat values
      refresh_status();
      // disable snapshot bit
      modify32(`ADDR_WR_STREAMERS_SSCR1, mask, ~val);
   endtask; // stats_acq_update

   /*
    * Take snapshot and reset, record timestamp and update with final stats
    */
   task stats_acq_end();
      uint32_t mask, val;
      // enable snapshot and reset stats bits
      mask = `WR_STREAMERS_SSCR1_RST_STATS | `WR_STREAMERS_SSCR1_SNAPSHOT_STATS;
      val  = `WR_STREAMERS_SSCR1_RST_STATS | `WR_STREAMERS_SSCR1_SNAPSHOT_STATS;
      modify32(`ADDR_WR_STREAMERS_SSCR1, mask, val);
      // Read RST rising edge timestamp and update acq_end
      stats_get_rst_tstamp(stats.acq_end);
      // refresh all values at end of stats acquisition
      refresh_status();
      // disable snapshot and reset stats bits
      modify32(`ADDR_WR_STREAMERS_SSCR1, mask, ~val);
   endtask; // stats_acq_end

   /*
    * Set a fixed latency for the streamer
    */
   task set_fixed_latency (uint32_t cycles);
      // configure latency in clock cycles
      write32(`ADDR_WR_STREAMERS_RX_CFG5, cycles);
      // override default latency value with the one from the register
      modify32(`ADDR_WR_STREAMERS_CFG,
	       `WR_STREAMERS_CFG_OR_RX_FIX_LAT,
	       `WR_STREAMERS_CFG_OR_RX_FIX_LAT);
   endtask // set_fixed_latency


endclass // CWBStreamer

/*
 * BTrain WB slave class
 */
class CWBBTrain extends CWBPeriph;

   const uint32_t c_DUMMY_VAL = 'h0000CAFE;

   // FIXME: make these protected once we figure out how to connect them to the DUT
   // through the constructor
   static logic rx_valid;

   function new(const ref string name,
		const ref CBusAccessor bus,
		const ref uint32_t base);
      // call parent contructor
      super.new(name, bus, base);
   endfunction // new

   task refresh_status ();
      uint32_t rval;
      read32(`ADDR_BTRAIN_SCR, rval);
      rval = (`BTRAIN_SCR_DUMMY & rval) >> `BTRAIN_SCR_DUMMY_OFFSET;
      if (c_DUMMY_VAL == rval)
	present = 1;
      else
	present = 0;
   endtask // refresh_status

   /*
    * program the b-value simulation generator
    *
    *      B=20      *
    *               * *
    *              *   *
    *             *     *
    *            *       *
    *           *         *
    *          *           *
    *         *             *
    *        *               *   *
    *       *                 * *
    *  B=0 *                   *
    *      |cycNum=0 |cycNum=1 |cycNum=
    */
   task simgen_config (int cycLen[], cycInc[], size);

      uint32_t val, cycNum;
      int i;

      // read the max number of subcycles
      read32(`ADDR_BTRAIN_SIMB_CTRL, val);
      val >>= `BTRAIN_SIMB_CTRL_CMAXLEN_OFFSET;
      assert (val > size) begin
	 $display("[%t] %s: configuring %0d SimGen subcycles (max allowed = %0d)",
		  $time, name, size, val);
	 cycNum = size;
      end
      else begin
	 $error("[%t] %s: %0d SimGen subcycles not supported (capped to %0d)",
		$time, name, size, val);
	 cycNum = val;
      end
      // set number of subcycles
      write32(`ADDR_BTRAIN_SIMB_CTRL, cycNum);
      // loop through and program each subcycle
      for (i = 0; i < cycNum; i++) begin
	 $display("[%t] %s: configuring SimGen subcycle #%0d: LEN = %0d, INC = %0d",
		  $time, name, i, cycLen[i], cycInc[i]);
	 val = (i <<`BTRAIN_SIMB_CTRL_SCID_OFFSET);
	 modify32(`ADDR_BTRAIN_SIMB_CTRL, `BTRAIN_SIMB_CTRL_SCID, val);
	 write32(`ADDR_BTRAIN_BSIM_SCYC_LEN, cycLen[i]);
	 write32(`ADDR_BTRAIN_BSIM_SCYC_INC, cycInc[i]);
      end

   endtask // simgen_config

   /*
    * Send a single frame
    */
   task send_frame_single();
      $display("[%t] %s: sending single frame", $time, name);
      modify32(`ADDR_BTRAIN_SCR, `BTRAIN_SCR_TX_SINGLE, `BTRAIN_SCR_TX_SINGLE);
   endtask; // send_frame_single

   /*
    * Configure and enable periodic sending of frames
    */
   task send_frame_periodic(uint16_t period);
      $display("[%t] %s: sending periodic frames every %0d cycles", $time, name, period);
      // override default TX configuration with the one from the register
      modify32(`ADDR_BTRAIN_SCR, `BTRAIN_SCR_TX_OR_CONFIG, `BTRAIN_SCR_TX_OR_CONFIG);
      write32(`ADDR_BTRAIN_TX_PERIOD, period);
   endtask; // send_frame_periodic

   /*
    * Configure the length, delay and polarity of the RX frame valid signal
    */
   task set_rx_valid(uint16_t length, delay, bit invert);
      uint32_t val, mask;
      $display("[%t] %s: setting RX valid length = %0d cycles, delay = %0d cycles, invert = %b",
	       $time, name, length, delay, invert);
      // override default RX configuration with the one from the register and set invert bit
      mask = `BTRAIN_SCR_RX_VALID_POL_INV | `BTRAIN_SCR_RX_OR_CONFIG;
      val  = (invert << `BTRAIN_SCR_RX_VALID_POL_INV_OFFSET)  | `BTRAIN_SCR_RX_OR_CONFIG;
      modify32(`ADDR_BTRAIN_SCR, mask, val);
      val = (delay << `BTRAIN_RX_OUT_DATA_TIME_DELAY_OFFSET) | length;
      #1us;
      write32(`ADDR_BTRAIN_RX_OUT_DATA_TIME, val);
   endtask; // set_rx_valid_len

   /*
    * Set debugging mode
    * 0: disabled
    * 1: send counter
    * 2: send simulated waveform (need to program the waveform)
    * 3: send constant values (dummy frame)
    */
   task set_debug_mode(byte mode);
      uint32_t val;
      $display("[%t] %s: setting TX debug mode to %0d", $time, name, mode);
      val = mode << `BTRAIN_SCR_TX_DBG_OFFSET;
      modify32(`ADDR_BTRAIN_SCR, `BTRAIN_SCR_TX_DBG, val);
   endtask; // set_debug_mode

endclass // CWBBTrain

/*
 * The BTrain "management" class
 *
 * Includes one instance of CWBStreamer and CWBBTrain.
 * Peripheral base offsets corresond to the BTrain reference design.
 */
class CWBBTRainMgt;

   // base offsets/pointers for WR peripherals
   const uint32_t c_BASE_WRPC        = 'h00040000;
   const uint32_t c_BASE_AUX_IN_WRPC = 'h00020700;
   const uint32_t c_BASE_STREAMERS   = c_BASE_WRPC + c_BASE_AUX_IN_WRPC;
   const uint32_t c_BASE_BTRAIN      = 'h00001200;

   CWBStreamer wrstream;
   CWBBTrain wrbtrain;

   protected string name;

   function new(input string name, const ref CBusAccessor bus);
      // string to be used in various messages etc.
      this.name = name;
      // Handles to WR streamer and BTrain WB peripherals
      this.wrstream = new(name, bus, c_BASE_STREAMERS);
      this.wrbtrain = new(name, bus, c_BASE_BTRAIN);
   endfunction // new

   /*
    * Refresh status for both peripherals and make sure they are present
    */
   task init();
      wrstream.refresh_status();
      wrbtrain.refresh_status();
      assert ((wrstream.present == 1) && (wrbtrain.present == 1))
	$display("[%t] %s: init OK", $time, name);
      else
	$fatal(1, "[%t] %s: init FAILED", $time, name);
   endtask // refresh_status

   /*
    * Initialize class and reset streamer stats
    */
   task run();
      init();
      wrstream.stats_acq_start();
   endtask // run

   /*
    * Take snapshot of streamer stats and display them
    */
   task wrap_up();
      wrstream.stats_acq_end();
      wrstream.stats_display();
   endtask // wrap_up

endclass // CWBBTRainMgt
