# Modelsim run script for continuous integration
# execute: vsim -c -do "run_ci.do"
vsim -quiet -t 1ps -L unisim work.main -voptargs="+acc" -sv_seed random
set StdArithNoWarnings 1
set NumericStdNoWarnings 1
run -all

