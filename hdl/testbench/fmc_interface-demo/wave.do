onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /main/DUT_SPEC_A/cmp_xwrc_board_spec/link_ok_o
add wave -noupdate /main/DUT_SPEC_B/cmp_xwrc_board_spec/link_ok_o
add wave -noupdate -divider {Interesting signals}
add wave -noupdate /main/I_GennumA/ready
add wave -noupdate /main/I_GennumB/ready
add wave -noupdate /main/a_to_b_n
add wave -noupdate /main/b_to_a_n
add wave -noupdate /main/b_to_a_p_i
add wave -noupdate /main/a_to_b_p_i
add wave -noupdate -expand -group DUT_A->tx -format Analog-Step -height 84 -max 55.0 /main/DUT_SPEC_A/cmp_btrain/wb_in.btrain_dbg_tx_b_or_i_i
add wave -noupdate -expand -group DUT_B->tx -format Analog-Step -height 84 -label btrain_dbg_tx_b_or_i_i -max 80.0 /main/DUT_SPEC_B/cmp_btrain/wb_in.btrain_dbg_tx_b_or_i_i
add wave -noupdate -expand -group DUT_B->rx -format Analog-Step -height 84 -label btrain_dbg_rx_b_or_i_i -max 80.0 /main/DUT_SPEC_B/cmp_btrain/wb_in.btrain_dbg_rx_b_or_i_i
add wave -noupdate -expand -group DUT_A->rx -format Analog-Step -height 84 -label btrain_dbg_rx_b_or_i_i -max 80.0 /main/DUT_SPEC_A/cmp_btrain/wb_in.btrain_dbg_rx_b_or_i_i
add wave -noupdate -group DUT_A -group Tx -format Analog-Step -height 84 -max 20.0 /main/DUT_SPEC_A/cmp_btrain/wb_in.btrain_dbg_tx_b_or_i_i
add wave -noupdate -group DUT_A -group Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/FrameHeader.frame_type
add wave -noupdate -group DUT_A -group Tx /main/DUT_SPEC_A/cmp_btrain/send_BTrain_frame
add wave -noupdate -group DUT_A -group Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/fsm_out.sof
add wave -noupdate -group DUT_A -group Tx -radix unsigned /main/DUT_SPEC_A/cmp_btrain/tx_period_value
add wave -noupdate -group DUT_A -expand -group Rx /main/DUT_SPEC_A/cmp_btrain/rx_Frame_valid_pX_o
add wave -noupdate -group DUT_A -expand -group Rx /main/DUT_SPEC_A/cmp_btrain/rx_Frame_typeID_o
add wave -noupdate -group DUT_A -expand -group Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_first_p1_o
add wave -noupdate -group DUT_A -expand -group Rx -radix unsigned /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_latency_o
add wave -noupdate -group DUT_A -expand -group Rx -label fixed_latency -radix unsigned /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_streamer_cfg_i.fixed_latency
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/clk_i
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rst_n_i
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/tx_data_o
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/tx_valid_o
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/tx_dreq_i
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/tx_last_p1_o
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/tx_flush_p1_o
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_data_i
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_valid_i
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_first_p1_i
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_dreq_o
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_last_p1_i
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_FrameHeader_o
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_BFramePayloads_o
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_IFramePayloads_o
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_PFramePayloads_o
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_Frame_valid_pX_o
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_Frame_typeID_o
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/ready_o
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/tx_TransmitFrame_p1_i
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/tx_FrameHeader_i
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/tx_BFramePayloads_i
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/tx_IFramePayloads_i
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/tx_PFramePayloads_i
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/wb_slave_i
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/wb_slave_o
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/send_BTrain_frame
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/send_tick_p
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/wb_in_tx_ctrl
add wave -noupdate -group DUT_A->BTrain -expand /main/DUT_SPEC_A/cmp_btrain/wb_in
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/wb_out
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_Frame_valid_p1
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_FrameHeader
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_BFramePayloads
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_IFramePayloads
add wave -noupdate -group DUT_A->BTrain -height 16 /main/DUT_SPEC_A/cmp_btrain/s_out_state
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/output_cnt
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_data_time_valid
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_data_time_delay
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/rx_valid_pol_inv
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/tx_period_value
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/sample_rate_cnt
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/tx_data
add wave -noupdate -group DUT_A->BTrain /main/DUT_SPEC_A/cmp_btrain/tx_valid
add wave -noupdate -group DUT_A->BTrain->Rx /main/DUT_SPEC_A/cmp_btrain/cmp_rxCtrl/clk_i
add wave -noupdate -group DUT_A->BTrain->Rx /main/DUT_SPEC_A/cmp_btrain/cmp_rxCtrl/rst_n_i
add wave -noupdate -group DUT_A->BTrain->Rx /main/DUT_SPEC_A/cmp_btrain/cmp_rxCtrl/rx_data_i
add wave -noupdate -group DUT_A->BTrain->Rx /main/DUT_SPEC_A/cmp_btrain/cmp_rxCtrl/rx_valid_i
add wave -noupdate -group DUT_A->BTrain->Rx /main/DUT_SPEC_A/cmp_btrain/cmp_rxCtrl/rx_first_p1_i
add wave -noupdate -group DUT_A->BTrain->Rx /main/DUT_SPEC_A/cmp_btrain/cmp_rxCtrl/rx_last_p1_i
add wave -noupdate -group DUT_A->BTrain->Rx /main/DUT_SPEC_A/cmp_btrain/cmp_rxCtrl/FrameHeader_o
add wave -noupdate -group DUT_A->BTrain->Rx /main/DUT_SPEC_A/cmp_btrain/cmp_rxCtrl/BFramePayloads_o
add wave -noupdate -group DUT_A->BTrain->Rx /main/DUT_SPEC_A/cmp_btrain/cmp_rxCtrl/IFramePayloads_o
add wave -noupdate -group DUT_A->BTrain->Rx /main/DUT_SPEC_A/cmp_btrain/cmp_rxCtrl/rxframe_valid_p1_o
add wave -noupdate -group DUT_A->BTrain->Rx /main/DUT_SPEC_A/cmp_btrain/cmp_rxCtrl/rxframe_type_o
add wave -noupdate -group DUT_A->BTrain->Rx /main/DUT_SPEC_A/cmp_btrain/cmp_rxCtrl/header
add wave -noupdate -group DUT_A->BTrain->Rx /main/DUT_SPEC_A/cmp_btrain/cmp_rxCtrl/payload
add wave -noupdate -group DUT_A->BTrain->Rx /main/DUT_SPEC_A/cmp_btrain/cmp_rxCtrl/frame_type
add wave -noupdate -group DUT_A->BTrain->Rx /main/DUT_SPEC_A/cmp_btrain/cmp_rxCtrl/frame_type_d
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/clk_i
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/rst_n_i
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/ctrl_send_i
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/ctrl_dbg_mode_i
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/ctrl_dbg_f_type_i
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/ready_o
add wave -noupdate -group DUT_A->BTrain->Tx -expand /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/FrameHeader_i
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/BFramePayloads_i
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/IFramePayloads_i
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/tx_sent_p1_o
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/tx_data_o
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/tx_valid_o
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/tx_dreq_i
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/tx_last_p1_o
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/tx_flush_p1_o
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/wbregs_i
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/wbregs_o
add wave -noupdate -group DUT_A->BTrain->Tx -height 16 /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/s_tx_state
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/FrameHeader
add wave -noupdate -group DUT_A->BTrain->Tx -expand /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/Btype_payload
add wave -noupdate -group DUT_A->BTrain->Tx -expand /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/Itype_payload
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/Ctype_payload
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/dbg_counter
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/tx_period_cnt
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/tx_sent_p1
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/simValue
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/simEnabled
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/frame_type_d
add wave -noupdate -group DUT_A->BTrain->Tx /main/DUT_SPEC_A/cmp_btrain/cmp_txCtrl/frame_type
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/clk_sys_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rst_n_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/src_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/src_o
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/snk_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/snk_o
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tx_data_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tx_valid_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tx_dreq_o
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tx_last_p1_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tx_flush_p1_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_first_p1_o
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_last_p1_o
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_data_o
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_valid_o
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_dreq_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/clk_ref_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tm_time_valid_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tm_tai_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tm_cycles_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/link_ok_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/wb_slave_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/wb_slave_o
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/snmp_array_o
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/snmp_array_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tx_streamer_cfg_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_streamer_cfg_i
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/to_wb
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/from_wb
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/dbg_word
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/start_bit
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_data
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/wb_regs_slave_in
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/wb_regs_slave_out
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tx_frame
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/reset_time_tai
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/latency_acc
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/latency_cnt
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/sent_frame_cnt_out
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rcvd_frame_cnt_out
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/lost_frame_cnt_out
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/lost_block_cnt_out
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_valid
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_latency_valid
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_latency
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_lost_frames
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_lost_frames_cnt
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_lost_blocks
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_frame
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tx_streamer_cfg
add wave -noupdate -group DUT_A->WR_Streamers /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_streamer_cfg
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/clk_sys_i
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/rst_n_i
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/src_i
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/src_o
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/clk_ref_i
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tm_time_valid_i
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tm_tai_i
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tm_cycles_i
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/link_ok_i
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_data_i
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_dreq_o
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_last_p1_i
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_reset_seq_i
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_frame_p1_o
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_streamer_cfg_i
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_threshold_hit
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_timeout_hit
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_flush_latched
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_last
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_we
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_full
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_empty
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_rd
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_q
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_d
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/state
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/seq_no
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/count
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/ser_count
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/word_count
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/total_words
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/timeout_counter
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/pack_data
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/fsm_out
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/escaper
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/fab_src
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/fsm_escape
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/fsm_escape_enable
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/crc_en
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/crc_en_masked
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/crc_reset
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/crc_value
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_almost_empty
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_almost_full
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/buf_frame_count
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tag_cycles
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tag_valid
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tag_valid_latched
add wave -noupdate -group DUT_A->WR_Streamers->Tx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/link_ok_delay_cnt
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/clk_sys_i
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rst_n_i
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/snk_i
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/snk_o
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/clk_ref_i
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/tm_time_valid_i
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/tm_tai_i
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/tm_cycles_i
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_first_p1_o
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_last_p1_o
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_data_o
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_valid_o
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_dreq_i
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_lost_p1_o
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_lost_blocks_p1_o
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_lost_frames_p1_o
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_lost_frames_cnt_o
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_latency_o
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_latency_valid_o
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_frame_p1_o
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx -expand /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_streamer_cfg_i
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fab
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fsm_in
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx -height 16 /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/state
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/ser_count
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/seq_no
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/seq_new
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/count
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/crc_match
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/crc_en
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/crc_en_masked
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/crc_restart
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/detect_escapes
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/is_escape
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_pending
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/pack_data
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_data
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_drop
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_accept
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_accept_d0
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_dvalid
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_sync
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_last
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/frames_lost
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/blocks_lost
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_dout
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_din
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/pending_write
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fab_dvalid_pre
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/tx_tag_cycles
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_tag_cycles
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/tx_tag_valid
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_tag_valid
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/got_next_subframe
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/is_frame_seq_id
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/word_count
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/sync_seq_no
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_latency
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_latency_stored
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_latency_valid
add wave -noupdate -expand -group DUT_A->WR_Streamers->Rx /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/is_vlan
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/clk_i
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/rst_n_i
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/sent_frame_i
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/rcvd_frame_i
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/lost_block_i
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/lost_frame_i
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/lost_frames_cnt_i
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/rcvd_latency_i
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/rcvd_latency_valid_i
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/clk_ref_i
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/tm_time_valid_i
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/tm_tai_i
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/tm_cycles_i
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/reset_stats_i
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/snapshot_ena_i
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/reset_time_tai_o
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/reset_time_cycles_o
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/sent_frame_cnt_o
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/rcvd_frame_cnt_o
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/lost_frame_cnt_o
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/lost_block_cnt_o
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/latency_cnt_o
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/latency_acc_overflow_o
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/latency_acc_o
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/latency_max_o
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/latency_min_o
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/snmp_array_o
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/snmp_array_i
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/reset_time_tai
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/reset_time_cycles
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/latency_max
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/latency_min
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/latency_acc
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/latency_acc_overflow
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/sent_frame_cnt_out
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/rcvd_frame_cnt_out
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/lost_frame_cnt_out
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/lost_block_cnt_out
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/latency_cnt_out
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/latency_acc_overflow_out
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/latency_acc_out
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/latency_max_out
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/latency_min_out
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/reset_stats_remote
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/reset_stats
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/reset_stats_d1
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/reset_stats_p
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/snapshot_remote_ena
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/snapshot_ena
add wave -noupdate -group DUT_A->WR_Streamers->Stats /main/DUT_SPEC_A/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/U_STATS/snapshot_ena_d1
add wave -noupdate -group DUT_B -expand -group Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/FrameHeader.frame_type
add wave -noupdate -group DUT_B -expand -group Tx /main/DUT_SPEC_B/cmp_btrain/send_BTrain_frame
add wave -noupdate -group DUT_B -expand -group Tx /main/DUT_SPEC_B/cmp_btrain/tx_period_value
add wave -noupdate -group DUT_B -expand -group Rx /main/DUT_SPEC_B/cmp_btrain/rx_Frame_valid_pX_o
add wave -noupdate -group DUT_B -expand -group Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_first_p1_o
add wave -noupdate -group DUT_B -expand -group Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_valid_o
add wave -noupdate -group DUT_B -expand -group Rx -label btrain_scr_rx_valid_pol_inv_o /main/DUT_SPEC_B/cmp_btrain/wb_out.btrain_scr_rx_valid_pol_inv_o
add wave -noupdate -group DUT_B -expand -group Rx -label btrain_rx_out_data_time_valid_o /main/DUT_SPEC_B/cmp_btrain/wb_out.btrain_rx_out_data_time_valid_o
add wave -noupdate -group DUT_B -expand -group Rx -label btrain_rx_out_data_time_delay_o /main/DUT_SPEC_B/cmp_btrain/wb_out.btrain_rx_out_data_time_delay_o
add wave -noupdate -group DUT_B -expand -group Rx /main/DUT_SPEC_B/cmp_btrain/rx_Frame_typeID_o
add wave -noupdate -group DUT_B -expand -group Rx -radix unsigned /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_latency_o
add wave -noupdate -group DUT_B -expand -group Rx -format Analog-Step -height 84 -max 20.0 /main/DUT_SPEC_B/cmp_btrain/wb_in.btrain_dbg_rx_b_or_i_i
add wave -noupdate -group DUT_B -expand -group Rx -label fixed_latency -radix unsigned /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_streamer_cfg_i.fixed_latency
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/clk_i
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rst_n_i
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/tx_data_o
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/tx_valid_o
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/tx_dreq_i
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/tx_last_p1_o
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/tx_flush_p1_o
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_data_i
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_valid_i
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_first_p1_i
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_dreq_o
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_last_p1_i
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_FrameHeader_o
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_BFramePayloads_o
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_IFramePayloads_o
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_PFramePayloads_o
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_Frame_valid_pX_o
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_Frame_typeID_o
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/ready_o
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/tx_TransmitFrame_p1_i
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/tx_FrameHeader_i
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/tx_BFramePayloads_i
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/tx_IFramePayloads_i
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/tx_PFramePayloads_i
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/wb_slave_i
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/wb_slave_o
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/send_BTrain_frame
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/send_tick_p
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/wb_in_tx_ctrl
add wave -noupdate -group DUT_B->BTrain -expand /main/DUT_SPEC_B/cmp_btrain/wb_in
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/wb_out
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_Frame_valid_p1
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_FrameHeader
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_BFramePayloads
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_IFramePayloads
add wave -noupdate -group DUT_B->BTrain -height 16 /main/DUT_SPEC_B/cmp_btrain/s_out_state
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/output_cnt
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_data_time_valid
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_data_time_delay
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/rx_valid_pol_inv
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/tx_period_value
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/sample_rate_cnt
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/tx_data
add wave -noupdate -group DUT_B->BTrain /main/DUT_SPEC_B/cmp_btrain/tx_valid
add wave -noupdate -group DUT_B->BTrain->Rx /main/DUT_SPEC_B/cmp_btrain/cmp_rxCtrl/clk_i
add wave -noupdate -group DUT_B->BTrain->Rx /main/DUT_SPEC_B/cmp_btrain/cmp_rxCtrl/rst_n_i
add wave -noupdate -group DUT_B->BTrain->Rx /main/DUT_SPEC_B/cmp_btrain/cmp_rxCtrl/rx_data_i
add wave -noupdate -group DUT_B->BTrain->Rx /main/DUT_SPEC_B/cmp_btrain/cmp_rxCtrl/rx_valid_i
add wave -noupdate -group DUT_B->BTrain->Rx /main/DUT_SPEC_B/cmp_btrain/cmp_rxCtrl/rx_first_p1_i
add wave -noupdate -group DUT_B->BTrain->Rx /main/DUT_SPEC_B/cmp_btrain/cmp_rxCtrl/rx_last_p1_i
add wave -noupdate -group DUT_B->BTrain->Rx -expand /main/DUT_SPEC_B/cmp_btrain/cmp_rxCtrl/FrameHeader_o
add wave -noupdate -group DUT_B->BTrain->Rx /main/DUT_SPEC_B/cmp_btrain/cmp_rxCtrl/BFramePayloads_o
add wave -noupdate -group DUT_B->BTrain->Rx -expand /main/DUT_SPEC_B/cmp_btrain/cmp_rxCtrl/IFramePayloads_o
add wave -noupdate -group DUT_B->BTrain->Rx -expand /main/DUT_SPEC_B/cmp_btrain/cmp_rxCtrl/CFramePayloads_o
add wave -noupdate -group DUT_B->BTrain->Rx /main/DUT_SPEC_B/cmp_btrain/cmp_rxCtrl/rxframe_valid_p1_o
add wave -noupdate -group DUT_B->BTrain->Rx /main/DUT_SPEC_B/cmp_btrain/cmp_rxCtrl/rxframe_type_o
add wave -noupdate -group DUT_B->BTrain->Rx /main/DUT_SPEC_B/cmp_btrain/cmp_rxCtrl/header
add wave -noupdate -group DUT_B->BTrain->Rx /main/DUT_SPEC_B/cmp_btrain/cmp_rxCtrl/payload
add wave -noupdate -group DUT_B->BTrain->Rx /main/DUT_SPEC_B/cmp_btrain/cmp_rxCtrl/frame_type
add wave -noupdate -group DUT_B->BTrain->Rx /main/DUT_SPEC_B/cmp_btrain/cmp_rxCtrl/frame_type_d
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/clk_i
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/rst_n_i
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/ctrl_send_i
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/ctrl_dbg_mode_i
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/ctrl_dbg_f_type_i
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/ready_o
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/FrameHeader_i
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/BFramePayloads_i
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/IFramePayloads_i
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/tx_sent_p1_o
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/tx_data_o
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/tx_valid_o
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/tx_dreq_i
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/tx_last_p1_o
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/tx_flush_p1_o
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/wbregs_i
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/wbregs_o
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/s_tx_state
add wave -noupdate -group DUT_B->BTrain->Tx -expand /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/FrameHeader
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/Btype_payload
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/Itype_payload
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/dbg_counter
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/tx_period_cnt
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/tx_sent_p1
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/simValue
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/simEnabled
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/frame_type_d
add wave -noupdate -group DUT_B->BTrain->Tx /main/DUT_SPEC_B/cmp_btrain/cmp_txCtrl/frame_type
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/clk_sys_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rst_n_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/src_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/src_o
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/snk_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/snk_o
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tx_data_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tx_valid_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tx_dreq_o
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tx_last_p1_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tx_flush_p1_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_first_p1_o
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_last_p1_o
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_data_o
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_valid_o
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_dreq_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/clk_ref_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tm_time_valid_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tm_tai_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tm_cycles_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/link_ok_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/wb_slave_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/wb_slave_o
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/snmp_array_o
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/snmp_array_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tx_streamer_cfg_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_streamer_cfg_i
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/to_wb
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/from_wb
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/dbg_word
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/start_bit
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_data
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/wb_regs_slave_in
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/wb_regs_slave_out
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tx_frame
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/reset_time_tai
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/latency_acc
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/latency_cnt
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/sent_frame_cnt_out
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rcvd_frame_cnt_out
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/lost_frame_cnt_out
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/lost_block_cnt_out
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_valid
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_latency_valid
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_latency
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_lost_frames
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_lost_frames_cnt
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_lost_blocks
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_frame
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/tx_streamer_cfg
add wave -noupdate -group DUT_B->WR_Streamers /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/rx_streamer_cfg
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/clk_sys_i
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/rst_n_i
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/src_i
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/src_o
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/clk_ref_i
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tm_time_valid_i
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tm_tai_i
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tm_cycles_i
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/link_ok_i
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_data_i
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_valid_i
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_dreq_o
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_last_p1_i
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_flush_p1_i
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_reset_seq_i
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_frame_p1_o
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_streamer_cfg_i
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_threshold_hit
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_timeout_hit
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_flush_latched
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_last
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_we
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_full
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_empty
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_rd
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_q
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_fifo_d
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/state
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/seq_no
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/count
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/ser_count
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/word_count
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/total_words
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/timeout_counter
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/pack_data
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/fsm_out
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/escaper
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/fab_src
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/fsm_escape
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/fsm_escape_enable
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/crc_en
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/crc_en_masked
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/crc_reset
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/crc_value
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_almost_empty
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tx_almost_full
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/buf_frame_count
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tag_cycles
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tag_valid
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/tag_valid_latched
add wave -noupdate -group DUT_B->WR_Streamers->Tx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_tx/U_TX/link_ok_delay_cnt
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/clk_sys_i
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rst_n_i
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/snk_i
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/snk_o
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/clk_ref_i
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/tm_time_valid_i
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/tm_tai_i
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/tm_cycles_i
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_last_p1_o
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_data_o
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_dreq_i
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_lost_p1_o
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_lost_blocks_p1_o
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_lost_frames_p1_o
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_lost_frames_cnt_o
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_latency_o
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_latency_valid_o
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_frame_p1_o
add wave -noupdate -group DUT_B->WR_Streamers->Rx -expand /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_streamer_cfg_i
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fab
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fsm_in
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/ser_count
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/seq_no
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/state
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/seq_new
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/count
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/crc_match
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/crc_en
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/crc_en_masked
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/crc_restart
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/detect_escapes
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/is_escape
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_pending
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/pack_data
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_data
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_drop
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_accept
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_accept_d0
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_dvalid
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_sync
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_last
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/frames_lost
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/blocks_lost
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_dout
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fifo_din
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/pending_write
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/fab_dvalid_pre
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/tx_tag_cycles
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_tag_cycles
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/tx_tag_valid
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_tag_valid
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/got_next_subframe
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/is_frame_seq_id
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/word_count
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/sync_seq_no
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_latency
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_latency_stored
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/rx_latency_valid
add wave -noupdate -group DUT_B->WR_Streamers->Rx /main/DUT_SPEC_B/cmp_xwrc_board_spec/cmp_board_common/gen_wr_streamers/cmp_xwr_streamers/gen_rx/U_RX/is_vlan
add wave -noupdate /main/DUT_SPEC_B/B_d1
add wave -noupdate /main/DUT_SPEC_B/B_d2
add wave -noupdate /main/DUT_SPEC_B/B_err_cnt
add wave -noupdate /main/DUT_SPEC_B/B_err
add wave -noupdate /main/DUT_SPEC_B/U_BT_FMC_IF/rx_Frame_valid_pX_i
add wave -noupdate /main/DUT_SPEC_B/U_BT_FMC_IF/rx_FrameHeader_i
add wave -noupdate /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_FrameHeader_o
add wave -noupdate -expand /main/DUT_SPEC_B/U_BT_FMC_IF/rx_BFramePayloads_i
add wave -noupdate /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_BFramePayloads_o
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/clk_sys_i
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rst_sys_n_i
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_FrameHeader_i
add wave -noupdate -expand -group DUT_B->BF_FMC_IF -expand /main/DUT_SPEC_B/U_BT_FMC_IF/rx_BFramePayloads_i
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_IFramePayloads_i
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_Frame_typeID_i
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_ready_i
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_TransmitFrame_p1_o
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_FrameHeader_o
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_BFramePayloads_o
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_IFramePayloads_o
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_CFramePayloads_o
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_wb_clk_i
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_wb_slave_i
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_wb_slave_o
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_wb_clk_i
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_wb_slave_i
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_wb_slave_o
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_FrameHeader
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_BFramePayloads
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_IFramePayloads
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_Frame_valid_pX_i
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_BFramePayloads_i.B
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_Frame_typeID_reg
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_FrameIrq_o
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_FrameIrq
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_Frame_valid_p1
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_BFramePayloads.B
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_Frame_valid_p1_reg
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_BFramePayloads_reg.B
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_FrameHeader_reg
add wave -noupdate -expand -group DUT_B->BF_FMC_IF -expand /main/DUT_SPEC_B/U_BT_FMC_IF/rx_BFramePayloads_reg
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_IFramePayloads_reg
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_CFramePayloads_reg
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_scr_d
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_reg_in
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_reg_out
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_wb_regs_slave_in
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_wb_regs_slave_out
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_Frame_typeID
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_FrameHeader
add wave -noupdate -expand -group DUT_B->BF_FMC_IF -expand /main/DUT_SPEC_B/U_BT_FMC_IF/rx_BFramePayloads
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/rx_IFramePayloads
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_CFramePayloads
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_wb_regs_slave_in
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_wb_regs_slave_out
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_reg_in
add wave -noupdate -expand -group DUT_B->BF_FMC_IF /main/DUT_SPEC_B/U_BT_FMC_IF/tx_reg_out
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/clk_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rst_n_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_Irq_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_wb_clk_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_wb_adr_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_wb_dat_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_wb_dat_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_wb_ack_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_wb_cyc_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_wb_stb_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_wb_we_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_wb_err_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_wb_rty_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_wb_stall_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_wb_clk_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_wb_adr_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_wb_dat_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_wb_dat_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_wb_cyc_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_wb_stb_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_wb_we_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_wb_ack_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_wb_err_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_wb_rty_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_wb_stall_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/s_rx_fsm
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_PFieldsReadVector_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_Frame_typeID_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_FrameHeader_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_Frame_valid_p1_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF -expand /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_BFramePayloads_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_IFramePayloads_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_CFramePayloads_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_Frame_typeID_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_ready_o
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_PFieldsUpdateVector_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_TransmitFrame_p1_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF -expand /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_FrameHeader_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF -expand /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_BFramePayloads_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF -expand /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_IFramePayloads_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF -expand /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_CFramePayloads_i
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/s_tx_fsm
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_wb_adr
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_wb_dat
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_wb_cyc
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_wb_stb
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_wb_we
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_addr_base
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_ready
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_header
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_payload
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_frameType
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_b
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_f
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/tx_payload_size
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_wb_adr
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_wb_dat
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_wb_cyc
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_wb_stb
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_wb_we
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_frameType
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_header
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_payload
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_b
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_f
add wave -noupdate -expand -group DUT_B->BT_CARRIER_IF /main/DUT_SPEC_B/U_BT_CARRIER_IF/rx_payload_size
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/tx_data
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/tx_valid
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/tx_dreq
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/tx_last_p1
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/tx_flush_p1
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/rx_data
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/rx_valid
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/rx_first_p1
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/rx_dreq
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/rx_last_p1
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/tx_ready
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/tx_TransmitFrame_p1
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/tx_FrameHeader
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/tx_BFramePayloads
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/tx_IFramePayloads
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/tx_CFramePayloads
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/rx_FrameHeader
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/rx_BFramePayloads
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/rx_IFramePayloads
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/rx_CFramePayloads
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/rx_Frame_valid_pX
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/rx_Frame_typeID
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/rx_cfg_pol_inv
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/bup_out
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/bdown_out
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/rx_FrameIrq
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/rx_wb_clk
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/rx_wb_slave_in
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/rx_wb_slave_out
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/tx_wb_clk
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/tx_wb_slave_in
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/tx_wb_slave_out
add wave -noupdate -group DUT_B->top -expand /main/DUT_SPEC_B/carrier_tx_FrameHeader
add wave -noupdate -group DUT_B->top -expand /main/DUT_SPEC_B/carrier_tx_BFramePayloads
add wave -noupdate -group DUT_B->top -expand /main/DUT_SPEC_B/carrier_tx_IFramePayloads
add wave -noupdate -group DUT_B->top -expand /main/DUT_SPEC_B/carrier_tx_CFramePayloads
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/carrier_tx_ready
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/carrier_tx_send
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/cnt
add wave -noupdate -group DUT_B->top /main/DUT_SPEC_B/B_dummy
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 2} {368212000 ps} 1} {{Cursor 3} {370580000 ps} 1} {{Cursor 3} {474580000 ps} 0} {{Cursor 4} {3273991187 ps} 0}
configure wave -namecolwidth 383
configure wave -valuecolwidth 95
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {1446972417 ps}
