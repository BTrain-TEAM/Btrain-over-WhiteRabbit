#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <time.h>

#include <libdevmap.h>
#include <extest.h>

#include <BTrain.h>

static struct mapping_desc *btrain = NULL;

static char *btrain_txdbg_options[] = {
	"debugging disabled",
	"send values of incremented counters in the frames",
	"use for B value provided by the BvalueSimGen",
	"send frames with dummy content (see BTrani FrameTransceiver_pkg),"
	"programmed separetaly"
};

static int get_set_tx_debug(struct cmd_desc *cmdd, struct atom *atoms)
{
	volatile struct BTRAIN_WB *ptr =
		(volatile struct BTRAIN_WB *)btrain->base;
	uint32_t dbg, val;

	if (atoms == (struct atom *)VERBOSE_HELP) {
		printf("%s - Get/Set debug mode [options]\n"
		       "\t0: %s\n \t1: %s\n \t2: %s\n \t3: %s\n",
		       cmdd->name, btrain_txdbg_options[0], btrain_txdbg_options[1],
		       btrain_txdbg_options[2], btrain_txdbg_options[3]);
		return 1;
	}

	++atoms;
	if (atoms->type != Terminator){
		if (atoms->type != Numeric)
		      return -TST_ERR_WRONG_ARG;
		dbg = atoms->val;
		val = iomemr32(btrain->is_be, ptr->SCR);
		val &= ~BTRAIN_SCR_TX_DBG_MASK;
		val |= BTRAIN_SCR_TX_DBG_W(dbg);
		ptr->SCR = iomemw32(btrain->is_be, val);
	}
	val = BTRAIN_SCR_TX_DBG_R(iomemr32(btrain->is_be, ptr->SCR));

	fprintf(stderr, "Debug mode: %d: %s\n", val, btrain_txdbg_options[val]);
	return 1;
}

static char *btrain_ftdbg_options[] = {
	"do not enforce (send type that is input to module)",
	"force B frames",
	"force I frames",
	"force C frames",
};

static int get_set_ft_debug(struct cmd_desc *cmdd, struct atom *atoms)
{
	volatile struct BTRAIN_WB *ptr =
		(volatile struct BTRAIN_WB *)btrain->base;
	uint32_t dbg, val;

	if (atoms == (struct atom *)VERBOSE_HELP) {
		printf("%s - Get/Set debug forced frame type [options]\n"
		       "\t0: %s\n \t1: %s\n \t2: %s\n \t3: %s\n",
		       cmdd->name, btrain_ftdbg_options[0], btrain_ftdbg_options[1],
		       btrain_ftdbg_options[2], btrain_ftdbg_options[3]);
		return 1;
	}

	++atoms;
	if (atoms->type != Terminator) {
		if(atoms->type != Numeric)
			return -TST_ERR_WRONG_ARG;
		dbg = atoms->val;
		val = iomemr32(btrain->is_be, ptr->SCR);
		val &= ~BTRAIN_SCR_TX_DBG_FTYPE_MASK;
		val |= BTRAIN_SCR_TX_DBG_FTYPE_W(dbg);
		ptr->SCR = iomemw32(btrain->is_be, val);
	}
	val = BTRAIN_SCR_TX_DBG_FTYPE_R(iomemr32(btrain->is_be, ptr->SCR));
	fprintf(stderr, "Debug frame type: %d: %s\n", val, btrain_ftdbg_options[val]);
	return 1;
}

static int get_last_tx_frame(struct cmd_desc *cmdd, struct atom *atoms)
{
	volatile struct BTRAIN_WB *ptr =
		(volatile struct BTRAIN_WB *)btrain->base;

	if (atoms == (struct atom *)VERBOSE_HELP) {
		printf("%s - It provides access to the most recently "
		       "transmitted value of B or I or PFW, depending "
		       "on the BTrain frame type. (see ftdbg)", cmdd->name);
		return 1;
	}

	fprintf(stderr, "Last TX frame: 0x%x\n",
		iomemr32(btrain->is_be, ptr->DBG_TX_B_OR_I));
	return 1;
}

static int get_last_rx_frame(struct cmd_desc *cmdd, struct atom *atoms)
{
	volatile struct BTRAIN_WB *ptr =
		(volatile struct BTRAIN_WB *)btrain->base;

	if (atoms == (struct atom *)VERBOSE_HELP) {
		printf("%s - It provides access to the most recently "
		       "received value of B or I or PFW, depending "
		       "on the BTrain frame type. (see ftdbg)", cmdd->name);
		return 1;
	}

	fprintf(stderr, "Last RX frame: 0x%x\n",
		iomemr32(btrain->is_be, ptr->DBG_RX_B_OR_I));
	return 1;
}

static int set_single_tx_frame(struct cmd_desc *cmdd, struct atom *atoms)
{
	volatile struct BTRAIN_WB *ptr =
		(volatile struct BTRAIN_WB *)btrain->base;

	if (atoms == (struct atom *)VERBOSE_HELP) {
		printf("%s - %s\n", cmdd->name, cmdd->help);
		return 1;
	}

	ptr->SCR |= iomemw32(btrain->is_be, BTRAIN_SCR_TX_SINGLE);
	fprintf(stderr, "set transmit single WR BTrain frame flag\n");
	return 1;
}

static int get_set_tx_period(struct cmd_desc *cmdd, struct atom *atoms)
{
	volatile struct BTRAIN_WB *ptr =
		(volatile struct BTRAIN_WB *)btrain->base;
	int period;
	uint32_t val;
	if (atoms == (struct atom *)VERBOSE_HELP) {
		printf("%s get/set config of transmission period in "
		       "integer [us] (-1 to enable default config that "
		       "is set by the generic).\n "
		       "When the value is not zero, frames are sent "
		       "with that period.\n", cmdd->name);
		return 1;
	}

	++atoms;
	if (atoms->type == Terminator) {
		// Get Latency
		fprintf(stderr, "Transmission period: %d [us]\n",
			(iomemr32(btrain->is_be, ptr->TX_PERIOD) * 16) / 1000);
	}
	else {
		if (atoms->type != Numeric)
			return -TST_ERR_WRONG_ARG;
		period = atoms->val;
		if (period < 0) {
			val = ~BTRAIN_SCR_TX_OR_CONFIG &
			      iomemr32(btrain->is_be, ptr->SCR);
			ptr->SCR = iomemw32(btrain->is_be, val);
			fprintf(stderr, "Disabled overriding of default transmission "
				"period (it is now the default/set "
				"by generic)\n");
		}
		else {
			val = (period * 1000) / 16;
			ptr->TX_PERIOD = iomemw32(btrain->is_be,val);
			val = iomemr32(btrain->is_be, ptr->SCR);
			val |= BTRAIN_SCR_TX_OR_CONFIG;
			ptr->SCR = iomemw32(btrain->is_be, val);
			val = iomemr32(btrain->is_be, ptr->TX_PERIOD);
			fprintf(stderr, "Transmission period set: %d [us]"
				"(set %d | read : %d [cycles] | raw: 0x%x)\n",
				period, ((period * 1000) / 16), val, val);
		}
	}

	return 1;
}

static int get_set_data_valid_cfg(struct cmd_desc *cmdd, struct atom *atoms)
{
	volatile struct BTRAIN_WB *ptr =
		(volatile struct BTRAIN_WB *)btrain->base;
	uint32_t val;
	int delay, time, polarity, or;
	int found_err;

	if (atoms == (struct atom *)VERBOSE_HELP) {
		printf("%s - [options]\n"
		       "without option returns RX output data valid setting.\n"
		       "options: -d delay -t length -p polarity\n"
		       "\t-d: Output data delay in clock cylces (16ns) "
		       "between receiving data and making it valid\n"
		       "\t-l: length of valid HIGH in clock cycles (16ns) during which "
		       "data is valid. Two special values\n"
		       "\t\t0x0000: output data disabled\n"
		       "\t\t0xFFFF: output data continuously valid until next update\n"
		       "\t-p: Invert RX valid polarity 0:active high 1:active low.\n",
		       cmdd->name);
		return 1;
	}

	++atoms;
	if (atoms->type != Terminator) { // set data valid
		found_err = 1;
		delay = time = polarity = -1;
		while (atoms->type != Terminator) {
			if (!strncmp(atoms->text, "-d", 2)) {
				++atoms; //next is the value
				if (atoms->type != Numeric) {
					found_err = 1;
					break;
				}
				delay = atoms->val;
				found_err = 0;
			}
			if (!strncmp(atoms->text, "-l", 2)) {
				++atoms; //next is the value
				if (atoms->type != Numeric) {
					found_err = 1;
					break;
				}
				time = atoms->val;
				found_err = 0;
			}
			if (!strncmp(atoms->text, "-p", 2)) {
				++atoms; // next is the value
				if (atoms->type != Numeric) {
					found_err = 1;
					break;
				}
				polarity = atoms->val;
				found_err = 0;
			}
			++atoms;
		}
		if (found_err != 0) {
			fprintf(stderr, "Wrong arguments. "
				"Please check %s usage by running: h %s\n",
				cmdd->name, cmdd->name);
			return -1;
		}
		if (delay != -1) {
			val = iomemr32(btrain->is_be, ptr->RX_OUT_DATA_TIME);
			/* Reset current delay value and set new one */
			val &= ~BTRAIN_RX_OUT_DATA_TIME_DELAY_MASK;
			val |= (delay << BTRAIN_RX_OUT_DATA_TIME_DELAY_SHIFT);
			ptr->RX_OUT_DATA_TIME = iomemw32(btrain->is_be, val);
		}
		if (time != -1) {
			val = iomemr32(btrain->is_be, ptr->RX_OUT_DATA_TIME);
			/* Reset current time period value and set new one */
			val &= ~BTRAIN_RX_OUT_DATA_TIME_VALID_MASK;
			val |= (time << BTRAIN_RX_OUT_DATA_TIME_VALID_SHIFT);
			ptr->RX_OUT_DATA_TIME = iomemw32(btrain->is_be, val);
		}
		if (polarity != -1) {
			val = iomemr32(btrain->is_be, ptr->SCR);
			if (polarity)
				val |= BTRAIN_SCR_RX_VALID_POL_INV;
			else
				val &= ~BTRAIN_SCR_RX_VALID_POL_INV;
			ptr->SCR = iomemw32(btrain->is_be, val);
		}
		/* enable override of default parameters in generics */
		val = iomemr32(btrain->is_be, ptr->SCR);
		val |= BTRAIN_SCR_RX_OR_CONFIG;
		ptr->SCR = iomemw32(btrain->is_be, val);
	}
	/* Read back settings from HW */
	val = iomemr32(btrain->is_be, ptr->RX_OUT_DATA_TIME);
	delay = (val & BTRAIN_RX_OUT_DATA_TIME_DELAY_MASK) >>
		BTRAIN_RX_OUT_DATA_TIME_DELAY_SHIFT;
	time = (val & BTRAIN_RX_OUT_DATA_TIME_VALID_MASK) >>
		BTRAIN_RX_OUT_DATA_TIME_VALID_SHIFT;
	val = iomemr32(btrain->is_be, ptr->SCR);
	polarity = (val & BTRAIN_SCR_RX_VALID_POL_INV) !=0;
	or = (val & BTRAIN_SCR_RX_OR_CONFIG) !=0;
	fprintf(stderr, "RX output data valid configuration (%s):\n"
		"\tdelay: %d [clk cyc (16ns)] | length: %d [clk cyc (16ns)] "
		"| polarity: %d (0: positive, 1: negative)\n",
		(or) ? "enabled" : "disabled", delay, time, polarity);
	return 1;
}

static int get_set_data_valid_or(struct cmd_desc *cmdd, struct atom *atoms)
{
	volatile struct BTRAIN_WB *ptr =
		(volatile struct BTRAIN_WB *)btrain->base;
	uint32_t val;
	int or;

	if (atoms == (struct atom *)VERBOSE_HELP) {
		printf("%s - [options]\n"
		       "0 - do not override default configuration (set with generics) "
		       "with the WB configuration set using command dv\n"
		       "1 - override default configuration (set with generics) "
		       "with the WB configuration set using command dv\n",
		       cmdd->name);
		return 1;
	}

	++atoms;
	if (atoms->type != Terminator) { 
		if (atoms->type != Numeric)
			return -TST_ERR_WRONG_ARG;
		or = (atoms->val) ? 1 : 0; /* convert input into 0 or 1 */
		val = iomemr32(btrain->is_be, ptr->SCR);
		if (or)
			val |= BTRAIN_SCR_RX_OR_CONFIG;
		else
			val &= ~BTRAIN_SCR_RX_OR_CONFIG;
		ptr->SCR = iomemw32(btrain->is_be, val);
	}
	/* Read back settings from HW */
	val = iomemr32(btrain->is_be, ptr->SCR);
	or = (val & BTRAIN_SCR_RX_OR_CONFIG) !=0;
	fprintf(stderr, "Overriding of the defaultRX output data valid"
		" configuration with WB config: %s\n", 
		(or) ? "enabled" : "disabled");
	return 1;
}

static int set_sim_subcycle(struct cmd_desc *cmdd, struct atom *atoms)
{
	volatile struct BTRAIN_WB *ptr =
		(volatile struct BTRAIN_WB *)btrain->base;
	uint32_t length, inc, scid, regval, cycle_length;
	int i;

	if (atoms == (struct atom *)VERBOSE_HELP) {
		printf("%s - options\n"
		       "\tWithout option, returns configuration of "
		       "simulation sub cycles\n"
		       "\tscid: sub cycle id\n"
		       "\tlength: sub cycle length (in number of "
		       "transmitted frames\n"
		       "\tinc: sub cycle increment\n",
		       cmdd->name);
		return 1;
	}

	++atoms;
	if (atoms->type == Terminator) {
		cycle_length = BTRAIN_SIMB_CTRL_CMAXLEN_R(
				iomemr32(btrain->is_be, ptr->SIMB_CTRL));
		fprintf(stderr, "BTrain simulation cycles configuration:\n"
			"maximum cycle length: %d sub-cycles\n"
			"sub cycles list:\n", cycle_length);
		for (i = 0; i < cycle_length; ++i) {
			/* write sub cycle id */
			regval = iomemr32(btrain->is_be, ptr->SIMB_CTRL);
			regval &= ~BTRAIN_SIMB_CTRL_SCID_MASK; // reset scid bitfield
			regval |= (i << BTRAIN_SIMB_CTRL_SCID_SHIFT);// set scid
			ptr->SIMB_CTRL = iomemw32(btrain->is_be, regval);

			/* read subcycle length and increment */
			inc = iomemr32(btrain->is_be, ptr->BSIM_SCYC_INC);
			/* inc is a 16-bit signed value, convert into 32-bit signed 
			   if negative*/
			if(inc & 0x8000)
				inc = inc - 0x10000;
			length = iomemr32(btrain->is_be, ptr->BSIM_SCYC_LEN);
			fprintf(stderr, "\tsub cycle id: %d | length: %d (num of tx "
				"frames) | inc: %d\n", i, length, inc);
		}
		return 1;
	}
	/* this command expects 3 mandatory numerical args */
	for (i = 0; i < cmdd->comp; ++i) {
		if (atoms[i].type != Numeric) {
			fprintf(stderr, "Wrong arguments. "
				"Please check %s usage by running: h %s\n",
				cmdd->name, cmdd->name);
			return -TST_ERR_WRONG_ARG;
		}
	}
	scid = atoms->val;
	++atoms;
	length = atoms->val;
	++atoms;
	inc = atoms->val;

	/* Set SCID bit field */
	regval = iomemr32(btrain->is_be, ptr->SIMB_CTRL);
	regval &= ~BTRAIN_SIMB_CTRL_SCID_MASK; // reset scid bitfield
	regval |= (scid << BTRAIN_SIMB_CTRL_SCID_SHIFT);// set scid
	ptr->SIMB_CTRL = iomemw32(btrain->is_be, regval);

	/* Set sub cycle length */
	ptr->BSIM_SCYC_LEN = iomemw32(btrain->is_be, length);

	/* Set sub cycle increment */
	ptr->BSIM_SCYC_INC = iomemw32(btrain->is_be, inc);

	fprintf(stderr, "Set BTrain simulation sub cycle: scid: %d "
		"length: %d increment: %d\n",
		scid, length, inc);
	return 1;
}

static int get_set_sim_cycle(struct cmd_desc *cmdd, struct atom *atoms)
{
	volatile struct BTRAIN_WB *ptr =
		(volatile struct BTRAIN_WB *)btrain->base;
	uint32_t inc, length, cycle_length, regval;
	int i;

	if (atoms == (struct atom *)VERBOSE_HELP) {
		printf("%s - options\n"
		       "\tWithout option, returns Btrain "
		       "simulation cycle composition\n"
		       "\tcyclelength: number of sub cycles composing the cycle\n",
		       cmdd->name);
		return 1;
	}

	++atoms; //points to the first parameter
	if (atoms->type != Terminator) { // set cycle period and length
		if (atoms->type != Numeric)
			return -TST_ERR_WRONG_ARG;

		cycle_length = atoms->val;

		/* set cycle length */
		regval = iomemr32(btrain->is_be, ptr->SIMB_CTRL);
		regval &= ~BTRAIN_SIMB_CTRL_CLEN_MASK;
		regval |= (cycle_length << BTRAIN_SIMB_CTRL_CLEN_SHIFT);
		ptr->SIMB_CTRL = iomemw32(btrain->is_be, regval);
	}
	// display cycle composition
	cycle_length = (iomemr32(btrain->is_be, ptr->SIMB_CTRL) &
		        BTRAIN_SIMB_CTRL_CLEN_MASK) >>
			BTRAIN_SIMB_CTRL_CLEN_SHIFT;
	fprintf(stderr, "BTrain simulation cycle composition:\n"
		"cycle length: %d sub-cycles\n"
		"sub cycles list:\n", cycle_length);
	for (i = 0; i < cycle_length; ++i) {
		/* write sub cycle id */
		regval = iomemr32(btrain->is_be, ptr->SIMB_CTRL);
		regval &= ~BTRAIN_SIMB_CTRL_SCID_MASK; // reset scid bitfield
		regval |= (i << BTRAIN_SIMB_CTRL_SCID_SHIFT);// set scid
		ptr->SIMB_CTRL = iomemw32(btrain->is_be, regval);

		/* read subcycle length and increment */
		inc = iomemr32(btrain->is_be, ptr->BSIM_SCYC_INC);
		/* inc is a 16-bit signed value, convert into 32-bit signed if negative*/
		if(inc & 0x8000)
			inc = inc - 0x10000;
		length = iomemr32(btrain->is_be, ptr->BSIM_SCYC_LEN);
		fprintf(stderr, "\tsub cycle id: %d | length: %d (num of tx "
			"frames) | inc: %d\n", i, length, inc);
	}
	return 1;
}

enum btrain_cmd_id{
	BTRAIN_CMD_SET_TXDBG = CMD_USR,
	BTRAIN_CMD_SET_FTDBG,
	BTRAIN_CMD_GET_LASTTXFRAME,
	BTRAIN_CMD_GET_LASTRXFRAME,
	BTRAIN_CMD_SET_SINGLEFRAME,
	BTRAIN_CMD_SET_TXPER,
	BTRAIN_CMD_DATAVALID,
	BTRAIN_CMD_DATAVALID_OR,
	BTRAIN_CMD_SET_SIMSUBCYCLE,
	BTRAIN_CMD_SIMCYCLE,
	BTRAIN_CMD_LAST,
};

#define BTRAIN_CMD_NB BTRAIN_CMD_LAST - CMD_USR
struct cmd_desc btrain_cmd[BTRAIN_CMD_NB + 1] = {
	{ 1, BTRAIN_CMD_SET_TXDBG, "txdbg",
	  "Get/Set tx debugging of btrain",
	  "0/1/2/3", 1, get_set_tx_debug},
	{ 1, BTRAIN_CMD_SET_FTDBG, "ftdbg",
	  "Get/Set Debugging of forced frame type",
	  "0/1/2/3/4", 1, get_set_ft_debug},
	{ 1, BTRAIN_CMD_GET_LASTTXFRAME, "ltxfr",
	  "last TX frame", "", 0,
	  get_last_tx_frame},
	{ 1, BTRAIN_CMD_GET_LASTRXFRAME, "lrxfr",
	  "last RX frame", "", 0,
	  get_last_rx_frame},
	{ 1, BTRAIN_CMD_SET_SINGLEFRAME, "txsfr",
	  "Single WR BTrain frame transmission request", "", 0,
	  set_single_tx_frame},
	{ 1, BTRAIN_CMD_SET_TXPER, "txp",
 	  "Get/Set transmission period in us", "period", 0,
	  get_set_tx_period},
	{ 1, BTRAIN_CMD_DATAVALID, "dv",
	  "Get/Set RX output data valid setting",
	  "[-d delay -l length -p polarity]", 0, get_set_data_valid_cfg},
	{ 1, BTRAIN_CMD_DATAVALID_OR, "dvor",
	  "get/set overriding of default data valid settings configured"
	  " with WB (set using dv command)",
	  "[0/1]", 0, get_set_data_valid_or},
	  { 1, BTRAIN_CMD_SET_SIMSUBCYCLE, "simsc",
	  "Defines a sub cycle for Btrain simulation generator",
	  "[subcycleid length increment]", 3, set_sim_subcycle},
	{ 1, BTRAIN_CMD_SIMCYCLE, "simc",
	  "Get/Set Btrain simulation cycle",
	  "[cyclelength]", 0, get_set_sim_cycle},
	{0, },
};

static void print_version(void)
{
	fprintf(stderr, "Built in wrpc-sw repo ver:%s, by %s on %s %s\n",
		__GIT_VER__, __GIT_USR__, __TIME__, __DATE__);
}

static void btrain_help(char *prog)
{
	fprintf(stderr, "%s [options]\n", prog);
	fprintf(stderr, "%s\n", dev_mapping_help());
	print_version();
}

static void sig_hndl()
{
	// Signal occured: free resource and exit
	fprintf(stderr, "Handle signal: free resource and exit.\n");
	dev_unmap(btrain);
	exit(1);
}

static int verify_reg_version()
{
	volatile struct BTRAIN_WB *ptr =
		(volatile struct BTRAIN_WB *)btrain->base;
	uint32_t ver = 0;
	ver = iomemr32(btrain->is_be, ptr->VER);
	fprintf(stderr, "Wishbone register version: in FPGA = 0x%x |"
		" in SW = 0x%x\n", ver, WBGEN2_BTRAIN_VERSION);
	if(ver != WBGEN2_BTRAIN_VERSION)
		return -1;
	else
		return 0;
}

int main(int argc, char *argv[])
{
	int ret;
	struct mapping_args *map_args;

	map_args = dev_parse_mapping_args(argc, argv);
	if (!map_args) {
		btrain_help(argv[0]);
		return -1;
	}

	btrain = dev_map(map_args, sizeof(struct BTRAIN_WB));
	if (!btrain) {
		fprintf(stderr, "%s: btrain mmap() failed: %s\n", argv[0],
			strerror(errno));
		free(map_args);
		return -1;
	}

	ret = verify_reg_version();
	if (ret) {
		fprintf(stderr, "Register version in FPGA and SW does not match\n");
		dev_unmap(btrain);
		return -1;
	}
	
	ret = extest_register_user_cmd(btrain_cmd, BTRAIN_CMD_NB);
	if (ret) {
		dev_unmap(btrain);
		return -1;
	}

	/* execute command loop */
	ret = extest_run("btrain", sig_hndl);

	dev_unmap(btrain);
	return (ret) ? -1 : 0;
}
