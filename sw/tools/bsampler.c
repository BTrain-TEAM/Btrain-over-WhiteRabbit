/*
 * This work is part of the BTrain over White Rabbit project
 *
 * Copyright (C) 2018 CERN (www.cern.ch)
 * Author: Maciej Lipinski <maciej.lipinski@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <time.h>

#include <libdevmap.h>
#include <extest.h>

#include <bupdown_sampler.h>

static struct mapping_desc *sampler = NULL;

static int read_memory_size()
{
  volatile struct BUPDOWN_SAMPLER_WB *ptr =
    (volatile struct BUPDOWN_SAMPLER_WB *)sampler->base;
  uint32_t val = 0;
  val = (int)(iomemr32(sampler->is_be, ptr->MSR));
  fprintf(stderr, "Memory size: %d entries (%d samples, %f [ms])\n",
          val, (int)(val/3),((((float)val)*4)/3000));
  return val;
}

static int get_dummy(struct cmd_desc *cmdd, struct atom *atoms)
{
  volatile struct BUPDOWN_SAMPLER_WB *ptr =
    (volatile struct BUPDOWN_SAMPLER_WB *)sampler->base;
  uint32_t val;
  if (atoms == (struct atom *)VERBOSE_HELP) {
    printf("%s - It reads dummy value that should be 0xCAFE\n", cmdd->name);
    return 1;
  }

  val = BUPDOWN_SAMPLER_SCR_DUMMY_R(iomemr32(sampler->is_be, ptr->SCR));

  fprintf(stderr, "Dummy value: 0x%x\n", val);
  return 1;
}

static int set_acq_start(struct cmd_desc *cmdd, struct atom *atoms)
{
  volatile struct BUPDOWN_SAMPLER_WB *ptr =
    (volatile struct BUPDOWN_SAMPLER_WB *)sampler->base;

  if (atoms == (struct atom *)VERBOSE_HELP) {
    printf("%s - It starts acquisition\n",cmdd->name);
    return 1;
  }

  ptr->SCR |= iomemw32(sampler->is_be, BUPDOWN_SAMPLER_SCR_ACQ_START);
  fprintf(stderr, "Started acquisition\n");

  return 1;
}

static int set_acq_stop(struct cmd_desc *cmdd, struct atom *atoms)
{
  volatile struct BUPDOWN_SAMPLER_WB *ptr =
    (volatile struct BUPDOWN_SAMPLER_WB *)sampler->base;

  if (atoms == (struct atom *)VERBOSE_HELP) {
    printf("%s - It starts acquisition\n",cmdd->name);
    return 1;
  }

  ptr->SCR |= iomemw32(sampler->is_be, BUPDOWN_SAMPLER_SCR_ACQ_STOP);
  fprintf(stderr, "Stopped acquisition\n");

  return 1;
}

static int get_acq_stat(struct cmd_desc *cmdd, struct atom *atoms)
{
  volatile struct BUPDOWN_SAMPLER_WB *ptr =
    (volatile struct BUPDOWN_SAMPLER_WB *)sampler->base;
  uint32_t val;

  if (atoms == (struct atom *)VERBOSE_HELP) {
    printf("%s - It starts acquisition\n",cmdd->name);
    return 1;
  }

  val = BUPDOWN_SAMPLER_SCR_ACQ_STATUS_R(iomemr32(sampler->is_be, ptr->SCR));

  if(val == 0)      fprintf(stderr, "Acq status: none (%d)\n", val);
  else if(val == 1) fprintf(stderr, "Acq status: ongoing (%d)\n", val);
  else if(val == 1) fprintf(stderr, "Acq status: ongoing (%d)\n", val);
  else              fprintf(stderr, "Acq status: unknown (%d)\n", val);

  return 1;
}

static int get_set_mode(struct cmd_desc *cmdd, struct atom *atoms)
{
  volatile struct BUPDOWN_SAMPLER_WB *ptr =
    (volatile struct BUPDOWN_SAMPLER_WB *)sampler->base;
  uint32_t dbg, val;

  if (atoms == (struct atom *)VERBOSE_HELP) {
    printf("%s - Set acquisition (0: single | 1: circle buff"
    " | 2: star at C0)\n",cmdd->name);
    return 1;
  }
  ++atoms;
  if (atoms->type != Terminator){
    if (atoms->type != Numeric)
      return -TST_ERR_WRONG_ARG;
    dbg = atoms->val;
    val = iomemr32(sampler->is_be, ptr->SCR);
    val &= ~BUPDOWN_SAMPLER_SCR_ACQ_MODE_MASK;
    val |= BUPDOWN_SAMPLER_SCR_ACQ_MODE_W(dbg);
    ptr->SCR = iomemw32(sampler->is_be, val);
  }
  val = BUPDOWN_SAMPLER_SCR_ACQ_MODE_R(iomemr32(sampler->is_be, ptr->SCR));

  if(val == 0)      fprintf(stderr, "Acq mode: single (%d)\n", val);
  else if(val == 1) fprintf(stderr, "Acq mode: circular buff (%d)\n", val);
  else if(val == 2) fprintf(stderr, "Acq mode: start at C0 active (%d)\n", val);
  else              fprintf(stderr, "Acq mode: unknown (%d)\n", val);

  return 1;
}

static int get_data(struct cmd_desc *cmdd, struct atom *atoms)
{
  volatile struct BUPDOWN_SAMPLER_WB *ptr =
    (volatile struct BUPDOWN_SAMPLER_WB *)sampler->base;
  uint32_t val;
  int32_t B_wr, B_old, B_conv, C_old, C_wr;
  int i = 0;
  int last_word = 0, samples_num_req = 0, max_words = 0;

  if (atoms == (struct atom *)VERBOSE_HELP) {
    printf("%s - get data\n",cmdd->name);
    return 1;
  }
  ++atoms;
  if (atoms->type != Terminator){
    if (atoms->type != Numeric)
      return -TST_ERR_WRONG_ARG;
    samples_num_req = atoms->val;
  }

  i = 0;
  max_words = read_memory_size();

  if(samples_num_req != 0 && (3*samples_num_req) < max_words)
    last_word = 3*samples_num_req;
  else
    last_word = max_words;

  fprintf(stderr,"read samples: %d (max samples: %d | users req num: %d\n",
            (last_word/3), (max_words/3), samples_num_req);
  fprintf(stderr,"|-----------------------------------------------------------|\n");
  fprintf(stderr,"|    ID   |    B_wr   |   B_old   |    B_cnv  | C_old|  C_wr|\n");
  fprintf(stderr,"|-----------------------------------------------------------|\n");
  do {
    //W0:
    val   = iomemr32(sampler->is_be, ptr->MEM[i+0]);
    B_wr  = (int32_t)(val);
    //W1:
    val   = iomemr32(sampler->is_be, ptr->MEM[i+1]);
    B_old = (int32_t)(val & 0xFFFFFFFE);
    C_old = val & 0x00000001;
    //W2:
    val   = iomemr32(sampler->is_be, ptr->MEM[i+2]);
    B_conv= (int32_t)(val & 0xFFFFFFFE);
    C_wr  = val & 0x00000001;
    fprintf(stderr, "%10d, %10d, %10d, %10d, %5d, %5d\n",
            (i/3), B_wr, B_old, B_conv, C_old, C_wr);
    i=i+3;
    } while(i < last_word); // todo: read the max from WB

  fprintf(stderr,"|-----------------------------------------------------------|\n");
  fprintf(stderr,"|    ID   |    B_wr   |   B_old   |    B_cnv  | C_old|  C_wr|\n");
  return 1;
}

static int get_set_cmode(struct cmd_desc *cmdd, struct atom *atoms)
{
  volatile struct BUPDOWN_SAMPLER_WB *ptr =
    (volatile struct BUPDOWN_SAMPLER_WB *)sampler->base;
  uint32_t dbg, val;

  if (atoms == (struct atom *)VERBOSE_HELP) {
    printf("%s - Set C0 mode [0: C0_old / 1:C0_wr]\n",cmdd->name);
    return 1;
  }
  ++atoms;
  if (atoms->type != Terminator){
    if (atoms->type != Numeric)
      return -TST_ERR_WRONG_ARG;
    dbg = atoms->val;
    val = iomemr32(sampler->is_be, ptr->SCR);
    val &= ~BUPDOWN_SAMPLER_SCR_C0_MODE_MASK;
    val |= BUPDOWN_SAMPLER_SCR_C0_MODE_W(dbg);
    ptr->SCR = iomemw32(sampler->is_be, val);
  }

  val = BUPDOWN_SAMPLER_SCR_C0_MODE_R(iomemr32(sampler->is_be, ptr->SCR));

  if(val == 0)      fprintf(stderr, "C0 mode: old (%d)\n", val);
  else if(val == 1) fprintf(stderr, "C0 mode: wr (%d)\n", val);
  else              fprintf(stderr, "C0 mode: unknown (%d)\n", val);

  return 1;
}

static int set_creset(struct cmd_desc *cmdd, struct atom *atoms)
{
  volatile struct BUPDOWN_SAMPLER_WB *ptr =
    (volatile struct BUPDOWN_SAMPLER_WB *)sampler->base;

  if (atoms == (struct atom *)VERBOSE_HELP) {
    printf("%s - It starts acquisition\n",cmdd->name);
    return 1;
  }

  ptr->SCR |= iomemw32(sampler->is_be, BUPDOWN_SAMPLER_SCR_C0_RESET);
  fprintf(stderr, "Force C0 reset\n");

  return 1;
}

static int get_set_aperiod(struct cmd_desc *cmdd, struct atom *atoms)
{
  volatile struct BUPDOWN_SAMPLER_WB *ptr =
    (volatile struct BUPDOWN_SAMPLER_WB *)sampler->base;
  uint32_t dbg, val;

  if (atoms == (struct atom *)VERBOSE_HELP) {
    printf("%s - Set acquisition period \n",cmdd->name);
    return 1;
  }
  ++atoms;

  if (atoms->type != Terminator){
    if (atoms->type != Numeric)
      return -TST_ERR_WRONG_ARG;
    dbg = atoms->val;
    val = iomemr32(sampler->is_be, ptr->MCR);
    val &= ~BUPDOWN_SAMPLER_MCR_S_PERIOD_MASK;
    val |= BUPDOWN_SAMPLER_MCR_S_PERIOD_W(dbg);
    ptr->MCR = iomemw32(sampler->is_be, val);
  }
  val = BUPDOWN_SAMPLER_MCR_S_PERIOD_R(iomemr32(sampler->is_be, ptr->MCR));

  if(val == 0) fprintf(stderr, "Acq period:  1 sample (4 [us])\n");
  else         fprintf(stderr, "Acq period: %d sample (%d [us])\n",
                       val, (4*val));

  return 1;
}

static int get_set_dload(struct cmd_desc *cmdd, struct atom *atoms)
{
  volatile struct BUPDOWN_SAMPLER_WB *ptr =
    (volatile struct BUPDOWN_SAMPLER_WB *)sampler->base;
  uint32_t dbg, val;

  if (atoms == (struct atom *)VERBOSE_HELP) {
    printf("%s - Disable termination (load) \n",cmdd->name);
    return 1;
  }
  ++atoms;
  if (atoms->type != Terminator){
    if (atoms->type != Numeric)
      return -TST_ERR_WRONG_ARG;
    dbg = atoms->val;
    val = iomemr32(sampler->is_be, ptr->SCR);
    val &= ~BUPDOWN_SAMPLER_SCR_IN_LOAD_DISABLED;
    if(dbg)
      val |= BUPDOWN_SAMPLER_SCR_IN_LOAD_DISABLED;
    ptr->SCR = iomemw32(sampler->is_be, val);
  }
  val = (iomemr32(sampler->is_be, ptr->SCR));

  if(val & BUPDOWN_SAMPLER_SCR_IN_LOAD_DISABLED) 
    fprintf(stderr, "Termination disabled\n");
  else
    fprintf(stderr, "Termination enabled\n");

  return 1;
}

enum sampler_cmd_id{
  SAMPLER_CMD_DUMMY_READ = CMD_USR,
  SAMPLER_CMD_ACQ_START,
  SAMPLER_CMD_ACQ_STOP,
  SAMPLER_CMD_ACQ_STAT,
  SAMPLER_CMD_ACQ_MODE,
  SAMPLER_CMD_ACQ_READ,
  SAMPLER_CMD_C0_MODE,
  SAMPLER_CMD_C0_RES,
  SAMPLER_CMD_ACQ_PERIOD,
  SAMPLER_CMD_DIS_LOAD,
  SAMPLER_CMD_LAST,
};

#define SAMPLER_CMD_NB SAMPLER_CMD_LAST - CMD_USR
struct cmd_desc sampler_cmd[SAMPLER_CMD_NB + 1] = {
  { 1, SAMPLER_CMD_DUMMY_READ, "dummy",
    "read dummy value", "", 0, get_dummy},
  { 1, SAMPLER_CMD_ACQ_START, "start",
    "Start acquisition","", 0, set_acq_start},
  { 1, SAMPLER_CMD_ACQ_STOP, "stop",
    "Stop acquisition","", 0, set_acq_stop},
  { 1, SAMPLER_CMD_ACQ_STAT, "stat",
    "Acquisition status","", 0, get_acq_stat},
  { 1, SAMPLER_CMD_ACQ_MODE, "mode",
    "Get/Set acquisition mode (0: single | 1: circle buff | 2: star at C0)",
    "[0/1/2]", 0, get_set_mode},
  { 1, SAMPLER_CMD_ACQ_READ, "read",
    "Read acquired data", "[sample nunber]", 0, get_data},
  { 1, SAMPLER_CMD_C0_MODE, "cmode",
    "Get/Set C0 mode", "[0/1]", 0, get_set_cmode},
  { 1, SAMPLER_CMD_C0_RES, "creset",
    "Force C0 reset", "", 0, set_creset},
  { 1, SAMPLER_CMD_ACQ_PERIOD, "aperiod",
    "Set acquisition period", "", 0, get_set_aperiod},
  { 1, SAMPLER_CMD_DIS_LOAD, "dload",
    "Disable termination (0: enable load | 1: disable load )", "", 0, get_set_dload},
  {0, },
};

static void print_version(void)
{
  fprintf(stderr, "Built in wrpc-sw repo ver:%s, by %s on %s %s\n",
  __GIT_VER__, __GIT_USR__, __TIME__, __DATE__);
}

static void sampler_help(char *prog)
{
  fprintf(stderr, "%s [options]\n", prog);
  fprintf(stderr, "%s\n", dev_mapping_help());
  print_version();
}

static void sig_hndl()
{
  // Signal occured: free resource and exit
  fprintf(stderr, "Handle signal: free resource and exit.\n");
  dev_unmap(sampler);
  exit(1);
}

static int verify_reg_version()
{
  volatile struct BUPDOWN_SAMPLER_WB *ptr =
    (volatile struct BUPDOWN_SAMPLER_WB *)sampler->base;
  uint32_t ver = 0;
  ver = iomemr32(sampler->is_be, ptr->VER);
  fprintf(stderr, "Wishbone register version: in FPGA = 0x%x |"
    " in SW = 0x%x\n", ver, WBGEN2_BUPDOWN_SAMPLER_VERSION);
  if(ver != WBGEN2_BUPDOWN_SAMPLER_VERSION)
    return -1;
  else
    return 0;
}

int main(int argc, char *argv[])
{
  int ret;
  struct mapping_args *map_args;

  map_args = dev_parse_mapping_args(argc, argv);
  if (!map_args) {
    sampler_help(argv[0]);
    return -1;
  }

  sampler = dev_map(map_args, sizeof(struct BUPDOWN_SAMPLER_WB));
  if (!sampler) {
    fprintf(stderr, "%s: sampler mmap() failed: %s\n", argv[0],
      strerror(errno));
    free(map_args);
    return -1;
  }

  ret = verify_reg_version();
  if (ret) {
    fprintf(stderr, "Register version in FPGA and SW does not match\n");
      dev_unmap(sampler);
    return -1;
  }

  read_memory_size();

  ret = extest_register_user_cmd(sampler_cmd, SAMPLER_CMD_NB);
  if (ret) {
    dev_unmap(sampler);
    return -1;
  }

  /* execute command loop */
  ret = extest_run("sampler", sig_hndl);

  dev_unmap(sampler);  
  return (ret) ? -1 : 0;
}