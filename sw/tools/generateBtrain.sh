#!/bin/bash
#-----------------------------------------------------------------------------
# Title      : Btrain over White Rabbit
# Project    : Btrain
#-----------------------------------------------------------------------------
# File       : main.sv
# Author     : Maciej Lipinski, Michel Arruat
# Company    : CERN
# Created    : 2016-11-24
#-----------------------------------------------------------------------------
# This script uses debugging mode of the Btrain encoder/decoder to  produce
# stream of frames with Bvalue that resembles PS cycle.The
#
# cycle time = 19*factor+7 [samples]
# factor=26315 gives cycle of 499 992 frames, so close to 2s
#
# If you want to reproduce exactly PS rate, you neeed
# factor=26315
# period=FA #4us
#
# Other useful config
# factor=263
# period=61A8 # 400us
#-----------------------------------------------------------------------------


factor=26315
period=0x4 #[us]

prg_path=/usr/local/bin
prg_name=wr-btrain

# example PCI
args="-o 0x01200 -f /sys/bus/pci/devices/0000:01:00.0/resource0"

# example VME:
# args=" -o 0x01200 --cern-vmebridge -a 0x180000 -m 0x39"

bus=0
pci_res=""
echo -n "This script will execute calling the wr-btrain command "
echo  "with the following arguments: "
echo  " "
echo  "wr-btrain $args"
echo  " "
echo -n "Type 1 to continue, type 0 to stop (modify the argument"
echo -n "and rerun the script) "
read bus

if [ $bus -eq 1 ]; then
    echo "executing... "
else
    echo "exiting..."
    exit

fi

# build an array with all sub cycles definition: scid length increment
declare -a sc_list=("0 $((5*$factor)) 0"
                    "1 $((1*$factor)) 1"
                    "2 $((3*$factor)) 0"
                    "3 $((3*$factor)) 5"
                    "4 $((3*$factor)) 0"
                    "5 $((3*$factor)) -6"
                    "6 $((4*$factor)) 0"
                    "7 $((1*$factor)) 2")

# build the command sequence to pipe into wr-btrain
prg_cmd=""
# append simsc commands
for r in "${sc_list[@]}"; do
    prg_cmd+=" simsc $r, "
done
# append simc command
length=${#sc_list[@]}
prg_cmd+=" simc $length, "
# append txdbg command
prg_cmd+=" txdbg 0x2, "
# start transmission
prg_cmd+=" txp $period"

# final command
cmd="echo '($prg_cmd)' | sudo $prg_path/$prg_name $args"

# execute command
eval $cmd

