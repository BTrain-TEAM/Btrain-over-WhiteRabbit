\normalsize
\subsection{General}

In order to accommodate transition between the legacy BTrain system and
the new BTrain over White Rabbit system, a dedicated converter from the new
transmission of B value in WR-BTrain Frames to the legacy Bup/Bdown pulses
was developed.
This converter is supplemented with a sampler of the B values
provided by the two systems, and software to access this sampler. A dedicated
top entity and Xilinx project for synthesis are provided. The supplementary
HDL and SW are described in this Annex and include:
\begin{itemize*}
\item \textbf{hdl/rtl/BUpDownConverter :} a module that generates the legacy 
      Bup/Bdown pulses from the B value received in WR-BTrain frames.
\item \textbf{hdl/rtl/BUpDownSampler:} a module that can compare:
      \begin{itemize*}
      \item B value received in WR-BTrain frames,
      \item Bup/Bdown pulses generated from the received B value,
      \item input Bup/Bdown pulses from the legacy system.
      \end{itemize*}
\item \textbf{hdl/top/spec\_bupdown\_conv} a top entity that instantiates
      the reference design with BUpDownConverter and BUpDownSampler for
      \href{https://www.ohwr.org/project/spec/wikis/home}{SPEC} with
      \href{https://ohwr.org/project/fmc-dio-5chttla/wikis/home}{DIO-FMC module}.
\item \textbf{hdl/top/svec\_bupdown\_conv} a top entity that instantiates
      the reference design with BUpDownConverter and BUpDownSampler for
      \href{https://ohwr.org/project/svec/wikis/home}{SVEC} with
      \href{https://ohwr.org/project/fmc-dio-5chttla/wikis/home}{DIO-FMC module}
      in slot 1.
\item \textbf{hdl/syn/spec\_bupdown\_conv:} an ISE project with the spec\_bupdown\_conv.
\item \textbf{hdl/syn/svec\_bupdown\_conv:} an ISE project with the svec\_bupdown\_conv.
\item \textbf{hdl/testbench/spec\_bupdown\_conv-verification} a testbench to verify the operation of the converter and the sampler.
\item \textbf{sw/tools/bsampler.c:} a tool to operate the BUpDownSampler.
\end{itemize*}

In this Annex, section~\ref{sec:WR to Bup/Bdown Pulses Converter} describes 
the principles of operation of the WR to Bup/Bdown Pulses Converter, along
with its ports and parameters. Section~\ref{sec:Bvalue and Bpulses Sampler}
describes principle of operation of the Bvalue and Bpulses Sampler, along 
with its ports and parameters.

\subsection{WR to Bup/Bdown Pulses Converter}
\label{sec:WR to Bup/Bdown Pulses Converter}

This module takes as an input the Bvalue and C0 flag received in WR-BTrain frames and
provides as an output the Bup and Bdown pulses compatible with the legacy BTrain system,
as ilustrated in Figure~\ref{fig:bupdownConverter}.
\begin{figure}[ht!]
  \begin{center}
    \includegraphics[width=0.99\textwidth]{BupdownConverter.jpg}
    \caption{Simplified ilustration of the WR to Bup/Bdown Pulses Converter operation}
    \label{fig:bupdownConverter}
  \end{center}
\end{figure}
The output pulses can be either active-low or active-high, depending on the configuration 
of the BTrainFrameTransceiver (see \tts{g\_rx\_valid\_pol\_inv} in 
\ref{sec:WR BTrain HDL generics and ports}
and \textit{Status and control register} in \ref{subsec:wbgen:BTrain}). 

The module works by comparing an internal counter with the B value received in the WR-BTrain
frames. The internal counter is resetted each time a C0 flag is TRUE in the received WR-BTrain
frame. While the resolution of the Bvalue in the WR BTrain frame is 10nT, the resolution
of the legacy system is 1000 lower, i.e  0.1G. Thus, a pulse is generated when the difference between
the Bvalue received in the BTran frame and the internal counter is great than 1000, i.e. the
internal counter is incremented/decremented in 1000 steps. Each time the internal counter is incremented,
a Bup pulse is produced, each time the internal counter is decremented, a Bdown pulse
is produced. The width of the pulse can be determined by generic \tts{g\_PULSE\_WIDTH\_NS}
and it is 1000 ns by default. If the difference between the Bvalue received in the
WR-BTrain frame and the internal counter is huge (i.e. > 2000), a train of pulses is produced. 
The frequency of the pulses in the train is limited by \tts{g\_MIN\_PULSE\_GAP\_NS}.


 \newpage
\subsubsection{Parameters}
\label{sec:bupdown converter parameters}

\begin{hdlparamtable} 
  g\_CLOCK\_PERIOD\_NS & integer & 16 & indicates the period of the input clk (clk\_i) in nanoseconds
  \\
  \hline

  g\_PULSE\_WIDTH\_NS & integer & 1000 & indicates the width of the Bup/Bdown pulse in 
  nanoseconds (needs to be a multiple of the g\_CLOCK\_PERIOD\_NS)
  \\
  \hline
  g\_MIN\_PULSE\_GAP\_NS & integer & 1000 & indicates the minimum gap between consecutive
  pulses of the same type (Bup or Bdown) thus effectively determines the frequency of the
  pulses.
  \\
\end{hdlparamtable}
% \newpage
\subsubsection{Ports}

\begin{hdlporttable}
 \hdltablesection{Clocks and resets}\\
  \hline
  clk\_i    & in & 1 & clock input, it should be identical to the system 
  clock provided by the BTrainFrameTransceiver.\\
  \hline
  rst\_n\_i & in & 1 & reset input, active-low, synchronous to \tts{clk\_i}, it should be identical
  to the system reset provided by the BTrainFrameTransceiver.\\
  \hline
  \hdltablesection{Data interface with BTrainFrameTransceiver}\\
  \hline
  rx\_frame\_header\_i    & in & rec & \tts{t\_FrameHeader}: WR-BTrain frame header\\
  \hline
  rx\_bframe\_payloads\_i & in & rec & \tts{t\_BFramePayload}: B-type payload\\
  \hline
  rx\_frame\_valid\_p\_i & in & 1 & indicates the reception of a valid frame. Single-clock pulse expected, polarity depends on the rx\_cfg\_pol\_inv\_i\\
  \hline
  rx\_cfg\_pol\_inv\_i & in & 1 & indicates the polarity of the input rx\_frame\_valid\_p\_i and the polarity of the output bup\_o and bdown\_o (0:active low, 1: active high) \\
  \hline
  bup\_o & out & 1 & pulse indicating that the Bvalue increased by 0.1G (active low or high, depending on rx\_cfg\_pol\_inv\_i) \\
  \hline
  bdown\_o & out & 1 & pulse indicating that the Bvalue decreased by 0.1G (active low or high, depending on rx\_cfg\_pol\_inv\_i)\\

\end{hdlporttable}


\subsection{Bvalue and Bpulses Sampler}
\label{sec:Bvalue and Bpulses Sampler}

This module has been developed to compare the Bup/Bdown pulses generated from the
received WR-BTrain frames with the pulses distributed by the legacy system. When
a WR-BTrain frame is received, this module writes to the memory the following 
information:
\begin{enumerate*}
\item The Bvalue received in the previous WR-BTrain frame (it takes the value from the previous frame 
to give time for the Bup/Bdown pulses to be generated and the internal counters to be updated)
\item The value in the counter that counts the Bup/Bdown pulses coming from the \tts{bupdown\_converter}
\item The value in the counter that counts the external input Bup/Bdown pulses expected at inputs 0 and 1 of DIO.
\end{enumerate*}

The two counters of Bup/Bdown pulses coming from the \tts{bupdown\_converter} and DIO 
(legacy BTrain) can be resetted either by the \tts{CO} signal received in the WR-BTrain
frames, or an external CO signal expected at input 2 of the DIO.

The acquisition performed by this module can be configured via WB registars 
as follows (see \textit{Stat and ctrl register} 
and \textit{Memory status register} in \ref{subsec:wbgen:bupdown_sampler}):
\begin{itemize*}
\item \textbf{CO\_MODE:} Defines the source of C0 signal (WR frame or external input signal).
\item \textbf{ACQ\_MODE:} Defines how the acquisition is started/finished:
        \begin{itemize*}
        \item single acquisition triggered by the user via WB,
        \item circular buffer,
        \item single acquisition started by the CO signal.
        \end{itemize*}

\item \textbf{S\_PERIOD:} Defines the period of storing the data, it can be 
       stored each time WR-BTrain frame is received, or with less frequency.

\end{itemize*}

The acquisition can be configured and controlled by Wishbone Registers accessible
using dedicated software tool described in \ref{sec:Bvalue and Bpulses Sampler Software Tool}.

% \newpage
\subsubsection{Parameters}
\begin{hdlparamtable} 
  g\_slave\_mode & enum & CLASSIC &  external Wishbone Slave interface mode
  [PIPELINED/CLASSIC]
  \\
  \hline
  g\_slave\_granularity & enum & BYTE & granularity of address bus in external
  Wishbone Slave interface [BYTE/WORD]
  \\
\end{hdlparamtable}
\newpage
\subsubsection{Ports}
\begin{hdlporttable}
 \hdltablesection{Clocks and resets}\\
  \hline
  clk\_i    & in & 1 & clock input, it should be identical to the system 
  clock provided to the BTrainFrameTransceiver.\\
  \hline
  rst\_n\_i & in & 1 & reset input, active-low, synchronous to \tts{clk\_i}, it should be identical
  to the system reset provided to the BTrainFrameTransceiver.\\
  \hline
  \hdltablesection{Data interface with BTrainFrameTransceiver}\\
  \hline
  rx\_frame\_header\_i    & in & rec & \tts{t\_FrameHeader}: WR-BTrain frame header\\
  \hline
  rx\_bframe\_payloads\_i & in & rec & \tts{t\_BFramePayload}: B-type payload\\
  \hline
  rx\_frame\_valid\_p\_i & in & 1 & indicates the reception of a valid frame. Single-clock pulse expected, polarity depends on the rx\_cfg\_pol\_inv\_i\\
  \hline
  rx\_cfg\_pol\_inv\_i & in & 1 & indicates the polarity of the input rx\_frame\_valid\_p\_i and the polarity of the output bup\_o and bdown\_o (0:active low, 1: active high) \\
  \hline
  \hdltablesection{Interface with DIO mezzanine}\\
  \hline
  C0\_old\_a\_i    & in & 1 & C0 reset external, an asynchronous input pulse (active low or high, depending on rx\_cfg\_pol\_inv\_i) \\
  \hline
  bup\_old\_a\_i    & in & 1 & Bup, an external asynchronous input pulse (active low or high, depending on rx\_cfg\_pol\_inv\_i) \\
  \hline
  bdown\_old\_a\_i    & in & 1 & Bdown, an external asynchronous input pulse (active low or high, depending on rx\_cfg\_pol\_inv\_i) \\
  \hline
  input\_load\_disabled\_o & out & 1 & Configuration of DIO inputs regarding load/termination (0: termination enabled, 1: termination disabled) \\
  \hline
  \hdltablesection{Inputs from bupdown\_converter}\\
  \hline
  bup\_old\_a\_i    & in & 1 & Bup input pulse (can be asynchronous, active low or high, depending on rx\_cfg\_pol\_inv\_i) \\
  \hline
  bdown\_old\_a\_i    & in & 1 & Bdown input pulse (can be asynchronous, active low or high, depending on rx\_cfg\_pol\_inv\_i) \\
  \hline
  \hdltablesection{Wishbone interface}\\
  \hline
  wb\_slave\_i    & in & rec & \tts{t\_wishbone\_slave\_in}  \\
  \hline
  wb\_slave\_o    & out & rec & \tts{t\_wishbone\_slave\_out} \\
  \hline

\end{hdlporttable}

\subsection{Top Level and Synthesis Project for the WR to Bup/Bdown Pulses Converter and Sampler}
\label{sec:Top Level and Synthesis Project for the WR to Bup/Bdown Pulses Conveter and Sampler}

The BTrain-over-WhiteRabbit repository includes top level designs that
instantiate Bup/Bdown Pulses Converter and Sampler modules (described 
in \ref{sec:WR to Bup/Bdown Pulses Converter}
and \ref{sec:Bvalue and Bpulses Sampler}) for two boards:
\begin{enumerate*}
\item \tts{hdl/top/spec\_bupdown\_conv} for
      \href{https://www.ohwr.org/project/spec/wikis/home}{SPEC} with
      \href{https://ohwr.org/project/fmc-dio-5chttla/wikis/home}{DIO-FMC module}.
\item \tts{hdl/top/svec\_bupdown\_conv} for
      \href{https://ohwr.org/project/svec/wikis/home}{SVEC} with
      \href{https://ohwr.org/project/fmc-dio-5chttla/wikis/home}{DIO-FMC module}
      in slot 1.
\end{enumerate*}
Table~\ref{tab:BupDownDIO} describes inputs/outputs of the DIO-FMC module in both
designs.
\begin{table}[h!]
    \begin{tabular}{| l |  l | l |}    \hline
    \textbf{Lemo I/O} & \textbf{Dir}      & \textbf{Signal desciption} \\ \hline
    1 & input  & Debug: Bup pulse from the legacy BTrain (for comparison)  \\ \hline
    2 & input  & Debug: Bdown pulse from the legacy BTrain (for comparison)\\ \hline
    3 & input  & Debug: C0 to reset the legacy BTrain pulses counter  \\ \hline
    3 & output & \textbf{Bup pulse} generated from the received B value\\ \hline
    4 & output & \textbf{Bdown pulse} generated from the received B value\\ \hline
    \end{tabular}
\caption{Inputs/outputs of the DIO-FMC module in the BUpDown Converter designs for SPEC and SVEC.}
\label{tab:BupDownDIO}
\end{table}

In \tts{hdl/syn/spec\_bupdown\_conv}, an ISE project to synthesize this top level 
for SPEC-T45 can be generated using \tts{hdlmake}.
\begin{lstlisting}
$ cd <your_location>/BTrain-over-WhiteRabbit/hdl/syn/spec_bupdown_conv
$ hdlmake makefile
$ make
\end{lstlisting}

In \tts{hdl/syn/svec\_bupdown\_conv}, an ISE project to synthesize this top level 
for SVEC can be generated using \tts{hdlmake}.
\begin{lstlisting}
$ cd <your_location>/BTrain-over-WhiteRabbit/hdl/syn/svec_bupdown_conv
$ hdlmake makefile
$ make
\end{lstlisting}

Additionally, the reference design for SPEC board (in \tts{hdl/top/spec\_ref\_design})
instantiate the \textit{Bup/Bdown Pulses Converter} module. The Bup and Bdown 
pulses drive outputs 4 and 5 of the DIO FMC, respectively.


\subsection{Bvalue and Bpulses Sampler Software Tool}
\label{sec:Bvalue and Bpulses Sampler Software Tool}

The tool to control and configure acquisition of Bsampler is available in \tts{sw/tools}.
It is compiled and used as any other of the diagnostics and configuration host tools
described in \ref{sec:Diagnostics and configuration using host tools}, yet it is
not deployed in the CERN Front Ends, neither in the lab nor operationally.
For example, to build it for PCI-based node (see more in \ref{sec:Diagnostics and configuration using host tools}):
\begin{lstlisting}
$ cd <your_location>/BTrain-over-WhiteRabbit/sw/tools
$ make
\end{lstlisting}
Assuming that you have followed the instructions in section~\ref{sec:Programming FPGA with WRPC firmware} 
modifying it appropriately to have the \tts{spec\_bupdown\_conv} design 
(see \ref{sec:Top Level and Synthesis Project for the WR to Bup/Bdown Pulses Conveter and Sampler}) loaded in the FPGA of your SPEC/SVEC
board, you should use the \tts{bsampler} tool with the address \tts{0x20000}.
For example, for SPEC at address 01:00.0, you would call it as follows:
\begin{lstlisting}
$ sudo ./bsampler -o  0x20000 -f /sys/bus/pci/devices/0000:01:00.0/resource0
\end{lstlisting}
For SVEC at address 0x180000, you would call it as follows:
\begin{lstlisting}
$ sudo ./bsampler -o  0x20000 --cern-vmebridge -a 0x180000 -m 0x39
\end{lstlisting}

This tool is self-describing, the following options are available:
\begin{lstlisting}
cfc-774-cbt:sampler[00] > h
Valid COMMANDS:
Idx  Name    Params                 Description
# 1: q       [                 ] -> Quit test program
# 2: h       [ o c             ] -> Help on commands
# 3: a       [                 ] -> Atom list commands
# 4: his     [                 ] -> History
# 5: s       [ Seconds         ] -> Sleep seconds
# 6: ms      [ MilliSecs       ] -> Sleep milliseconds
# 7: sh      [ Unix Cmd        ] -> Shell command
# 8: dummy   [                 ] -> read dummy value
# 9: start   [                 ] -> Start acquisition
#10: stop    [                 ] -> Stop acquisition
#11: stat    [                 ] -> Acquisition status
#12: mode    [ [0/1/2]         ] -> Get/Set acquisition mode 
#                                   (0: single | 1: circle buff | 2: star at C0)
#13: read    [ [sample nunber] ] -> Read acquired data
#14: cmode   [ [0/1]           ] -> Get/Set C0 mode
#15: creset  [                 ] -> Force C0 reset
#16: aperiod [                 ] -> Set acquisition period
#17: dload   [                 ] -> Disable termination (0: enable load | 1: disable load )

Type "h name" to get complete command help
\end{lstlisting}

