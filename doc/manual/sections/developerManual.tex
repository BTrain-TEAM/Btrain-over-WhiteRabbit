% \subsection{Introduction}
% \label{Introduction}

Very likely, you are reading this Developer Manual because you are about to integrate
your application-specific design logic (i.e. VHDL module) with the BTrain-over-WhiteRabbit
IP cores that are common for all the BTrain users. After providing an overview of such
integrating in section~\ref{sec:Overview of BTrain-over-WhiteRabbit integration},
this manual explains in details the integration following our (BE-CO-HT) design flow, i.e.:
using hdlmake and submodules (see section~\ref{sec:Required Tools and Sources}). The
BTrain-over-WhiteRabbit repository provides synthesis-ready top level designs
for supported boards (SPEC, SVEC, VFC-HD, VXS, CUTE-WR-DP) and simulation of top-level SPEC
and CUTE-WR-DP designs. This includes reference designs for all supported boards, 
an Ultra-fixed latency reference design for SPEC 
(see Annex~\ref{sec:Ultra-fixed latency reference design}), as well as top level 
Bup/Bdown Pulses Converter and Sampler for SPEC and SVEC 
(see Annex~\ref{sec:WR to Bup/Bdown Pulses Conveter and Sampler}).

We explain how to simulate the SPEC reference design in section~\ref{sec:SimRefDesign}
and how to synthesize all the reference designs in section~\ref{sec:SynRefDesign}. Finally,
we explain how to start your own project based on the the BTrain-over-WhiteRabbit IP cores /
repository in section~\ref{sec:IntegrationIntoYourDesign}.

Importantly, as of v1.2 release, BTrain-over-WhiteRabbit supports (in a prototype form)
BTrain interface over FMC pins. This allows to decouple the application-specific
design logic in a different FPGA than the BTrain-over-WhiteRabbit IP cores, provided
that the two FPGAs are connected using FMC. If this method is used, the integration
will look differant than desceribed in the following sections. Please see 
Annex~\ref{sec:FMC BTrain Interfce (prototype)} for details.

\newpage
\subsection{Overview of BTrain-over-WhiteRabbit integration}
\label{sec:Overview of BTrain-over-WhiteRabbit integration}

If you are using one of the reference designs to integrate your application-specific
design logic in the same FPGA as the BTrain-over-WhiteRabbit cores\footnote{If you
are using the BTrain over FMC interface, see Annex~\ref{sec:FMC BTrain Interfce (prototype)}.}, 
we recommend that you take the top-level
entity, inject your application-specific logic (i.e. VHDL module) into the top level and
connect your design to the proper port of the BTrainFrameTransceiver module (see
Fig~\ref{fig:WR-BTrainNode} on page~\pageref{fig:WR-BTrainNode} for the overview of the
WR-BTrain Node design). All the relevant ports and generics are described in
Annex~\ref{sec:WR BTrain HDL generics and ports}. It is important to know that:
\begin{itemize*}
\item the input/output signals are organized in records, there is one record with the information
      contained in the common header and a record for each of the BTrain frame types.
\item for transmission:
      \begin{itemize*}\small
      \item your design should connect to the relevant input payload record (B, I or C)
            and to the common header record \texttt{tx\_FrameHeader\_i}. It is important that
            you provide the appropriate \texttt{frame\_type}. Depending on the
            \texttt{frame\_type}, the data from the respective input record is transmitted.
      \item the transmission can happen only when the \texttt{ready\_o} is HIGH
      \item the transmission can be triggered in two ways:
              \begin{enumerate*}
              \item single-clock strobe to the input signal \texttt{tx\_TransmitFrame\_p1\_i}
              \item configuration of periodic transmission using the generic
                    \texttt{g\_tx\_period\_value} or/and setting the WB register
                    \texttt{TX\_PERIOD} (see Annex~\ref{subsec:wbgen:BTrain})
              \end{enumerate*}
     \item details regarding transmission and its configuration can be found in
           Annex~\ref{sec:WR BTrain Configuration}.
      \end{itemize*}
\item for reception:
      \begin{itemize*}\small
      \item received valid data is available to your design in a relevant payload output
            record (B I or C) and the common header output record (\texttt{rx\_FrameHeader\_o})
            when the output signal \texttt{rx\_Frame\_valid\_pX\_o} is HIGH
      \item the information as to which record is filled in (i.e which type of the received frame)
            is available in the \texttt{frame\_type} field of the common header output record
            \texttt{rx\_FrameHeader\_o} and, for your convenience, in the
            output signal \texttt{rx\_Frame\_typeID\_o}
      \item polarity of the output signal \texttt{rx\_Frame\_valid\_pX\_o} can be configured
            using generic \texttt{g\_rx\_valid\_pol\_inv} or/and setting the WB register
            \texttt{SCR}
      \item the period for which the received data is held in the output registers, as well as
            a delay between reception of the BTrain frame and presenting the data (indicated
            by  \texttt{rx\_Frame\_valid\_pX\_o}) can be configured
            using generics (\texttt{g\_rx\_out\_data\_time\_valid} and \texttt{g\_rx\_out\_data\_time\_delay})
            or/and setting the WB register \texttt{RX\_OUT\_DATA\_TIME}, see Annex~\ref{subsec:wbgen:BTrain}
      \item details regarding transmission and its configuration can be found in
            Annex~\ref{sec:WR BTrain Configuration}.
      \end{itemize*}
\end{itemize*}
\noindent\textbf{Note:} There are two ways of configuring transmission and reception parameters:
generics and WB registers. By default, the values provided in generics are used. Such
configuration can be overridden using WB registers. To override the default configuration,
a proper value must be written to a relevant register and an "override bit" must be set.
See Annex~\ref{sec:WR BTrain Configuration} for details.

\newpage
\subsection{Required Tools and Sources}
\label{sec:Required Tools and Sources}
Before starting the development, it is important to download the sources and necessary software
tools.

First, please clone the WhiteRabbit BTrain git repository from the CERN Gitlab server as
described in section~\ref{sec:RepositoryOrganizationAndEssentialReferences}.
After that, please install \textit{hdlmake} version v3.3\footnote{Previously, we
recommended \textit{hdlmake} version 2.1 patched with the Altera Arria V FPGA support
(required for the VFC-HD board), for instructions how to install it, see 
\href{https://gitlab.cern.ch/BTrain-TEAM/Btrain-over-WhiteRabbit/-/wikis/documentation}{BTrain-over-WhiteRabbit Manual}
for release v1.1.} following the
\href{https://hdlmake.readthedocs.io/en/master/#installing-hdlmake}{Installing hdlmake}
documentation guidelines. On a Linux machine, this can be easily done using
\texttt{pip}. If \texttt{pip} is not yet installed on your machine, on Ubuntu 
it is installed as follows:
\begin{lstlisting}
$ sudo apt install python-pip
\end{lstlisting}
Then, the newest \textit{hdlmake} can be installed as follows
\begin{lstlisting}
$ pip install hdlmake
\end{lstlisting}
Alternatively, you can clone the \textit{hdlmake} repository and check out v3.3:
\begin{lstlisting}
$ git clone https://ohwr.org/project/hdl-make.git <your_location>/hdl-make
$ cd <your_location>/hdl-make
$ git checkout 3.3
\end{lstlisting}
Then, using your favorite editor, you should create an \textit{hdlmake} script in /usr/bin to be
able to call it from any directory. The script should have the following content:
\begin{lstlisting}
#!/usr/bin/env bash
PYTHONPATH=<path_to_hdlmake_sources>/hdl-make/ python2.7 -m hdlmake $@
\end{lstlisting}
Please, make your script executable:
\begin{lstlisting}
$ chmod a+x /usr/bin/hdlmake
\end{lstlisting}
Anyhow, you will need to install \texttt{pip} because it is needed by \texttt{hdlmake}.

Last but not least, if you want to simulate the reference design, you will need ModelSim, while for
HDL synthesis you will need a synthesis software from the vendor of FPGA that you use.  Depending on whether you
want to work on Xilinx (e.g. SPEC, SVEC, VXS, CUTE-WR-DP boards) or Altera/Intel (e.g. VFC-HD) FPGA, you should
install either Xilinx ISE or Quartus Prime software. 

The version of tools that were used to build and verify the top-level 
designs for each release are listed at the release-specific wiki page, e.g
see \href{https://gitlab.cern.ch/BTrain-TEAM/Btrain-over-WhiteRabbit/-/wikis/v1.2}{v1.2 release wiki page}.
Please note that:
\begin{enumerate}
\item To be able to synthesize for Arria V FPGA (which is used on the VFC-HD board) you need at
  least a license for the Quartus Prime Standard Edition with the support of Arria V family.
\item In order to run all the simulation, ModelSim version 10.2a or newer is required. This is due to a
  bug present in earlier versions of ModelSim, which are not able to properly detect SystemVerilog
  interfaces when they are declared by binding them inside VHDL files \footnote{Some simulations do no
  use this feature and thus can be run with an older version of ModelSim, see e.g. \href{https://gitlab.cern.ch/BTrain-TEAM/Btrain-over-WhiteRabbit/-/wikis/v1.2}{v1.2 release wiki page}}.
\end{enumerate}

\newpage
\subsection{Simulation of Reference Design}
\label{sec:SimRefDesign}

A number of SystemVerilog testbenches are provided for the simulation of the BTrain reference design.
The testbenches instantiate two interconnected Devices Under Test (DUT), as shown in
Figure~\ref{fig:simSetup}; each DUT is an instance of the SPEC BTrain design, with
the BTrain TX interface of DUT~A connected to the BTrain RX interface of DUT~B (and vice
versa) and a Bus Functional Model (BFM) attached to the ``host'' interface.
During simulation, the testbenches uses the host interface to configure and read the status
of the DUTs.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=0.99\textwidth]{simSetup.png}
    \caption{BTrain reference design graphical simulation example}
    \label{fig:simSetup}
  \end{center}
\end{figure}

The testbenches provide the following functionalities:
\begin{enumerate*}
\item \textbf{Simple Demo Testbench for BTrain reference design}\\ 
      It can be found in \texttt{spec\_ref\_design-demo}. It
      is a simple testbench in which the two BTrain
      reference designs are configured with exemplary settings. The BTrain information
      is exchanged between the two BTrain reference designs and no verification of
      the correct operation of the designs is performed. It is described in section~\ref{sec:Simple Demo Testbench}.

\item \textbf{Verification Testbench for BTrain reference design} \\
      It can be found in \texttt{spec\_ref\_design-verification}. It
      is an advanced testbench in which the two BTrain
      reference designs are configured with randomized settings. The BTrain information
      is exchanged between the two BTrain reference designs and the testbench verifies
      the correct operation of the designs. It is described in sectino~\ref{sec:Verification Testbench}.


\item \textbf{Verification Testbench for BTrain Bup/Down Converter} \\
      It can be found in \texttt{spec\_bupdown\_conv-verification}. It is an extension of
      the \textit{Verification Testbench for BTrain reference design}. It verifies
      operation of the \textit{WR to Bup/Bdown Pulses Conveter and Sampler} design
      described in Annex~\ref{sec:WR to Bup/Bdown Pulses Conveter and Sampler}. No
      detailed description of this testbench is provided.

\item \textbf{Simple Demo Testbench for ultra-fixed latency reference design}\\ 
      It can be found in \texttt{spec\_fixed\_latency\_ref\_design-demo}. It is a testbench 
      that is very similar to the \textit{Simple Demo Testbench for BTrain reference design}. 
      It tests operation of the \textit{ultra-fixed latency reference design} in
      \texttt{top/spec\_fixed\_latency\_ref\_design} described in 
      Annex~\ref{sec:Ultra-fixed latency reference design}. No
      detailed description of this testbench is provided.

\item \textbf{Simple Demo Testbench for BTrain over FMC interface} \\
      It can be found in \texttt{fmc\_interface-demo}. It is a testbench 
      that is very similar to the \textit{Simple Demo Testbench for BTrain reference design}.
      It tests operation of the \textit{BTrain over FMC interface (prototype)} described in 
      Annex~\ref{sec:FMC BTrain Interfce (prototype)}. It instantiates two interconnected SPEC top
      level designs that transmit and receive BTrain traffic: 
      \texttt{spec\_ref\_design} and \texttt{spec\_fmc\_interface\_test}. The \texttt{spec\_fmc\_interface\_test}
      design instantiates both parts of the \textit{BTrain over FMC interface} (FMC-side and Carrier-side)
      which are interconnected simulating the interface. No detailed description of this testbench is provided.


\end{enumerate*}

\newpage

\subsubsection{Testbench folder layout}
The following listing shows the layout of folders and files relevant to the testbenches.

% use ``tree --charset iso8859-1 hdl/testbench'' to regenerate
\begin{lstlisting}
hdl/testbench
|-- fmc_interface-demo
|   |-- main.sv
|   |-- Manifest.py
|   |-- run_ci.do
|   |-- run.do
|   `-- wave.do
|-- include
|   |-- common_macros.svh
|   |-- regs
|   |   |-- BTrain_wb.svh
|   |   `-- bupdown_sampler_wb.svh
|   |-- spec_ref_design_macros.svh
|   |-- tb_btrain_ref_design.sv
|   |-- tb_bupdown_conv.sv
|   |-- wb_btrain_ref_design.sv
|   `-- wb_bupdown_sampler.sv
|-- Makefile
|-- spec_bupdown_conv-verification
|   |-- main.sv
|   |-- Manifest.py
|   |-- run_ci.do
|   |-- run.do
|   |-- tb_environment_and_test_program.sv
|   |-- tb_frame_program.svh
|   `-- wave.do
|-- spec_fixed_latency_ref_design-demo
|   |-- main.sv
|   |-- Manifest.py
|   |-- run_ci.do
|   |-- run.do
|   `-- wave.do
|-- spec_ref_design-demo
|   |-- main.sv
|   |-- Manifest.py
|   |-- run_ci.do
|   |-- run.do
|   `-- wave.do
`-- spec_ref_design-verification
    |-- main.sv
    |-- Manifest.py
    |-- run_ci.do
    |-- run.do
    |-- tb_environment_and_test_program.sv
    `-- wave.do
\end{lstlisting}
The Makefile in the top-level of the testbench folder is used mainly for automatically running all testbenches in Continuous Integration (CI) mode.
The actual reference design testbenches are the \code{main.sv} files in the respective folders (e.g. \code{spec\_ref\_design-demo} and \code{spec\_ref\_design-verification}).

The verification testbench makes use of the \code{include/tb\_btrain\_ref\_design.sv} and \code{include/tb\_bupdown\_conv\_ref\_design.sv} files, which contain all the board-agnostic testbench code.
The \code{include/wb\_btrain\_ref\_design.sv} and \code{include/wb\_bupdown\_sample\_design.sv} contain a set of classes which provide for the verification testbench a structured way of accessing and configuring the BTrain modules (and the underlying ``WR Streamer'' module) through the host interface of each DUT.

For configuration of WB registers in the WR Streamers, BTrainFrameTransceiver and BUpDownSampler modules, both
testbenches use macros defined in SystemVerilog header files: \texttt{wr\_streamers\_wb.svh}, \texttt{BTrain\_wb.svh} and
\texttt{bupdown\_sampler\_wb.svh}.


Although currently there exists only a simulation for the SPEC reference design, it is foreseen to also introduce simulations for more supported boards (SVEC, VFC-HD, etc), and they will all make use of the code in \code{include/tb\_btrain\_ref\_design.sv} and  \code{include/tb\_bupdown\_conv\_ref\_design.sv}.

\newpage
\subsubsection{Building and Running with ModelSim}
To build the reference design simulations, it is enough to switch to the respective folder and run \code{hdlmake} (stable version 2.1 or later, ideally v3.3) followed by \code{make}.\\
For the simple demo testbench:
\begin{lstlisting}
  $ cd hdl/testbench/spec_ref_design-demo
\end{lstlisting}
For the verification testbench:
\begin{lstlisting}
  $ cd hdl/testbench/spec_ref_design-verification
\end{lstlisting}
Similarly for all the other testbenches. And then
\begin{lstlisting}
  $ hdlmake makefile
  $ make
\end{lstlisting}

Please make sure that \code{hdlmake} can find the path to your ModelSim installation (\code{vsim} should be somewhere in your path).

Once the build is complete, the simulation can be run either in graphical or command-line mode. For the graphical mode, it is enough to launch \code{vsim} and execute the \code{run.do} Tcl script from the ModelSim prompt.
\begin{lstlisting}
  $ vsim
  [ModelSim >] do run.do
\end{lstlisting}

For the command-line mode:
\begin{lstlisting}
  $ vsim -c -do "run_ci.do"
\end{lstlisting}

Pleas also note  that for the verification testbench both the \code{run.do} and \code{run\_ci.do} Tcl scripts make use of the \code{-sv\_seed random} parameter of \code{vsim} in order to properly randomize each simulation run. The actual seed value is reported at the start of the simulation by ModelSim and it can be reused (by editing the Tcl script and replacing the word ``random'' with the seed value) in order to repeat a particular run.


\newpage
\subsubsection{Simple Demo Testbench}
\label{sec:Simple Demo Testbench}

This testbench does not verify correct operation of the  SPEC reference design, yet it is 
perfect for learning how the design work and to debugguing.
During simulation, the testbench uses the host interface to configure and read the status
of the DUTs, correct operation of the DUTs can be verified by looking at the waveforms.

\subsubsubsection{Simulation Flow}
During execution, the simulation goes through the following list
of steps:
\begin{enumerate*}
\item Wait for the host interfaces to be ready
\item Read dummy value from WR Streamers in both DUTs (should be 0xDEADBEEF)
\item Read dummy value from BTrainFrameTransceiver in both DUTs (should be 0xCAFE)
\item Wait until Ethernet links on both DUTs are up and the the startup count finishes
      (approximately $370\mu s$, be patient)
\item Configure DUT B to have fix latency of $2.4\mu s$ (300 00 cycles of 125MHz clock)
\item Configure DUT A to produce simple triangle waveform of the transmitted values
\item Request DUT B to send single BTrain frame
\item Configure transmission and reception of both DUTs (it won't be
      effective until the last sub-step when it is enabled)
      \begin{enumerate*}
      \item Set length of the output signals to 1 and 5 cycles at DUT A and B respectively
      \item Set period of transmission to $2.88\mu s$ on both DUTs
      \item Set debugging mode 2 (simulated waveform) on DUT A and debugging mode 3 (dummy values) on DUT B
      \item Enable WB configuration on both DUTs -- the transmission starts now
      \end{enumerate*}
\item Change the type of frame transmitted: on DUT A to I frames, on DUT B to C frames.
\item Update characteristics of the output signals at DUT B:
      delay of 20 cycle and length of output signal of 20 cycles
\item Update characteristics of the output signals at DUT B: disable output signal
\item Update characteristics of the output signals at DUT B:
      delay of 10 cycle and continuous output signal
\item Update characteristics of the output signals at DUT B:
      delay of 0 cycle and length of output signal of 1 cycles
\item Update characteristics of the output signals at DUT B: inverted polarity
      of the valid signal
\item Update characteristics of the output signals at DUT B: normal polarity
      of the valid signal
\item Read WR Streamers' statistics of both DUTs: number of transmitted and received frames
\item Reset WR Streamers' statistics of both DUTs
\item Read timestamp of the reset at both DUTs
\item Read WR Streamers' statistics of both DUTs: number of transmitted and received frames
\item \textbf{The simulation breaks the link}
\item Read WR Streamers' statistics of both DUTs: number of transmitted and received frames
\end{enumerate*}

\subsubsubsection{Expected Results}
While going through the simulation flow, the testbench will log information, warnings and
errors on the console. The following shows an example output from a successful simulation.
Figure~\ref{fig:modelsimWave-simple-demo} shows an annotated grahical wave view of the
simulation.

\begin{lstlisting}[breaklines=true, basicstyle=\ttfamily\scriptsize]
# Gennum ready A=1 and B=1
# Read dummy value from streamers @ A : 0x00000000deadbeef (should be 0xDEADBEEF)
# Read dummy value from streamers @ B : 0x00000000deadbeef (should be 0xDEADBEEF)
# DUMMY nubmer from spec A: 0x000000000000cafe
# DUMMY nubmer from spec B: 0x000000000000cafe
#
#
# ====================================================
# ===============wait for link up=====================
# ====================================================
# = be very patient, it might take some time, i.e.   =
# = few minutes of your time and 103us on simulation =
# ====================================================
#
#
# End 200us wait, now just wait for links up (yet another 100us)
# both up now
# Spec A - max number of sub-cycles in SimGen: 8
# Enforce sending I   frames on spec A and set debug mode to 2
# Enforce sending C   frames on spec B and set debug mode to 3
# Number of transmitted frames at specA: 43
# Number of received at specA: 42
# Number of transmitted frames at specB: 44
# Number of received at specB: 43
# Timestamp of Statistics reset for spec A: TAI= 0, CYC= 32901
# Timestamp of Statistics reset for spec A: TAI= 0, CYC= 32909
# Number of transmitted frames at specA: 8
# Number of received at specA: 8
# Number of transmitted frames at specB: 8
# Number of received at specB: 9
# Breaking link between spec A and spec B for some time to test loss cnt
# Breaking link between spec B and spec A for some time to test loss cnt
# Fixing link between spec A and spec B
# Fixing link between spec B and spec A
# Number of lost frames at specB: 0
\end{lstlisting}

\begin{figure}
  \begin{center}
    \includegraphics[height=0.58\textheight,angle=90]{modelsimWave-simple-demo.jpg}
    \caption{BTrain reference design graphical simulation example}
    \label{fig:modelsimWave-simple-demo}
  \end{center}
\end{figure}


\newpage
\subsubsection{Verification Testbench}
\label{sec:Verification Testbench}

This testbench verifies correct operation of the SPEC reference design.
During simulation, the testbench uses the host interface to configure and read the status of the DUTs, while two randomized BTrain frame generators are used to create bidirectional traffic between the DUTs. The testbench continuously monitors and compares the transmitted and received frames on both sides.


\subsubsubsection{Simulation Flow}
During execution, the BTrain reference design simulation goes through the following list of steps:
\begin{enumerate}
\item reset both DUTs
\item enable physical link
\item wait 5us
\item\label{itm:simloop} Detect and configure wishbone peripherals over host interface, and reset WR streamer counters
\item Randomize the configuration, mainly the number of BTrain frames to send in each direction and the transmission rate. Number of frames is constrained between 100 and 250, while the rate can be one of 250kHz, 500kHz, or a random value in between those two.
\item Wait for BTrain transmitter to be ready (this can take up to 350us of simulation time after reset)
\item Start bidirectional transmission of frames. The type of each frame is randomly chosen, using the following probability distribution: 60\% B, 20\% I, 15\% C and 5\% unknown type (random value, not B, I or C).
\item Monitor both BTrain receivers and record received frames. Each received frame is submitted for matching with those that were sent. If a mismatch is detected the simulation reports a fatal error and stops.
\item When both sides have completed sending and receiving frames, the internal counters are compared with the frame counters stored in the underlying WR streamer modules. If the counters disagree, the simulation reports a fatal error and stops.
\item The flow is repeated from step~\ref{itm:simloop} two more times, using a new random configuration. The transmission rate is in fact a ``random cyclic'' variable, which means that after three consecutive runs all three possible transmission rates will be used (but in random order).
\end{enumerate}

\newpage
\subsubsubsection{Expected Results}

While going through the simulation flow, the testbench will log information, warnings and errors on the console. The following shows an example output from a successful simulation, looping through the three simulation runs. Notice the reporting of the random seed value at the beginning of the simulation.

\begin{lstlisting}[breaklines=true, basicstyle=\ttfamily\scriptsize]
# Sv_Seed = 1916800663
#
# [   5.000us] START sim run #1
#
# [  11.851us] A2B/Mgt: init OK
# [  11.851us] B2A/Mgt: init OK
# [  13.901us] A2B/Cfg: tx_period = 250, nFrames = 197, mbox_size = 6, verbose = 0
# [  13.901us] A2B/Drv: Wait for TX ready (requires 350us after reset)
# [  13.901us] B2A/Cfg: tx_period = 125, nFrames = 237, mbox_size = 9, verbose = 0
# [  13.901us] B2A/Drv: Wait for TX ready (requires 350us after reset)
# [ 350.500us] A2B/Drv: TX ready
# [ 350.500us] B2A/Drv: TX ready
# [ 354.972us] B2A/Drv: Start TX
# [ 357.478us] A2B/Drv: Start TX
# [ 829.096us] B2A/Scr: 237 total frames: 227 matched ( 135 B, 54 I, 38 P), 10 unknown (skipped)
# [1143.908us] A2B/Scr: 197 total frames: 191 matched ( 113 B, 43 I, 35 P), 6 unknown (skipped)
# [1152.271us] A2B/Mgt: start: 0:0, end: 0:124211, sent frames: 191, rcvd frames: 227, lost frames: 0, lost blocks: 0, min latency: 186, max latency: 188, acc latency: 42500, acc latency cnt: 227, acc latency overflow: 0
# [1152.271us] B2A/Mgt: start: 0:0, end: 0:124211, sent frames: 227, rcvd frames: 191, lost frames: 0, lost blocks: 0, min latency: 186, max latency: 188, acc latency: 35718, acc latency cnt: 191, acc latency overflow: 0
#
# [1152.271us] END sim run #1
#
#
# [1152.271us] START sim run #2
#
# [1159.211us] A2B/Mgt: init OK
# [1159.211us] B2A/Mgt: init OK
# [1161.261us] A2B/Cfg: tx_period = 229, nFrames = 150, mbox_size = 6, verbose = 0
# [1161.261us] A2B/Drv: Wait for TX ready (requires 350us after reset)
# [1161.261us] A2B/Drv: TX ready
# [1161.261us] B2A/Cfg: tx_period = 250, nFrames = 186, mbox_size = 1, verbose = 0
# [1161.261us] B2A/Drv: Wait for TX ready (requires 350us after reset)
# [1161.261us] B2A/Drv: TX ready
# [1164.499us] A2B/Drv: Start TX
# [1168.919us] B2A/Drv: Start TX
# [1712.552us] A2B/Scr: 150 total frames: 146 matched ( 85 B, 36 I, 25 P), 4 unknown (skipped)
# [1911.148us] B2A/Scr: 186 total frames: 177 matched ( 103 B, 42 I, 32 P), 9 unknown (skipped)
# [1919.551us] A2B/Mgt: start: 0:126125, end: 0:220117, sent frames: 146, rcvd frames: 177, lost frames: 0, lost blocks: 0, min latency: 186, max latency: 188, acc latency: 33098, acc latency cnt: 177, acc latency overflow: 0
# [1919.551us] B2A/Mgt: start: 0:126125, end: 0:220117, sent frames: 177, rcvd frames: 146, lost frames: 0, lost blocks: 0, min latency: 186, max latency: 188, acc latency: 27306, acc latency cnt: 146, acc latency overflow: 0
#
# [1919.551us] END sim run #2
#
#
# [1919.551us] START sim run #3
#
# [1926.491us] A2B/Mgt: init OK
# [1926.491us] B2A/Mgt: init OK
# [1928.541us] A2B/Cfg: tx_period = 125, nFrames = 132, mbox_size = 9, verbose = 0
# [1928.541us] A2B/Drv: Wait for TX ready (requires 350us after reset)
# [1928.541us] A2B/Drv: TX ready
# [1928.541us] B2A/Cfg: tx_period = 244, nFrames = 232, mbox_size = 12, verbose = 0
# [1928.541us] B2A/Drv: Wait for TX ready (requires 350us after reset)
# [1928.541us] B2A/Drv: TX ready
# [1929.073us] A2B/Drv: Start TX
# [1932.320us] B2A/Drv: Start TX
# [2193.192us] A2B/Scr: 132 total frames: 128 matched ( 72 B, 33 I, 23 P), 4 unknown (skipped)
# [2836.264us] B2A/Scr: 232 total frames: 222 matched ( 135 B, 52 I, 35 P), 10 unknown (skipped)
# [2844.671us] A2B/Mgt: start: 0:222035, end: 0:335757, sent frames: 128, rcvd frames: 222, lost frames: 0, lost blocks: 0, min latency: 186, max latency: 188, acc latency: 41516, acc latency cnt: 222, acc latency overflow: 0
# [2844.671us] B2A/Mgt: start: 0:222035, end: 0:335757, sent frames: 222, rcvd frames: 128, lost frames: 0, lost blocks: 0, min latency: 186, max latency: 188, acc latency: 23954, acc latency cnt: 128, acc latency overflow: 0
#
# [2844.671us] END sim run #3
\end{lstlisting}

\begin{figure}
  \begin{center}
    \includegraphics[height=0.68\textheight,angle=90]{modelsimWave.pdf}
    \caption{BTrain reference design graphical simulation example}
    \label{fig:modelsimWave}
  \end{center}
\end{figure}

Figure~\ref{fig:modelsimWave} shows an annotated grahical wave view of the above simulation.

Each console message is prefixed with a simulation time-tag, followed by an identifier of the transmission direction (A2B or B2A) and testbench ``subsystem'' (eg. Mgt, Cfg, Drv).

For each run, the randomized configuration (per transmission direction) will be printed. This shows the number of frames to send, the transmission period (in microseconds) and the mailbox size (the number of outstanding frames to send that can wait at any given time). The ``verbose'' setting is also displayed, although this is not a random value. By default, ``verbose'' is disabled, to enable it, modify the top-level \code{main.sv}, and set the last argument of the call to the \code{test\_btrain\_ref\_design} program to '1', as indicated in the source code comments.

For each run, when transmission in one direction is complete, the number of frames received and matched will be printed. At the end of the run, the WR streamer statistics will be printed as well.

\newpage
\subsection{Synthesis of Top Level Designs}
\label{sec:SynRefDesign}

The following types of top level designs are provided in \texttt{hdl/syn}
folder:
\begin{itemize*}
  \item \textbf{Reference design} - BTrain reference designs provided for all 
     WR-BTrain node boards: SPEC, SVEC, VFC-HD and CUTE-WR-DP. They provide
     a starting point for design of application-specific WR-BTrain nodes. Most users 
     will be intersted in this design. 
  \item \textbf{Ultra-fixed latency reference design} - BTrain reference
     design for SPEC that has configuration optimzied for fixed-latency. 
     It is of interest
     for users that require fixed-latency with accuracy below 1ns.
  \item \textbf{BUpDown Converter and Sampler} - Top level design that
     instantiates the \texttt{BUpDownConverter} module which is a 
     converter from WR-BTrain to the legacy BTrain, described in 
     Annex~\ref{sec:WR to Bup/Bdown Pulses Conveter and Sampler}.
     It is provided for SPEC and SVEC and is an end-design that will be used in operation.
\end{itemize*}
For the SPEC reference design, two 
     variants are available, one for the Spartan-6
     ``xc6slx45t'' FPGA, and another one for the larger Spartan-6 ``xc6slx100t'' FPGA.

Thanks to the \textit{hdlmake} tool, the synthesis process for the top level designs does not differ
between Xilinx and Altera/Intel based boards. The tool creates a synthesis Makefile as well as
ISE/Quartus project files based on a set of Manifest.py files that you will find in your cloned copy
of the repository.

The subdirectory you should enter to run the synthesis depends on the hardware platform you use
and the type of reference design you want to synthesize:
\begin{itemize*}
  \item \textbf{Reference design:}
    \begin{itemize*}
      \item \textbf{SPEC-45T}: \texttt{hdl/syn/spec\_ref\_design/spartan6\_45T}
      \item \textbf{SPEC-100T}: \texttt{hdl/syn/spec\_ref\_design/spartan6\_100T}
      \item \textbf{SVEC}: \texttt{hdl/syn/svec\_ref\_design}
      \item \textbf{VFC-HD}: \texttt{hdl/syn/vfchd\_ref\_design}
      \item \textbf{VXS}: \texttt{hdl/syn/vxs\_ref\_design}
      \item \textbf{UTE-WR-DP}: \texttt{hdl/syn/cute\_ref\_design}
    \end{itemize*}
  \item \textbf{Ultra-fixed latency reference design:}
    \begin{itemize*}
      \item \textbf{SPEC-45T}: \texttt{hdl/syn/spec\_fixed\_latency\_ref\_design}
    \end{itemize*}
  \item \textbf{BUpDown Converter and Sampler:} 
    \begin{itemize*}
      \item \textbf{SPEC-45T}: \texttt{hdl/syn/spec\_bupdown\_conv}
      \item \textbf{SVEC}: \texttt{hdl/syn/svec\_bupdown\_conv}
    \end{itemize*}
\end{itemize*}

After selecting a proper location from the list above, please call \textit{hdlmake} with
\textit{makefile} as an arguments to create the Makefile and project file, followed by \textit{make} to perform the synthesis
\begin{lstlisting}
  $ hdlmake makefile
  $ make
\end{lstlisting}

This takes (depending on your computer) about 10-20 minutes and should generate bitstream files in
various formats depending on your selected reference hardware and top level design:
\begin{itemize*}
  \item \textbf{Reference design:}
  \begin{itemize*}
    \item \textbf{SPEC}: \texttt{spec\_btrain\_ref\_top.bin}, \texttt{spec\_btrain\_ref\_top.bit}
    \item \textbf{SVEC}: \texttt{svec\_btrain\_ref\_top.bin}, \texttt{svec\_btrain\_ref\_top.bit}
    \item \textbf{VFC-HD}: \texttt{vfchd\_btrain\_ref.sof}
    \item \textbf{VXS}: \texttt{vxs\_btrain\_ref\_top.bin}, \texttt{vxs\_btrain\_ref\_top.bit}
    \item \textbf{CUTE-WR-DP}:\texttt{cute\_btrain\_ref\_top.bin}, \texttt{cute\_btrain\_ref\_top.bit}
  \end{itemize*}
  \item \textbf{Ultra-fixed latency reference design:}
  \begin{itemize*}
    \item \textbf{SPEC}: \texttt{spec\_fixed\_latency\_ref\_top.bin}, \texttt{spec\_fixed\_latency\_ref\_top.bit}
  \end{itemize*}
  \item \textbf{BUpDown Converter and Sampler:} 
    \begin{itemize*}
      \item \textbf{SPEC}: \texttt{spec\_btrain\_ref\_top.bin}, \texttt{spec\_btrain\_ref\_top.bit}
      \item \textbf{SVEC}: \texttt{svec\_btrain\_ref\_top.bin}, \texttt{svec\_btrain\_ref\_top.bit}
    \end{itemize*}
\end{itemize*}
Now, you are ready to load your design following instructions section~\ref{sec:Programming FPGA with WRPC firmware}.

\subsection{Integration into Your Design on a single FPGA}
\label{sec:IntegrationIntoYourDesign}

This section assumes that you want to integrate your application-specific design
logic (i.e. your code) and the BTrain-over-WhiteRabbit HDL in the same FPGA (an 
alternative solution is to keep your design logic and the BTrain-over-WhiteRabbit 
cores in two different FPGAs inter-connected using FMC interface, see 
Annex~\ref{sec:FMC BTrain Interfce (prototype)}).
Provided that you use one of the supported boards, integrating BTrain-over-WhiteRabbit HDL into your
design can be as simple as including the Git repository into your project, copying the respective
reference design files, renaming them and changing a couple of paths.

If your board is not supported, then you can still follow these guidelines, but it will require more
work to integrate BTrain-over-WhiteRabbit HDL into your design. Existing reference designs can be
used as a starting point, but it will be necessary to first integrate the WR PTP core itself. A good
place to start is the ``Instantiating WRPC in your own HDL design''
\href{https://www.ohwr.org/project/wr-cores/wikis/Wrpc-release-v42#documentation}{WR PTP core user manual}. As
explained in there, if your board is similar to a supported one, you might be able to reuse
significant parts of our templates.

Assuming that you have a supported board, the following steps should get you started:

\begin{enumerate}

\item Start a new Git repository and clone it

  All the next steps assume that you will create a new Git repository to host your project. A good
  place for that is \href{https://gitlab.cern.ch/}{CERN Gitlab} (CERN-only). If you don't know how to start a
  new repository, you can have a look at the relevant page in the online help:

  \url{https://gitlab.cern.ch/help/gitlab-basics/create-project.md}

  Following the information on creating the Git repository, you need to to clone it as explained here:

  \url{https://gitlab.cern.ch/help/gitlab-basics/command-line-commands.md}

\item Import the BTrain-over-WhiteRabbit HDL as a submodule and initialize it

  Make a new folder in your cloned Git repository and add the BTrain-over-WhiteRabbit Git repository
  as a submodule. When this is done, enter the submodule folder and initialize its own dependencies
  (also Git submodules).

  \begin{lstlisting}
    $ mkdir rtl
    $ git submodule add \
      ssh://git@gitlab.cern.ch:7999/BTrain-TEAM/Btrain-over-WhiteRabbit.git rtl/
    $ cd rtl/Btrain-over-WhiteRabbit
    $ git submodule update --init
  \end{lstlisting}

\item Make a copy of a reference design

  Pick the reference design for your supported board (or the one that is the closest match) and copy
  it. You need to copy two folders, ``top'' and ``syn''. As an example, if your board is a SVEC:

  \begin{lstlisting}
    $ mkdir top syn
    $ cp -r rtl/Btrain-over-WhiteRabbit/hdl/top/svec_ref_design/. top
    $ cp -r rtl/Btrain-over-WhiteRabbit/hdl/syn/svec_ref_design/. syn
  \end{lstlisting}

\item Rename files and update paths

  The last step is to rename the top-level files that you just copied (to something that fits your
  application) and to update the various paths to match your folder structure and file names.

  The exact details of this step depend on your project. An example of the required modifications,
  taken from a real project which is based on the VFC-HD BTrain reference design can be found in the
  project wiki.

  \href{https://gitlab.cern.ch/BTrain-TEAM/Btrain-over-WhiteRabbit/uploads/30b088f0e7d18b7fc583dcdb60802a44/BTrain-over-WhiteRabbit_integration-example.diff}{\texttt{https://gitlab.cern.ch/BTrain-TEAM/Btrain-over-WhiteRabbit/uploads/\\30b088f0e7d18b7fc583dcdb60802a44/BTrain-over-WhiteRabbit\_\\integration-example.diff}}

  In the above example, ``vfchd\_btrain\_ref'' is renamed to ``vfchd\_btrain\_dcct''. The rest of
  the folders follow the structure described in the previous steps.

  When everything is in place, try to synthesize your project by going into the ``syn'' folder and following the instructions outlined in Section~\ref{sec:SynRefDesign}.

\end{enumerate}
