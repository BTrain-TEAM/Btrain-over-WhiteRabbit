This manual is intended for users of BTrain-over-WhiteRabbit. In most cases, 
such users integrate their own HDL (Hardware Description Language) designs with
the BTrain-over-WhiteRabbit HDL to either use the information received over the White Rabbit network, or to send information over
the same network. In other cases, some users operate existing designs that have already 
integrated BTrain-over-WhiteRabbit, e.g. software developers writing FESA classes. 
This manual describes:
\begin{itemize*}
\item What BTrain-over-WhiteRabbit is (section~\ref{sec:BTrain-over-WhiteRabbitOverview}), 
\item What a WR-BTrain node is (section~\ref{sec:WR-BTrainNode})
\item How a WR-BTrain network works and what should be its configuration (section~\ref{sec:WR-BTrainNetwork})
\item How the BTrain-over-WhiteRabbit repository is organized and where to find information
      essential to the reader (section~\ref{sec:RepositoryOrganizationAndEssentialReferences})
\item Input/output signals and LED meaning of the top level reference designs provided
      in the BTrain-over-WhiteRabbit project
      (sectin~\ref{sec:Output and Input Auxiliary Digital Signals of Top Level Designs})
\item A getting-started setup (section~\ref{sec:GettingStarted}),
\item How to program the FPGA using provided binaries with reference designs 
      (section~\ref{sec:Programming FPGA with WRPC firmware})
\item How to configure and diagnose the BTrain-over-WhiteRabbit IP Cores using provided 
      binaries with reference designs 
      (section~\ref{sec:Diagnostics and configuration using host tools} and 
       section~\ref{sec:Diagnostics using SNMP})
\item Principles of integrating your logic with the BTrain-over-WhiteRabbit IP core
      (section~\ref{sec:Overview of BTrain-over-WhiteRabbit integration})
\item What tools and sources you will need for the integration 
      (section~\ref{sec:Required Tools and Sources})
\item How to simulate and synthesize the reference design which is the starting point
      for the integration (section~\ref{sec:SimRefDesign} and section~\ref{sec:SynRefDesign})
\item How to integrate your design with the BTrain-over-WhiteRabbit 
      (section~\ref{sec:IntegrationIntoYourDesign}).
\end{itemize*}
Additionally, a number of Annexes is provided with supplementary and detailed
information about the operation of particular VHDL modules, top level designs,
software, and BTrain over FMC interface. The Wishbone register maps relevant to
the project are included in Anenx~\ref{sec:WB Memory Maps}.

\newpage

\subsection{BTrain-over-WhiteRabbit Overview}
\label{sec:BTrain-over-WhiteRabbitOverview}

At CERN, BTrain is a system that distributes the value of the magnetic field in all CERN
accelerators except for the LHC. It is
now being upgraded to use \href{https://www.ohwr.org/project/white-rabbit/wikis/home}{White Rabbit (WR)}.
The original BTrain system uses coaxial cables to distribute pulses which indicate increase
or decrease of magnetic filed, i.e. up and down pulses. These pulses are distributed
from the reference magnet to a number of client-applications (see left part of 
Figure~\ref{fig:BTrainUpgrade}). This pulse-based
BTrain system is referred to in this document as \textit{old-BTrain}. The old-BTrain is now
being upgraded to use WR network for distribution of the absolute value of the magnetic field
(i.e. B-value) and additional information (e.g. simulated version of B-value, I-value of 
current). This new WR-based BTrain system is referred
to in this document as \textit{WR-BTrain} (see right part of Figure~\ref{fig:BTrainUpgrade}).

\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=1.0\textwidth]{BTrainUpgrade.jpg}
    \caption{Upgrade of BTrain system}
    \label{fig:BTrainUpgrade}
  \end{center}
\end{figure}

The WR-BTrain is composed of a generic WR Switch and dedicated WR-BTrain nodes. These 
WR-BTrain nodes include application-specific \textit{user logic} that requires transmission 
and/or reception of
BTrain-related information over the WR network, see Figure~\ref{fig:BTrain-over-WhiteRabbit}.
BTrain-over-WhiteRabbit is a set of 
VHDL IP-cores that allow users to send this WR-BTrain-related information without 
having to know implementation details. The transmitted information is:
\begin{itemize*}
\item B-related: different values related to the magnetic field
\item I-related: value of the main current
\item C-related: values of current \& voltage from power converters or the main current
\end{itemize*}

The BTrainFrameTransceiver IP Core included in the BTrain-over-WhiteRabbit provides
the \textit{user logic} with an interface for exchange of BTrain-related information.
This interface consists of VHDL 
records\footnote{As of v1.2 release, BTrain interface is also available over FMC, see in Annex~\ref{sec:FMC BTrain Interfce (prototype)}}(similar to structures in C) that the \textit{user logic}
fills when sending and reads when receiving BTrain-related information. The BTrainFrameTransceiver IP Core
takes care of encapsulating (packing/unpacking) this BTrain-specific records into 
\href{https://gitlab.cern.ch/BTrain-TEAM/Btrain-over-WhiteRabbit/wikis/wr-btrain-frame-format}{BTrain frames} 
that are sent over White Rabbit Network using generic data transmission method, the
\href{https://www.ohwr.org/projects/wr-cores/wiki/wr-streamers}{WR Streamers}.
 
\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=0.9\textwidth]{BTrain-over-WhiteRabbit.jpg}
    \caption{WR-BTrain network.}
    \label{fig:BTrain-over-WhiteRabbit}
  \end{center}
\end{figure}

When the \textit{user logic} wishes to send BTrain-related information (e.g. control bits, 
measured B-value, simulated B-value), this information is encapsulated 
by the BTrainFrameTransceiver IP Core into a BTrain frame  according to the 
\href{https://gitlab.cern.ch/BTrain-TEAM/Btrain-over-WhiteRabbit/wikis/wr-btrain-frame-format}{specified format}. This BTrain 
frame is then encapsulated inside WR Streamers frame which is in turn encapsulated
inside an Ethernet frame and sent over the WR Network by the WR PTP core. When a WR-BTrain node 
 receives such an Ethernet Frame, this frame is decoded into the BTrain-related 
information that is then passed on to the \textit{user logic}. All the transmission and
encapsulation details are hidden from the user by the set of BTrain-over-WhiteRabbit IP Cores.
BTrain-over-WhiteRabbit can be seen as a deterministic transport layer for BTrain
information that can be configured to provide fixed-latency of transmission. Section~\ref{sec:Overview of BTrain-over-WhiteRabbit integration} 
describes how this deterministic layer can be 
interfaced by the \textit{user logic}. Details of the interfaces are provided in 
Annex~\ref{sec:WR BTrain HDL generics and ports}

The application specific \textit{user logic} and the BTrain-over-WhiteRabbit cores compose a
WR-BTrain node.

\newpage

\subsection{WR-BTrain Node}
\label{sec:WR-BTrainNode}

The WR-BTrain Node (Figure~\ref{fig:WR-BTrainNode}) can be implemented in any WR-compatible board 
with an FPGA. BTrain-over-WhiteRabbit repository provides reference designs for the following boards:
\href{http://www.ohwr.org/projects/spec/wiki}{SPEC PCIe},
\href{http://www.ohwr.org/projects/svec/wiki}{SVEC VME},
\href{http://www.ohwr.org/projects/vfc-hd/wiki}{VFC-HD VME},
\href{https://www.ohwr.org/project/cute-wr-dp/wikis}{CUTE-WR-DP FMC }
available on the Open Hardware Repository and the
\href{https://edms.cern.ch/ui/#!master/navigator/project?P:1754500648:1754500648:subDocs}{VXS switch} available only at CERN. 
Any other board that has one of the
\href{https://www.ohwr.org/project/wr-cores/wikis/wrpc-core}{WR-supported FPGAs}
and the 
\href{http://www.ohwr.org/projects/white-rabbit/wiki/WRReferenceDesign}{required hardware}
can be used to develop a WR-BTrain node. 

\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=1.0\textwidth]{WR-BTrainNode.jpg}
    \caption{WR-BTrain Node}
    \label{fig:WR-BTrainNode}
  \end{center}
\end{figure}


There is a number of reference designs of WR-BTrain nodes that are provided in the
BTrain-over-WhiteRabbit repository. These reference designs are synthesis-ready 
and include VHDL top entities 
with the BTrainFrameTranceiver, Board Support Package (see section~8 of the 
\href{https://www.ohwr.org/project/wr-cores/wikis/Wrpc-release-v42#documentation}{WR PTP Core manual}), Wishbone Crossbar (WB Xbar) 
and IP cores to communicate with host (PCI or VME)\footnote{Communication with host is not possible on the VXS switch and FMC DIO card}. The user is left with the task of 
adding the application-specific \textit{user logic} to this design, as described in
section~\ref{sec:Overview of BTrain-over-WhiteRabbit integration}. The interface
between  the BTrainFrameTranceiver and the \textit{user logic} is a set of
BTrain-specific VHDL records (similar to structure in C), signals and generic
described in Annex~\ref{sec:WR BTrain HDL generics and ports}.
While in most of the designs, the application specific \textit{user's logic}
resides in the same FPGA as the BTrain-over-WhiteRabbit cores, it is possible 
to separate them into two different FPGAs. As of v1.2 release, the BTrain-over-WhiteRabbit
cores include a prototype version of the \textit{BTrain over FMC interface} that 
extends the BTrain VHDL records interface onto the FMC pins.
The \textit{BTrain over FMC interface} is described in Annex~\ref{sec:FMC BTrain Interfce (prototype)}
and it is instantiated in the reference design of the \textit{CUTE-WR-DP} FMC card.

Users wishing to include BTrain-over-WhiteRabbit in their own top entity of a supported
board, need to instantiate at least the BTrainFrameTransceiver and the 
 Board Support Package in their design. 

Users wishing to include BTrain-over-WhiteRabbit in their own board with one of the 
supported FPGAs, need to adapt and customize the Board Support Package appropriately
(see section~8 of the 
\href{https://www.ohwr.org/project/wr-cores/wikis/Wrpc-release-v42#documentation}{WR PTP Core manual}).

Functionally, there are three main IP Cores in the BTrain-over-WhiteRabbit that constitute
a transmission/reception chain (as depicted in Figure~\ref{fig:BTrain-over-WhiteRabbit}):
\begin{itemize*} 
\item \textbf{BTrainFrameTransceiver:} Encodes user's input into a BTrain frame before transmission, 
decodes BTrain frame into user's output after reception.
\item \textbf{WR Streamers:} generic module for low and configurable latency data streaming over raw Ethernet, 
it is used to stream BTrain frames.
\item \textbf{WR PTP Core:} \textit{"Fancy Ethernet MAC"} that is used to:
  \begin{itemize*}
  \item Transmit/receive Ethernet frames by WR Streamers
  \item Perform WR synchronization, used by WR Streamers to measure/select transmission latency
  \end{itemize*}
\end{itemize*}
As described in section~\ref{sec:userManual}, the control/configuration/diagnostics of 
these IP cores inside the WR-BTrain node are possible from host and (with some limitations) 
via WR network:
\begin{itemize*} 
\item \textbf{BTrainFrameTransceiver:} from host-only
\item \textbf{WR Streamers:} statistics from host and via WR network, config from host
\item \textbf{WR PTP Core:} diagnostics from host and via WR network, vuart from host
\end{itemize*}
As described in section~\ref{sec:Diagnostics and configuration using host tools}, software tools 
are provided for access of the IP cores from the host 
(the same tools for VME and PCI). For access via the WR network (SNMP), 
standard tools can be used, described in section~\ref{sec:Diagnostics using SNMP}.



Apart from the above-mentioned main components of the BTrain-over-WhiteRabbit, 
the project additionally provides :
\begin{itemize*} 
 \item \textbf{Support for old-BTrain} - modules and software that allow generation of
  legacy Bup/Bdown pulses and their comparison with the legacy system (described
  in Annex~\ref{sec:WR to Bup/Bdown Pulses Conveter and Sampler}):
  \begin{itemize*}
    \item \textbf{BUpDownConverter:} a module that generates the legacy Bup/Bdown
    pulses from the B value received in WR-BTrain frames.
    \item \textbf{BUpDownSampler:} a module that allows to compare the old-BTrain and WR-BTrain, accessible from host.
  \end{itemize*}
 \item \textbf{Support for BTrain over FMC interface} - modules that extend BTrain VHDL records
   onto FMC pins  (described in Annex~\ref{sec:FMC BTrain Interfce (prototype)}):
  \begin{itemize*}
    \item \textbf{BTrainFMCInterface:} a module that implements BTrain over FMC interface
      from the side of the mezzanine, it interacts with the BTrain VHDL records.
    \item \textbf{BTrainCarrierInterface:} a module to be instantiated on the FMC
      carrier, it allows to access BTrain-over-WhiteRabbit cores over the FMC interface
      and provides to the \textit{user's logic} Btrain interface in form of VHDL records,
      the same as provided by the \texttt{BTrainFrameTransceiver} module.
  \end{itemize*}
\end{itemize*}


% This manual describes:
% \begin{itemize*} 
% \item How to use load the reference designs in the supported boards
% \item How to use the provided host tools to diagnose the BTrain-over-WhiteRabbit IP cores
% \item How to integrate user logic with the BTrain-over-WhiteRabbit in the reference and custom
%       top entities.
% \end{itemize*}

% \newpage
\subsection{WR-BTrain Network and its VLAN Configuration}
\label{sec:WR-BTrainNetwork}

For the deployed WR-BTrain systems at CERN, the WR Network is a carefully engineered
Ethernet network. 
It uses VLANs to ensure that the critical BTrain-related information is exchanged between 
appropriate WR-BTrain nodes. In particular, the VLAN configuration ensures that only the 
WR-BTrain nodes that are supposed to send BTrain-related information are allowed to do so. 
In all the WR-BTrain networks:
\begin{itemize*}
\item B-related information is transmitted by one WR-BTrain node (managed by TE-MSC-MM) 
      and received by all other WR-BTrain nodes.
\end{itemize*}
Additionally, in some WR-BTrain networks:
\begin{itemize*}
\item I-related or C-related information is transmitted by one WR-BTrain node (managed by TE-EPC-CCE) while
      it is received by WR-BTrain node (managed by TE-MSC-MM) and WR-BTrain nodes (managed by TE-EPC-CCE)
\end{itemize*}
See \href{https://wikis.cern.ch/display/HT/B-train+network+layout+for+PS}{the layout and configuration of the WR-BTrain network} (CERN-only access) for the PS accelerator for more details.


A WR-BTrain node that receives B-related information can simply connect to the network
and it will receive appropriate information. In order to avoid disturbing the distribution
of the B-related information, sending of any other information to the network
is prevented by default. A users of a WR-BTrain  node that requires transmission or reception of
any other information needs to contact the network administrator
(write to \href{mailto:white-rabbit-user-support@cern.ch}{white-rabbit-user-support@cern.ch}).
Note that each WR-BTrain node can send diagnostics information (via SNMP). This
is done, in principle, without the knowledge of the WR-BTrain user/implementer.

As of release v1.2 of WR-BTrain-over-WhiteRabbit, 
the default configuration of \textit{WR Streamers} changed. Now, the
WR-BTrain nodes send BTrain data using \textit{priority tag} (i.e. Ethernet frames with
\href{https://en.wikipedia.org/wiki/IEEE_802.1Q}{802.1Q} tag indicating VLAN ID zero) while
the diagnostics frames are sent without \textit{802.1Q}.
This allows the WR switch to treat the two types of traffic differently. The BTrain
frames is re-tagged assigning appropriate VLAN, according to the port number
(i.e. what type of WR-BTrain node is connected to it). The diagnostics frames
are sent to the SNMP server.


For testing a WR-BTrain node before it is connected to the operation WR Network, there are two possibilities:
\begin{enumerate*}
\item Create your own WR Network consisting of Two WR-BTrain nodes, and preferably
      a WR Switch. In this case, one of the WR-BTrain nodes must generate BTrain
      frames which can be done as described in Annex~\ref{sec:WR BTrain Configuration}.
\item At CERN, there is a development WR-BTrain network that distributes
      B-related information from the PS accelerator. Users can request connection
      to this network which will allow them to test reception of B-related information. The request should
      be done by writing to \href{mailto:white-rabbit-user-support@cern.ch}{white-rabbit-user-support@cern.ch}.
\end{enumerate*}

In the former case, any WR switch can be used to play with WR-BTrain nodes. 
\textbf{As of v1.2 release, the default configuration of the WR switch is not
sufficient to test WR-BTrain Nodes with their new default VLAN configuration}. 
Connecting two WR-BTrain nodes directly also requires proper VLAN configuration
(and configuration of the \texttt{mode}, all described in 
section~\ref{sec:WR PTP Synchronization Status}).
One (and only one)
of the following configuration modification needs to be done to allow WR-BTrain traffic
through the WR Switch in your test/development WR Network
(without the change, the traffic will be discarded at reception):
\begin{enumerate*}
 \item \textbf{Configuration of ports on the WR switch} - enable untagging of frame
   for the ports of the WR Switch to which WR-BTrain nodes are connected:
  \begin{itemize*}
    \item If the WR switch is connected to the network at CERN, configured
    using CERN infrastructure (\href{https://ccde.cern.ch/}{CCDE}, CERN-only access) and
    runs release v6.0 or later, all should be already properly set.
    \item If the WR switch has default configuration (e.g. it was just taken
      out of the box), its configuration needs to be changed using Command Line Interface 
      \textit{wrsw\_vlans} for all the relevant ports, e.g. to change it for
       ports 1-3, ssh to your switch and execute:
      \begin{lstlisting}[basicstyle=\scriptsize\ttfamily]
      wrs$ wrsw_vlans --port 1 --puntag 1
      wrs$ wrsw_vlans --port 2 --puntag 1
      wrs$ wrsw_vlans --port 3 --puntag 1
      \end{lstlisting}
      For WR switch v6.0 or later, this configuration can be also set in 
      \textit{dot-config}, ideally using \textit{wrs\_menuconfig}, see
      \href{https://ohwr.org/project/wr-switch-sw/wikis/home}{WR Switch manual}.
  \end{itemize*}
 \item \textbf{Configuration of wr-streamesr on the WR-BTrain node} - disable tagging of 
   transmitted frames in the configuration of WR Streamers in one of two possible ways:
   \begin{enumerate*}
     \item Set to 0 the value of \textit{qtag\_ena} in the \textit{c\_tx\_streamer\_cfg\_btrain}
       record defined in \textit{BTrainFrameTransceiver\_pkg.vhd}.
    \item Disable VLAN configuration using the \textit{wr-streamers} Command Line Tool
      (see detailed explanation in section~\ref{sec:WR Streamers Statistics and Configuration}):
      \begin{lstlisting}[basicstyle=\scriptsize\ttfamily]
cfc-774-cbt:wrstm[01] > qtagor 1
Overriding of the default QTag config with WB config is Enabled
      \end{lstlisting}
   \end{enumerate*}
 \item \textbf{Configuration of WRPC on the WR-BTrain node} - enable VLANs in the WRPC:
   \begin{itemize*}
    \item Enable PTP and user-data to work over VLANs by using the 
    WR PTP Core Shell over Virtual-UART (\textit{wr-vuart}) (see description of 
    \textit{wr-vuart} in section~\ref{sec:WR PTP Core Vuart} and detailed explanation 
    of the setting in section~\ref{sec:Writing configuration}):
      \begin{lstlisting}[basicstyle=\scriptsize\ttfamily]
wrc# vlan set 30
      \end{lstlisting}
   \end{itemize*}
\end{enumerate*}

\newpage
\subsection{Repository Organization and Essential References}
\label{sec:RepositoryOrganizationAndEssentialReferences}

The organization of folders in the BTrain-over-WhiteRabbit repository and links essential
for readers of this manual are presented in this section. 

The essential links:
\begin{itemize*}

\item \url{https://gitlab.cern.ch/BTrain-TEAM/BTrain-over-WhiteRabbit/wikis/home}\\
      hosts BTrain-over-WhiteRabbit project, including wiki, BTrain frame format, repository and this documentation.
% 
% \item \url{https://gitlab.cern.ch/BTrain-TEAM/Btrain-over-WhiteRabbit/wikis/BTrain-over-WhiteRabbit-release-1.0}
%       hosts binaries and documentation for the release of BTrain-over-WhiteRabbit described in this document.

\item \url{http://www.ohwr.org/projects/wr-cores/}\\
      hosts WR IP Cores that are used in the BTrain-over-WhiteRabbit project (the repository
      with the WR IP Cores is used as a submodule in the BTrain-over-WhiteRabbit repository).

\item \url{https://www.ohwr.org/project/wr-cores/wikis/wrpc-core}\\
      Documents the WR PTP Core (WRPC).

\item \url{https://www.ohwr.org/project/wr-cores/wikis/WR-Streamers}\\
      Describes WR Streamers.

\item \url{https://wikis.cern.ch/display/HT/Btrain+over+White+Rabbit}\\
      Contains information about the WR-BTrain, installed systems, etc. (CERN-only access)
\end{itemize*}

The folder structure in the BTrain-over-WhiteRabbit repository is presented in 
Figure~\ref{fig:repositoryLayout}. In order to clone this repository and its submodules
(note: you need to have an account on gitlab to clone the repository) execute the 
following commands:

\begin{lstlisting}
$ git clone ssh://git@gitlab.cern.ch:7999/BTrain-TEAM/Btrain-over-WhiteRabbit.git
$ cd Btrain-over-WhiteRabbit
$ git checkout v1.2
$ git submodules init
$ git submodules update
\end{lstlisting}


The repository is organized according to the recommendations followed by the BE-CO-HT 
section and described in the
\href{https://www.ohwr.org/project/ohr-support/wikis/Folder-structure-example}{Open Hardware Repository}.
It uses other repositories as git submodules. The folders of interest to the readers
of this manual are:
\begin{itemize*}
\item \textbf{doc/wb-regs:}  Wishbone register maps of the relevant IP cores in HTML form
\item \textbf{hdl/ip\_cores/wr-cores/boards:}  the board specific wrappers
\item \textbf{hdl/ip\_cores/wr-cores/modules:}  WR PTP Core and WR Streamers
\item \textbf{hdl/rtl/BTrainFrameTransceiver:}  the BTraniFrameTransceiver
\item \textbf{hdl/rtl/BTrainCarrierInterface:}  module to access BTrain over FMC
\item \textbf{hdl/top:}  the reference top entities
\item \textbf{hdl/syn:}  projects with the reference boards ready for synthesis
\item \textbf{hdl/testbench:}  the testbench for one of the reference boards
\item \textbf{sw/tools:}  tool to access BTrainFrameTransceiver WB registers from the host (via PCI or VME) and a script to configure generation of "dummy" BTrain frames.
\item \textbf{sw/wrpc-sw/tools:}  tools to access WB registers of WR PTP Core and WR Streamers from host (via PCI or VME)
\end{itemize*}

\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=0.9\textwidth]{repositoryLayout.jpg}
    \caption{BTrain-over-WhiteRabbit repository structure}
    \label{fig:repositoryLayout}
  \end{center}
\end{figure}

Note that the repository contains more folders and files than indicated to
be of interest to the reader. These include extension of BTrain VHDL records
into \textit{BTrain over FMC interface}, described in Annex~\ref{sec:FMC BTrain Interfce (prototype)}.
Moreover, the repository contains also modules added to allow transition between the old and new BTrain systems.
They include a project with top entity, modules and
software that allow generation of legacy Bup/Bdown pulses and their comparison
with the legacy system. A detailed description of this additional HDL and SW is 
provided in Annex~\ref{sec:WR to Bup/Bdown Pulses Conveter and Sampler}.


The repository includes the following top-level synthesis-ready designs 
(in \textit{hdl/top} and \textit{hdl/syn}):
\begin{itemize*}
\item \textbf{spec\_ref\_design} -- WR-BTrain node reference design for
    \href{http://www.ohwr.org/projects/spec/wiki}{SPEC} (T45 and T100) with
     \href{http://www.ohwr.org/projects/fmc-dio-5chttla/wiki}{FMC DIO card}
     
\item \textbf{svec\_ref\_design} -- WR-BTrain node reference design for
    \href{http://www.ohwr.org/projects/svec/wiki}{SVEC} (T150)
    
\item \textbf{vxs\_ref\_design}  -- WR-BTrain node reference design for
     \href{https://edms.cern.ch/ui/#!master/navigator/project?P:1754500648:1754500648:subDocs}{VXS switch}
     
\item \textbf{vfchd\_ref\_design} -- WR-BTrain node reference design for
     \href{http://www.ohwr.org/projects/vfc-hd/wiki}{VFC-HD}
     
\item \textbf{cute\_ref\_design} --  WR-BTrain node reference design with BTrainFMCInterface 
      (see Annex~\ref{sec:FMC BTrain Interfce (prototype)}) for
     \href{https://www.ohwr.org/project/cute-wr-dp/wikis}{CUTE-WR-DP}
     
\item \textbf{spec\_bupdown\_conv} -- WR-BTrain node design with Bup/Down Pulses Converter and Sampler
      (see Annex~\ref{sec:WR to Bup/Bdown Pulses Conveter and Sampler}) for
     \href{http://www.ohwr.org/projects/spec/wiki}{SPEC} (T45) with 
     \href{http://www.ohwr.org/projects/fmc-dio-5chttla/wiki}{FMC DIO card}
     
\item \textbf{svec\_bupdown\_conv} -- WR-BTrain node design with Bup/Down Pulses Converter and Sampler
     (see Annex~\ref{sec:WR to Bup/Bdown Pulses Conveter and Sampler}) for 
     \href{http://www.ohwr.org/projects/svec/wiki}{SVEC} (T150) with
     \href{http://www.ohwr.org/projects/fmc-dio-5chttla/wiki}{FMC DIO card} on slot~1
     
     
\item \textbf{spec\_fixed\_latency\_ref\_design} -- WR-BTrain node reference design configured with ultra-fixed latency
     (see Annex~\ref{sec:Ultra-fixed latency reference design} ) for
     \href{http://www.ohwr.org/projects/spec/wiki}{SPEC} (T45)  with
     \href{http://www.ohwr.org/projects/fmc-dio-5chttla/wiki}{FMC DIO card} 
\end{itemize*}

\newpage
\subsection{Output/Input LEMO Signals \& LEDs in Top Level Designs}
\label{sec:Output and Input Auxiliary Digital Signals of Top Level Designs}

Tables~\ref{tab:ioLEMO} and \ref{tab:LED} provides information about the
output/input LEMO signals and LEDs of all the top level designs
included in BTrain-over-WhiteRabbit.

\begin{table}[h!]\footnotesize
   \begin{tabular}{| l | r |  l | p{7cm} |}    \hline
   \textbf{Design} & \textbf{I/O} & \textbf{Dir}      & \textbf{Signal desciption} \\ \hline \hline 

   \multirow{5}{*}{\textbf{spec\_ref\_design}}&
     FMC\_DIO: 1 & output & Pulse Per Second (PPS)  \\ \cline{2-4}
   & FMC\_DIO: 2 & output & Registered \texttt{tx\_valid} from \textit{wr\_streamers} \\ \cline{2-4}
   & FMC\_DIO: 3 & output & Registered \texttt{rx\_valid} from \textit{wr\_streamers} \\ \cline{2-4}
   & FMC\_DIO: 4 & output & Bup pulse for legacy BTrain\\ \cline{2-4}
   & FMC\_DIO: 5 & output & Bdown pulse for legacy BTrain \\ \hline  \hline 

   & SVEC\_GPIO: 1 & output & Registered \texttt{tx\_valid} from \textit{wr\_streamers} \\ \cline{2-4}
   \textbf{svec\_ref\_design}
   & SVEC\_GPIO: 2 & output & Registered \texttt{rx\_valid} from \textit{wr\_streamers} \\ \cline{2-4}
   \textbf{svec\_bupdown\_conv}
   & SVEC\_GPIO: 3 & output & Pulse Per Second (PPS)  \\ \cline{2-4}
   & SVEC\_GPIO: 4 & output & \texttt{tm\_time\_valid\_o}, it is HIGH if the WR-Node is synchronized \\ \hline \hline 

   
   & FMC\_DIO: 1 & input & Debug: Bup pulse from legacy BTrain  \\  \cline{2-4}
   \textbf{spec\_bupdown\_conv}
   & FMC\_DIO: 2 & input & Debug: Bdown pulse from legacy BTrain\\  \cline{2-4}
   \textbf{svec\_bupdown\_conv}
   & FMC\_DIO: 3 & input & Debug: C0 to reset legacy pulse counter \\  \cline{2-4}
   & FMC\_DIO: 4 & output& Bup pulse for legacy BTrain\\ \cline{2-4} 
   & FMC\_DIO: 5 & output& Bdown pulse for legacy BTrain\\ \hline \hline 


   \multirow{2}{*}{\textbf{vxs\_ref\_design}}&
     VXS\_IN/OUT: 1 & output &Pulse Per Second (PPS)  \\ \cline{2-4}
   & VXS\_IN/OUT: 2 & output & Registered \texttt{rx\_valid} from \textit{wr\_streamers} \\ \hline \hline 

   \multirow{4}{*}{\textbf{vfchd\_ref\_design}}&
     VFCHD\_GPIO: 1 & output & Registered \texttt{tx\_valid} from \textit{wr\_streamers} \\ \cline{2-4}
   & VFCHD\_GPIO: 2 & output & Registered \texttt{rx\_valid} from \textit{wr\_streamers} \\ \cline{2-4}
   & VFCHD\_GPIO: 3 & output & Pulse Per Second (PPS)  \\ \cline{2-4}
   & VFCHD\_GPIO: 4 & output & \texttt{tm\_time\_valid\_o}, it is HIGH if the WR-Node is synchronized  \\ \hline \hline 

   \multirow{2}{*}{\textbf{cute\_ref\_design}}&
     FMC\_DIO: 1 & output & Pulse Per Second (PPS)  \\  \cline{2-4}
   & FMC\_DIO: 2 & output & 10 MHz clock \\ \hline \hline 

   & FMC\_DIO: 1 & output & Pulse Per Second (PPS)  \\ \cline{2-4}
   \textbf{spec\_fixed\_latency}
   & FMC\_DIO: 2 & output & Registered \texttt{tx\_valid} \\  \cline{2-4}
   \textbf{\_ref\_design} 
   & FMC\_DIO: 3 & output & Registered \texttt{rx\_valid}  \\  \cline{2-4}
   & FMC\_DIO: 3 & output & \texttt{tx\_valid} directly from \textit{wr\_streamers} \\  \cline{2-4}
   & FMC\_DIO: 4 & output & \texttt{rx\_valid} directly from \textit{wr\_streamers} \\ \hline 

    \end{tabular}
\caption{Output/Input LEMO Signals in Top Level Designs.}
\label{tab:ioLEMO}
\end{table}

\begin{table}[h!]\footnotesize
   \begin{tabular}{| l | l |  p{7cm} |}    \hline
   \textbf{Design} & \textbf{LED} & \textbf{Desciption} \\ \hline \hline 

   \textbf{spec\_ref\_design},     & SPEC-red        & blinking indicates that packets are being transferred (\texttt{led\_act}) \\ \cline{2-3}
   \textbf{spec\_bupdown\_conv},   & SPEC-green      & indicates if the link is up  (\texttt{link})\\ \cline{2-3}
   \textbf{spec\_fixed\_latencyn}  & FMV\_DIO-top (TERM)  & indicates Pulse Per Second (\texttt{pps\_led})  \\ \cline{2-3}
   \textbf{\_ref\_desig}           & FMV\_DIO-bottom (OUT)& indicates received BTrain frame (\texttt{rx\_valid} )\\ \hline\hline

   \multirow{4}{*}{\textbf{svec\_ref\_design}}
                                  &  SVEC\_1 & indicates if the link is up (\texttt{link})    \\ \cline{2-3}
                                  &  SVEC\_4 & indicatesPulse Per Second (\texttt{pps\_led})  \\ \cline{2-3}
                                  &  SVEC\_5 & blinking indicates that packets are being transferred (\texttt{led\_act}) \\ \cline{2-3}
                                  &  SVEC\_8 & indicates received BTrain frame (\texttt{rx\_valid} ) \\ \cline{2-3}
   
   \multirow{2}{*}{\textbf{svec\_bupdown\_conv}}
                                  &  FMC\_DIO-top (TERM)    & indicates Pulse Per Second (\texttt{pps\_led})  \\ \cline{2-3}
                                  &  FMC\_DIO-bottom (OUT)  & indicates received BTrain frame (\texttt{rx\_valid} )  \\ \hline\hline

   \multirow{2}{*}{\textbf{vxs\_ref\_design}}
                                  &  VXS-green &  indicates if the link is up  (\texttt{link}) \\ \cline{2-3}
                                  &  VXS-red   & blinking indicates that packets are being transferred (\texttt{led\_act})   \\ \hline\hline

   \multirow{4}{*}{\textbf{vfchd\_ref\_design}}
                                  &  VFCHD\_0 & indicates if the link is up  (\texttt{link})   \\ \cline{2-3}
                                  &  VFCHD\_3 & indicates Pulse Per Second (\texttt{pps\_led})  \\ \cline{2-3}
                                  &  VFCHD\_4 & blinking indicates that packets are being transferred (\texttt{led\_act}) \\ \cline{2-3}
                                  &  VFCHD\_7 & indicates received BTrain frame (\texttt{rx\_valid} ) \\ \hline\hline


   \multirow{4}{*}{\textbf{cute\_ref\_design}}
                                  &  CUTE-SFP\_0 & blinking indicates that packets are being transferred
                                                   (\texttt{led\_act}) \\ \cline{2-3}
                                  &  CUTE-SFP\_1 & indicates Pulse Per Second (\texttt{pps\_led})  \\ \cline{2-3}
                                  &  CUTE-USR\_1 & indicates if the WR time is valid (\texttt{tm\_time\_valid})  \\ \cline{2-3}
                                  &  CUTE-USR\_2 & indicates if the link is up  (\texttt{tm\_link\_up}) \\ \hline
    \end{tabular}
\caption{LEDs in Top Level Designs.}
\label{tab:LED}
\end{table}